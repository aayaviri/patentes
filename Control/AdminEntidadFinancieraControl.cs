﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using Modelo;

namespace Control
{
    public static class AdminEntidadFinancieraControl
    {
        public static AdminEntidadFinanciera getAdminEntidadFinancieraByIdUsuario(int idUsuario) {
            AdminEntidadFinancieraDao admEntiFinDao = new AdminEntidadFinancieraDao();
            return admEntiFinDao.getByIdUsuario(idUsuario);
        }

        public static DataTable getAdminEntidadFinancieraByIdEntidadFinanciera(int idEntidadFinanciera) {
            AdminEntidadFinancieraDao admEntiFinDao = new AdminEntidadFinancieraDao();
            return admEntiFinDao.getByIdEntidadFinanciera(idEntidadFinanciera);
        }

        public static AdminEntidadFinanciera getById(int idAdmin) {
            AdminEntidadFinancieraDao adminDao = new AdminEntidadFinancieraDao();
            return adminDao.getById(idAdmin);
        }

        public static Boolean guardarAdmin(AdminEntidadFinanciera admin, Usuario usuario)
        {
            AdminEntidadFinancieraDao adminDao = new AdminEntidadFinancieraDao();
            UsuarioDao usuarioDao = new UsuarioDao();
            usuario.setRol("admin_entifin");
            if (admin.getId() == 0)
            {
                int idUsuario = usuarioDao.insert(usuario);
                admin.setIdUsuario(idUsuario);
                adminDao.insert(admin);
            }
            else
            {
                usuarioDao.update(usuario);
                adminDao.update(admin);
            }
            return true;
        }
    }
}
