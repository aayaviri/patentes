﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using Modelo;

namespace Control
{
    public static class CajeroControl
    {
        public static DataTable listarCajerosPorIdSucursal(int idSucursal)
        {
            CajeroDao cajeroDao = new CajeroDao();
            DataTable result = cajeroDao.getCajeroDataTableByIdSucursal(idSucursal);
            return result;
        }

        public static RespuestaControl guardarCajero(Cajero cajero)
        {
            CajeroDao cajeroDao = new CajeroDao();
            SucursalDao sucursalDao = new SucursalDao();
            Sucursal sucursal = sucursalDao.getById(cajero.getIdSucursal());
            RespuestaControl respuesta = new RespuestaControl();
            Cajero cajeroResp = cajeroDao.getByCodigoIdEntiFin(cajero.getCodigo(), sucursal.getIdEntidadFinanciera());
            if (cajeroResp != null && cajeroResp.getId() != cajero.getId())
            {
                respuesta.setSuccess(false);
                respuesta.setMessage("El codigo del cajero ya esta en uso.");
            }
            else {
                if (cajero.getId() == 0)
                {
                    cajeroDao.insert(cajero);
                }
                else
                {
                    cajeroDao.update(cajero);
                }
            }
            return respuesta;
        }

        public static Cajero getCajeroById(int id)
        {
            CajeroDao cajeroDao = new CajeroDao();
            return cajeroDao.getById(id);
        }

        public static RespuestaControl guardarCajero(Cajero cajero, Usuario usuario) {
            CajeroDao cajeroDao = new CajeroDao();
            UsuarioDao usuarioDao = new UsuarioDao();
            usuario.setRol("cajero");

            SucursalDao sucursalDao = new SucursalDao();
            Sucursal sucursal = sucursalDao.getById(cajero.getIdSucursal());
            
            RespuestaControl respuesta = new RespuestaControl();
            Cajero cajeroResp = cajeroDao.getByCodigoIdEntiFin(cajero.getCodigo(), sucursal.getIdEntidadFinanciera());
            Usuario usuarioResp = usuarioDao.getByLogin(usuario.getLogin());

            if (cajeroResp != null && cajeroResp.getId() != cajero.getId())
            {
                respuesta.setSuccess(false);
                respuesta.setMessage("El codigo del cajero ya esta en uso. Utilice otro por favor.");
            }
            else if (usuarioResp != null && usuarioResp.getId() != usuario.getId())
            {
                respuesta.setSuccess(false);
                respuesta.setMessage("El login de usuario ya esta en uso. Utilice otro por favor.");
            }
            else {
                if (cajero.getId() == 0)
                {
                    int idUsuario = usuarioDao.insert(usuario);
                    cajero.setIdUsuario(idUsuario);
                    cajeroDao.insert(cajero);
                }
                else
                {
                    usuarioDao.update(usuario);
                    cajeroDao.update(cajero);
                }
            }
            return respuesta;
        }

        public static RespuestaControl eliminarCajero(Cajero cajero)
        {
            RespuestaControl resp = new RespuestaControl();
            CajeroDao cajeroDao = new CajeroDao();
            UsuarioDao usuarioDao = new UsuarioDao();
            try
            {
                cajeroDao.delete(cajero);
                Usuario usuarioCajero = usuarioDao.getById(cajero.getIdUsuario());
                usuarioDao.delete(usuarioCajero);
            }
            catch (Exception ex)
            {
                resp.setSuccess(false);
                resp.setMessage(ex.Message);
                //resp.setMessage("No se puede eliminar el cajero seleccionado.\n\nPara poder eliminar un cajero, éste no debe estar asociado a ningun pago.");
            }
            return resp;
        }
    }
}
