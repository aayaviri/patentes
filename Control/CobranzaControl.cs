﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Modelo;

using System.Diagnostics;

namespace Control
{
    public static class CobranzaControl
    {
        public static SitioMunicipal getSitioMunicipalById(int id) {
            SitioMunicipalDao sitioDao = new SitioMunicipalDao();
            return sitioDao.getById(id);
        }

        public static Boolean guardarSitioMunicipal(SitioMunicipal sitio) {
            SitioMunicipalDao sitioDao = new SitioMunicipalDao();
            Debug.WriteLine(sitio.ToString());
            if (sitio.getId() == 0)
            {
                sitioDao.insert(sitio);
            }
            else {
                sitioDao.update(sitio);
            }
            return true;
        }

        

        public static Gestion getGestionByAnio(int anio) {
            GestionDao gestionDao = new GestionDao();
            return gestionDao.getByGestion(anio);
        }

        public static Cajero getCajeroByIdUsuario(int idUsuario) {
            CajeroDao cajeroDao = new CajeroDao();
            return cajeroDao.getByIdUsuario(idUsuario);
        }

        
    }
}
