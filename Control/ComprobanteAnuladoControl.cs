﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using Modelo;

namespace Control
{
    public static class ComprobanteAnuladoControl
    {
        public static RespuestaControl guardarComprobanteAnulado(ComprobanteAnulado comprobanteAnulado)
        {
            ComprobanteAnuladoDao comprobanteAnuladoDao = new ComprobanteAnuladoDao();
            PagoDao pagoDao = new PagoDao();
            RespuestaControl respuesta = new RespuestaControl();
            List<Pago> pagosAnulados = new List<Pago>();

            DataTable pagosDt = pagoDao.getPagoDataTableByNroComprobante(comprobanteAnulado.getNroComprobante());
            if (pagosDt.Rows.Count == 0)
            {
                respuesta.setSuccess(false);
                respuesta.setMessage("El nro comprobante no tiene pagos asociados.");
            }
            else {
                foreach (DataRow row in pagosDt.Rows) {
                    pagosAnulados.Add(pagoDao.loadDataRowToModel(row));
                }
                foreach (Pago pagoRevertido in pagosAnulados) {
                    pagoRevertido.setEstado("pendiente");
                    pagoDao.update(pagoRevertido);
                }
                comprobanteAnulado.setFechaAnulacion(DateTime.Now);
                comprobanteAnuladoDao.insert(comprobanteAnulado);
                respuesta.setMessage("Comprobante anulado correctamente.");
            }
            
            return respuesta;
        }

        public static DataTable listarComprobantesAnuladosIdCajeroFechas(int idCajero, DateTime fechaInicio, DateTime fechaFin)
        {
            ComprobanteAnuladoDao comprobanteAnuladoDao = new ComprobanteAnuladoDao();
            DataTable result = comprobanteAnuladoDao.getComprobanteAnuladoDataTableByIdCajeroFechas(idCajero, fechaInicio, fechaFin);
            return result;
        }

    }
}
