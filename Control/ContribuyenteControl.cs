﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Modelo;

using System.Diagnostics;

namespace Control
{
    public static class ContribuyenteControl
    {
        public static DataTable buscarContribuyentes(String search) {
            ContribuyenteDao contribuyenteDao = new ContribuyenteDao();
            return contribuyenteDao.getContribuyenteDataTableBySearch(search);
        }

        public static Contribuyente getContribuyenteById(int id) {
            ContribuyenteDao contribuyenteDao = new ContribuyenteDao();
            return contribuyenteDao.getById(id);
        }

        public static RespuestaControl guardarContribuyente(Contribuyente contribuyente) {
            ContribuyenteDao contribuyenteDao = new ContribuyenteDao();
            RespuestaControl resp = new RespuestaControl();

            Contribuyente contribResp = contribuyenteDao.getContribuyenteByCi(contribuyente.getCi());
            if (contribResp != null && contribResp.getId() != contribuyente.getId())
            {
                resp.setSuccess(false);
                resp.setMessage("El C.I. del contribuyente ya esta registrado.");
            }
            else {
                if (contribuyente.getId() == 0)
                {
                    contribuyenteDao.insert(contribuyente);
                }
                else
                {
                    contribuyenteDao.update(contribuyente);
                }
            }

            return resp;
        }

        public static RespuestaControl eliminarContribuyente(Contribuyente contribuyente) {
            RespuestaControl resp = new RespuestaControl();
            ContribuyenteDao contribuyenteDao = new ContribuyenteDao();
            try {
                contribuyenteDao.delete(contribuyente);
            }
            catch(Exception ex) {
                resp.setSuccess(false);
                resp.setMessage("No se puede eliminar el contribuyente seleccionado.\n\nPara poder eliminar el contribuyente este no debe estar asociado a ningun Sitio Municipal");
            }
            return resp;
        }
    }
}
