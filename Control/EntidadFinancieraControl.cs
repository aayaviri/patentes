﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Modelo;

using System.Diagnostics;

namespace Control
{
    public static class EntidadFinancieraControl
    {
        public static DataTable cargarEntidadesFinancieras() {
            EntidadFinancieraDao entiFinDao = new EntidadFinancieraDao();
            return entiFinDao.getEntidadFinancieraDataTable();
        }

        public static EntidadFinanciera getEntidadFinancieraById(int id) {
            EntidadFinancieraDao entiFinDao = new EntidadFinancieraDao();
            return entiFinDao.getById(id);
        }

        public static Boolean guardarEntidadFinanciera(EntidadFinanciera entiFin) {
            EntidadFinancieraDao entiFinDao = new EntidadFinancieraDao();
            Debug.WriteLine(entiFin.ToString());
            if (entiFin.getId() == 0)
            {
                entiFinDao.insert(entiFin);
            }
            else {
                entiFinDao.update(entiFin);
            }

            return true;
        }

        public static RespuestaControl eliminarEntidadFinanciera(EntidadFinanciera entidadFinanciera)
        {
            RespuestaControl resp = new RespuestaControl();
            EntidadFinancieraDao entiFinDao= new EntidadFinancieraDao();
            try
            {
                entiFinDao.delete(entidadFinanciera);
            }
            catch (Exception ex)
            {
                resp.setSuccess(false);
                resp.setMessage("No se puede eliminar la Entidad Financiera seleccionada.\n\nLa Entidad Financiera ya fue procesada");
            }
            return resp;
        }
    }
}
