﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Diagnostics;

using Modelo;

namespace Control
{
    public static class GestionControl
    {
        public static Boolean verificarUfvHoy(Gestion gestion)
        {
            Boolean result = false;
            if (gestion.getFechaUfv().Date.Equals(DateTime.Today))
            {
                result = true;
            }
            return result;
        }

        public static Boolean guardarNuevaUfv(Gestion gestion)
        {
            GestionDao gestionDao = new GestionDao();
            gestion.setFechaUfv(DateTime.Today);
            if (gestion.getId() == 0)
            {
                gestionDao.insert(gestion);
            }
            else
            {
                gestionDao.update(gestion);
            }
            return true;
        }

        public static Gestion obtenerUltimaGestion()
        {
            GestionDao gestionDao = new GestionDao();
            return gestionDao.getLastGestion();
        }

        public static Gestion getGestionByNumGestion(int numGestion) {
            GestionDao gestionDao = new GestionDao();
            return gestionDao.getByGestion(numGestion);
        }

        public static RespuestaControl generarPagosGestion(Gestion gestion) {
            RespuestaControl resp = new RespuestaControl();
            Gestion gestionOk = getGestionByNumGestion(gestion.getGestion());
            //TODO: ESTA IMPLEMENTACION ES COMPLICADA Y VA A LLEVAR MAS TIEMPO DESARROLLARLA Y TESTEARLA.
            return resp;
        }

        public static RespuestaControl guardarGestion(Gestion gestion) {
            RespuestaControl resp = new RespuestaControl();
            GestionDao gestionDao = new GestionDao();
            
            if (gestion.getId() == 0)
            {
                gestionDao.insert(gestion);
            }
            else
            {
                gestionDao.update(gestion);
            }

            return resp;
        }
    }
}
