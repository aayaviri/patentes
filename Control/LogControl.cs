﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Modelo;

namespace Control
{
    public static class LogControl
    {
        public static void logAccesoCorrectoAlSistema(Usuario usuario) { 
            LogDao logDao = new LogDao();
            Log log = new Log();
            log.setDescripcion("El usuario "+usuario.getLogin()+" accedio correctamente al sistema. \n"+ usuario.ToString());
            log.setIdUsuario(usuario.getId());
            log.setRol(usuario.getRol());
            log.setFechaHora(DateTime.Now);
            logDao.insert(log);
        }

        public static void logSalidaDelSistema(Usuario usuario)
        {
            LogDao logDao = new LogDao();
            Log log = new Log();
            log.setDescripcion("El usuario "+usuario.getLogin()+" salió correctamente del sistema. \n" + usuario.ToString());
            log.setIdUsuario(usuario.getId());
            log.setRol(usuario.getRol());
            log.setFechaHora(DateTime.Now);
            logDao.insert(log);
        }

        public static void logAccesoFallidoAlSistema(Usuario usuario)
        {
            LogDao logDao = new LogDao();
            Log log = new Log();
            log.setDescripcion("El usuario "+ usuario.getLogin() +" no pudo acceder correctamente al sistema con las siguientes credenciales: \n" + usuario.ToString());
            log.setFechaHora(DateTime.Now);
            logDao.insert(log);
        }

        public static void logPagarSitio(Usuario usuario, SitioMunicipal sitio, List<Pago> pagos) {
            String descripcion = "El usuario: "+usuario.getLogin()+ " realizo el cobro del sitio Municipal: " +sitio.getCodigoLicencia()+ ": ";
            LogDao logDao = new LogDao();
            Log log = new Log();
            foreach(Pago pago in pagos) {
                descripcion += "\n - Gestion: " + pago.getGestion() + " | Monto: "+ pago.getTotal();
            }

            log.setIdUsuario(usuario.getId());
            log.setRol(usuario.getRol());
            log.setFechaHora(DateTime.Now);
            logDao.insert(log);
        }

        public static void logGenerarComprobantePago(Usuario usuario, SitioMunicipal sitio) {
            LogDao logDao = new LogDao();
            Log log = new Log();
            log.setDescripcion("El usuario " + usuario.getLogin() + " generó el comprobante para el sitio: " + sitio.getCodigoLicencia());
            log.setIdUsuario(usuario.getId());
            log.setRol(usuario.getRol());
            log.setFechaHora(DateTime.Now);
            logDao.insert(log);
        }

        public static void logNuevoCajero(Usuario usuario, Cajero cajero) {
            LogDao logDao = new LogDao();
            Log log = new Log();
            log.setDescripcion("El usuario " + usuario.getLogin() + " registro al cajero con id: " + cajero.getId());
            log.setIdUsuario(usuario.getId());
            log.setRol(usuario.getRol());
            log.setFechaHora(DateTime.Now);
            logDao.insert(log);
        }

        public static void logModificarCajero(Usuario usuario, Cajero cajero) {
            LogDao logDao = new LogDao();
            Log log = new Log();
            log.setDescripcion("El usuario " + usuario.getLogin() + " modificó los datos del cajero con id: " + cajero.getId());
            log.setIdUsuario(usuario.getId());
            log.setRol(usuario.getRol());
            log.setFechaHora(DateTime.Now);
            logDao.insert(log);
        }

        public static void logEliminarCajero(Usuario usuario, Cajero cajero)
        {
            LogDao logDao = new LogDao();
            Log log = new Log();
            log.setDescripcion("El usuario " + usuario.getLogin() + " eliminó el cajero con id: " + cajero.getId());
            log.setIdUsuario(usuario.getId());
            log.setRol(usuario.getRol());
            log.setFechaHora(DateTime.Now);
            logDao.insert(log);
        }

        public static void logNuevaSucursal(Usuario usuario, Sucursal sucursal)
        {
            LogDao logDao = new LogDao();
            Log log = new Log();
            log.setDescripcion("El usuario " + usuario.getLogin() + " creó una nueva sucursal con id: " + sucursal.getId());
            log.setIdUsuario(usuario.getId());
            log.setRol(usuario.getRol());
            log.setFechaHora(DateTime.Now);
            logDao.insert(log);
        }

        public static void logModificarSucursal(Usuario usuario, Sucursal sucursal)
        {
            LogDao logDao = new LogDao();
            Log log = new Log();
            log.setDescripcion("El usuario " + usuario.getLogin() + " modificó la sucursal con id: " + sucursal.getId());
            log.setIdUsuario(usuario.getId());
            log.setRol(usuario.getRol());
            log.setFechaHora(DateTime.Now);
            logDao.insert(log);
        }

        public static void logEliminarSucursal(Usuario usuario, Sucursal sucursal)
        {
            LogDao logDao = new LogDao();
            Log log = new Log();
            log.setDescripcion("El usuario " + usuario.getLogin() + " eliminó la sucursal con id: " + sucursal.getId());
            log.setIdUsuario(usuario.getId());
            log.setRol(usuario.getRol());
            log.setFechaHora(DateTime.Now);
            logDao.insert(log);
        }

        public static void logRegistrarUfv(Usuario usuario, Double ufv) {
            LogDao logDao = new LogDao();
            Log log = new Log();
            log.setDescripcion("El usuario " + usuario.getLogin() + " una nueva ufv para el dia actual: " + ufv);
            log.setIdUsuario(usuario.getId());
            log.setRol(usuario.getRol());
            log.setFechaHora(DateTime.Now);
            logDao.insert(log);
        }

        public static void logCambioDosificacion(Usuario usuario, Cajero cajero) {
            LogDao logDao = new LogDao();
            Log log = new Log();
            log.setDescripcion("El usuario " + usuario.getLogin() + " cambió su dosificación a: " + cajero.getDosificacion());
            log.setIdUsuario(usuario.getId());
            log.setRol(usuario.getRol());
            log.setFechaHora(DateTime.Now);
            logDao.insert(log);
        }

        public static void logGenerarBalanceDiario(Usuario usuario) {
            LogDao logDao = new LogDao();
            Log log = new Log();
            log.setDescripcion("El usuario " + usuario.getLogin() + " generó su balance diario.");
            log.setIdUsuario(usuario.getId());
            log.setRol(usuario.getRol());
            log.setFechaHora(DateTime.Now);
            logDao.insert(log);
        }

        public static void logGenerarReporteEntidadFinanciera(Usuario usuario, EntidadFinanciera entiFin)
        {
            LogDao logDao = new LogDao();
            Log log = new Log();
            log.setDescripcion("El usuario " + usuario.getLogin() + " generó el reporte de la Entidad Financiera: " + entiFin.getNombre());
            log.setIdUsuario(usuario.getId());
            log.setRol(usuario.getRol());
            log.setFechaHora(DateTime.Now);
            logDao.insert(log);
        }
    }
}
