﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Modelo;

using System.Diagnostics;

namespace Control
{
    public static class ModuloEntidadFinancieraControl
    {
        public static Cajero getCajeroById(int id) {
            CajeroDao cajeroDao = new CajeroDao();
            return cajeroDao.getById(id);
        }

        public static AdminEntidadFinanciera getAdminEntiFinByIdUsuario(int id) {
            AdminEntidadFinancieraDao adminEntiFinDao = new AdminEntidadFinancieraDao();
            return adminEntiFinDao.getByIdUsuario(id);
        }
        
    }
}
