﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Diagnostics;

using Modelo;

namespace Control
{
    public static class PagoControl
    {
        public static Pago getPagoById(int id) {
            PagoDao pagoDao = new PagoDao();
            return pagoDao.getById(id);
        }

        public static DataTable getPagosPendientesPorIdSitioMunicipal(int idSitioMunicipal)
        {
            PagoDao pagoDao = new PagoDao();
            DataTable dataTable = pagoDao.getPagoDataTableByIdSitioMunicipalEstado(idSitioMunicipal, "pendiente");
            return dataTable;
        }

        public static DataTable getPagosPagadosPorIdSitioMunicipal(int idSitioMunicipal)
        {
            PagoDao pagoDao = new PagoDao();
            DataTable dataTable = pagoDao.getPagoDataTableByIdSitioMunicipalEstado(idSitioMunicipal, "pagado");
            return dataTable;
        }

        public static Boolean procesarPagos(List<Pago> pagoList)
        {
            PagoDao pagoDao = new PagoDao();
            foreach (Pago pago in pagoList) {
                pago.setFechaPago(DateTime.Now);
                pago.setEstado("pagado");
                pagoDao.update(pago);
            }
            return true;
        }

        public static Double getPagosTotal(DataTable pagosDataTable) {
            Double total = 0;
            foreach (DataRow pagoDataRow in pagosDataTable.Rows) {
                total += (Double)pagoDataRow["total"];
            }
            return total;
        }

        public static DataTable calcularPagosPendientes(DataTable dataTable, Gestion gestionActual, Cajero cajero)
        {
            DataRow currentRow;
            Double ufvVencida;
            int anioPago;
            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                ufvVencida = 0;
                currentRow = dataTable.Rows[i];
                anioPago = (int)currentRow["gestion"];
                //obtengo el ufvVenc de la gestion
                Gestion gestionPago = CobranzaControl.getGestionByAnio(anioPago);

                Debug.WriteLine(gestionPago);

                if (!gestionPago.getGestion().Equals(gestionActual.getGestion()))
                {
                    ufvVencida = gestionPago.getUfv();
                }

                //descuento Aplicado
                if (gestionActual.getGestion() == gestionPago.getGestion() && gestionActual.getFechaLimiteDescuentoAplicado().Date >= DateTime.Today) {
                    currentRow["descuentoAplicado"] = gestionActual.getDescuentoAplicado();
                }

                currentRow["multaIDF"] = gestionPago.getMultaIDF();
                currentRow["ufvVenc"] = ufvVencida;
                currentRow["interesUfv"] = gestionPago.getInteresUfv();
                //calculo del impuesto tributario en ufvs:
                if (ufvVencida == 0)
                {
                    currentRow["impuestoDeterminadoUfv"] = currentRow["impuestoDeterminadoBs"];
                }
                else
                {
                    currentRow["impuestoDeterminadoUfv"] = Math.Round(((Double)currentRow["impuestoDeterminadoBs"]) / ufvVencida, 2);
                }

                //Calculo de deuda tributaria en Ufvs:
                currentRow["deudaTributariaUfv"] = (Double)currentRow["impuestoDeterminadoUfv"] + (Double)currentRow["descuentoUfv"] + (Double)currentRow["interesUfv"] + (Double)currentRow["multaIDF"] - (Double)currentRow["descuentoAplicado"];
                currentRow["deudaTributariaUfv"] = Math.Round((Double)currentRow["deudaTributariaUfv"], 2);
                //calculo del total
                currentRow["total"] = Math.Round((Double)currentRow["deudaTributariaUfv"] * gestionActual.getUfv(), 2);
                currentRow["ufvPago"] = gestionActual.getUfv();
                if (cajero != null) {
                    currentRow["idCajero"] = cajero.getId();
                }
            }
            return dataTable;
        }

        public static RespuestaControl generarPagosUltimaGestion() {
            RespuestaControl resp = new RespuestaControl();

            int numGestionActual = DateTime.Today.Year;
            
            return resp;
        }
    }
}
