﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using Modelo;

namespace Control
{
    public static class ReporteControl
    {
        public static DataTable reportePagadosIdEntidadFechas(int idEntidadFinanciera, DateTime fechaInicio, DateTime fechaFin)
        {
            ReporteDao reporteDao = new ReporteDao();
            DataTable result = reporteDao.getPagadosIdEntidadFechas(idEntidadFinanciera, fechaInicio, fechaFin);
            return result;
        }

        public static DataTable getPagosPagadosPorCajeroFechas(int idCajero, DateTime fechaInicio, DateTime fechaFin)
        {
            ReporteDao reporteDao = new ReporteDao();
            return reporteDao.getPagosPagadosDataTableByIdCajeroFechas(idCajero, fechaInicio, fechaFin);
        }

        public static DataTable getPagosPagadosPorSucursalFechas(int idSucursal, DateTime fechaInicio, DateTime fechaFin)
        {
            ReporteDao reporteDao = new ReporteDao();
            return reporteDao.getPagosPagadosDataTableByIdSucursalFechas(idSucursal, fechaInicio, fechaFin);
        }

        public static DataTable getComprobantesAnuladosPorSucursalFechas(int idSucursal, DateTime fechaInicio, DateTime fechaFin)
        {
            ReporteDao reporteDao = new ReporteDao();
            return reporteDao.getComprobantesAnuladosByIdSucursalFechas(idSucursal, fechaInicio, fechaFin);
        }

        public static DataTable getComprobantesAnuladosPorEntidadFinancieraFechas(int idEntidadFinanciera, DateTime fechaInicio, DateTime fechaFin)
        {
            ReporteDao reporteDao = new ReporteDao();
            return reporteDao.getComprobantesAnuladosByIdEntidadFinancieraFechas(idEntidadFinanciera, fechaInicio, fechaFin);
        }

        public static DataTable getResumenEntidadesFechas(DateTime fechaInicio, DateTime fechaFin)
        {
            ReporteDao reporteDao = new ReporteDao();
            return reporteDao.getResumenEntidadesFechas(fechaInicio, fechaFin);
        }

        public static DataTable getReporteLogFechas(DateTime fechaInicio, DateTime fechaFin) {
            LogDao logDao = new LogDao();
            return logDao.getByFechas(fechaInicio, fechaFin);
        }

        public static DataTable reportePagadosIdSitioFechas(int idSitio, DateTime fechaInicio, DateTime fechaFin)
        {
            ReporteDao reporteDao = new ReporteDao();
            DataTable result = reporteDao.getPagosPagadosDataTableByIdSitioFechas(idSitio, fechaInicio, fechaFin);
            return result;
        }

        public static DataTable getResumenSucursalFechasIdEntidad(DateTime fechaInicio, DateTime fechaFin, int idEntidadFinanciera)
        {
            ReporteDao reporteDao = new ReporteDao();
            return reporteDao.getResumenSucursalesFechasIdEntidad(fechaInicio, fechaFin, idEntidadFinanciera);
        }
    }
}
