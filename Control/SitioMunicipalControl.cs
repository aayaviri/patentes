﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Modelo;

using System.Diagnostics;

namespace Control
{
    public static class SitioMunicipalControl
    {
        public static DataTable buscarSitiosMunicipales(String searchText) {
            SitioMunicipalDao sitioDao = new SitioMunicipalDao();
            return sitioDao.getSitioMunicipalDataTableBySearch(searchText);
        }

        public static DataTable buscarSitiosMunicipalesPorCodigo(String codigoLicencia) {
            SitioMunicipalDao sitioDao = new SitioMunicipalDao();
            return sitioDao.getSitioMunicipalDataTableBySearchCodigoLicencia(codigoLicencia);
        }

        public static DataTable buscarSitiosMunicipalesPorNombreApellidos(String nombre, String apellidos)
        {
            SitioMunicipalDao sitioDao = new SitioMunicipalDao();
            return sitioDao.getSitioMunicipalDataTableBySearchNombreApellidos(nombre, apellidos);
        }

        public static DataTable buscarSitiosMunicipalesPorNombreApellidos(String nombre, String apellidoPaterno, String apellidoMaterno) {
            SitioMunicipalDao sitioDao = new SitioMunicipalDao();
            return sitioDao.getSitioMunicipalDataTableBySearchNombreApellidoPaternoMaterno(nombre, apellidoPaterno, apellidoMaterno);
        }

        public static SitioMunicipal getSitioMunicipalById(int id) {
            SitioMunicipalDao sitioDao = new SitioMunicipalDao();
            return sitioDao.getById(id);
        }

        public static Boolean guardarSitioMunicipal(SitioMunicipal sitio) {
            SitioMunicipalDao sitioDao = new SitioMunicipalDao();
            Debug.WriteLine(sitio.ToString());
           try
                {
            if (sitio.getId() == 0)
            {
                
                    sitioDao.insert(sitio);
               
                
            }
            else {
                sitioDao.update(sitio);
            }
                }
           catch (Exception )
           {
               return false;
           }
            return true;
        }

        public static RespuestaControl eliminarContribuyente(SitioMunicipal sitioMunicipal)
        {
            RespuestaControl resp = new RespuestaControl();
            SitioMunicipalDao sitioDao = new SitioMunicipalDao();
            try
            {
                sitioDao.delete(sitioMunicipal);
            }
            catch (Exception ex)
            {
                resp.setSuccess(false);
                resp.setMessage("No se puede eliminar la Sitio Municipal seleccionado.\n\nEl sitio ya ha sido procesado con pagos.");
            }
            return resp;
        }
    }
}
