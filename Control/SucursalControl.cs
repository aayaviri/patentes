﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using Modelo;

namespace Control
{
    public static class SucursalControl
    {
        public static DataTable listarSurcursalesPorEntidadFinanciera(int idEntidadFinanciera) { 
            SucursalDao sucursalDao = new SucursalDao();
            DataTable result = sucursalDao.getSucursalDataTableByIdEntidadFinanciera(idEntidadFinanciera);
            return result;
        }

        public static Boolean guardarSucursal(Sucursal sucursal)
        {
            SucursalDao sucursalDao = new SucursalDao();
            if (sucursal.getId() == 0)
            {
                sucursalDao.insert(sucursal);
            }
            else
            {
                sucursalDao.update(sucursal);
            }
            return true;
        }

        public static Sucursal getSucursalById(int id) {
            SucursalDao sucursalDao = new SucursalDao();
            return sucursalDao.getById(id);
        }

        public static RespuestaControl eliminarSucursal(Sucursal sucursal)
        {
            RespuestaControl resp = new RespuestaControl();
            SucursalDao sucursalDao = new SucursalDao();
            try
            {
                sucursalDao.delete(sucursal);
            }
            catch (Exception ex)
            {
                resp.setSuccess(false);
                resp.setMessage("No se puede eliminar la sucursal seleccionada.\n\nPara poder eliminar la sucursal esta no debe tener ningun cajero");
            }
            return resp;
        }
    }
}
