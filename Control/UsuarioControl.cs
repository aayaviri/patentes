﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Modelo;

namespace Control
{
    public static class UsuarioControl
    {
        public static Usuario getUsuarioByid(int id)
        {
            UsuarioDao usuarioDao = new UsuarioDao();
            return usuarioDao.getById(id);
        }

        public static Usuario validarLogin(Usuario usuario)
        {
            UsuarioDao usuarioDao = new UsuarioDao();
            Usuario usuarioAutentificado = usuarioDao.getByLogin(usuario.getLogin(), usuario.getPassword());
            return usuarioAutentificado;
        }

        public static RespuestaControl guardarUsuario(Usuario usuario)
        {
            UsuarioDao usuarioDao = new UsuarioDao();
            RespuestaControl resp = new RespuestaControl();
            if (usuario.getId() == 0)
            {
                try
                {
                    usuarioDao.insert(usuario);
                }
                catch (Exception ex) {
                    resp.setSuccess(false);
                    resp.setMessage(ex.Message);
                }
            }
            else
            {
                try
                {
                    usuarioDao.update(usuario);
                }
                catch (Exception ex) {
                    resp.setSuccess(false);
                    resp.setMessage(ex.Message);
                }
            }

            return resp;
        }
    }
}
