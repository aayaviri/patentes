﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo
{
    public class AdminEntidadFinanciera
    {
        private int id;
        private int? idEntidadFinanciera;
        private int? idUsuario;
        private String nombreCompleto;
        private String estado;

        public AdminEntidadFinanciera() { }

        public void setId(int id) {
            this.id = id;
        }

        public int getId() {
            return this.id;
        }

        public void setIdEntidadFinanciera(int? idEntidadFinanciera) {
            this.idEntidadFinanciera = idEntidadFinanciera;
        }

        public int? getIdEntidadFinanciera() {
            return this.idEntidadFinanciera;
        }

        public void setIdUsuario(int? idUsuario) {
            this.idUsuario = idUsuario;
        }

        public int? getIdUsuario() {
            return this.idUsuario;
        }

        public void setNombreCompleto(String nombreCompleto) {
            this.nombreCompleto = nombreCompleto;
        }

        public String getNombreCompleto() {
            return this.nombreCompleto;
        }

        public void setEstado(String estado) {
            this.estado = estado;
        }

        public String getEstado() {
            return this.estado;
        }
    }
}
