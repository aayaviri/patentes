﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Modelo
{
    public class AdminEntidadFinancieraDao
    {
        private cobranza_patentesDataSetTableAdapters.admin_entidad_financieraTableAdapter tableAdapter = new cobranza_patentesDataSetTableAdapters.admin_entidad_financieraTableAdapter();

        public AdminEntidadFinanciera getByIdUsuario(int idUsuario)
        {
            AdminEntidadFinanciera adminEntidadFinanciera = null;
            DataTable dataTable = tableAdapter.GetByIdUsuario(idUsuario);
            if (dataTable.Rows.Count > 0)
            {
                adminEntidadFinanciera = loadDataRowToModel(dataTable.Rows[0]);
            }
            return adminEntidadFinanciera;
        }

        public AdminEntidadFinanciera getById(int idAdmin) {
            AdminEntidadFinanciera adminEntidadFinanciera = null;
            DataTable dataTable = tableAdapter.GetById(idAdmin);
            if (dataTable.Rows.Count > 0)
            {
                adminEntidadFinanciera = loadDataRowToModel(dataTable.Rows[0]);
            }
            return adminEntidadFinanciera;
        }

        public DataTable getByIdEntidadFinanciera(int idEntidadFinanciera) { 
            DataTable dtResult = tableAdapter.GetByIdEntidadFinanciera(idEntidadFinanciera);
            return dtResult;
        }

        public int insert(AdminEntidadFinanciera adminEntidadFinanciera)
        {
            int id = int.Parse(tableAdapter.InsertQuery(
                adminEntidadFinanciera.getNombreCompleto(), 
                adminEntidadFinanciera.getIdUsuario(), 
                adminEntidadFinanciera.getIdEntidadFinanciera(), 
                adminEntidadFinanciera.getEstado()).ToString());
            return id;
        }

        public void update(AdminEntidadFinanciera adminEntidadFinanciera)
        {
            tableAdapter.UpdateQuery(
                adminEntidadFinanciera.getNombreCompleto(), 
                adminEntidadFinanciera.getIdUsuario(), 
                adminEntidadFinanciera.getIdEntidadFinanciera(), 
                adminEntidadFinanciera.getEstado(),
                adminEntidadFinanciera.getId());
        }

        public void delete(AdminEntidadFinanciera adminEntidadFinanciera)
        {
            tableAdapter.DeleteQuery(adminEntidadFinanciera.getId());
        }

        private AdminEntidadFinanciera loadDataRowToModel(DataRow dataRow)
        {
            AdminEntidadFinanciera adminEntidadFinanciera = new AdminEntidadFinanciera();

            adminEntidadFinanciera.setId((int)dataRow["id"]);
            adminEntidadFinanciera.setNombreCompleto((String)dataRow["nombreCompleto"]);
            adminEntidadFinanciera.setIdEntidadFinanciera((int)dataRow["idEntidadFinanciera"]);
            adminEntidadFinanciera.setIdUsuario((int)dataRow["idUsuario"]);
            adminEntidadFinanciera.setEstado((String)dataRow["estado"]);

            return adminEntidadFinanciera;
        }
    }
}
