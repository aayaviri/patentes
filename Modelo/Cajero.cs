﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo
{
    public class Cajero
    {
        private int id;
        private String codigo;
        private String nombreCompleto;
        private String estado;
        private int idSucursal;
        private int idUsuario;
        private int dosificacion;

        public Cajero() { }

        public Cajero(int id, String codigo, String nombreCompleto, String estado, int idSucursal, int idUsuario, int dosificacion) {
            this.id = id;
            this.codigo = codigo;
            this.nombreCompleto = nombreCompleto;
            this.estado = estado;
            this.idSucursal = idSucursal;
            this.idUsuario = idUsuario;
            this.dosificacion = dosificacion;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getId() {
            return this.id;
        }

        public void setCodigo(String codigo) {
            this.codigo = codigo;
        }

        public String getCodigo() {
            return this.codigo;
        }

        public void setNombreCompleto(String nombreCompleto) {
            this.nombreCompleto = nombreCompleto;
        }

        public String getNombreCompleto() {
            return this.nombreCompleto;
        }

        public void setEstado(String estado) {
            this.estado = estado;
        }

        public String getEstado() {
            return this.estado;
        }

        public void setIdSucursal(int idSucursal) {
            this.idSucursal = idSucursal;
        }

        public int getIdSucursal() {
            return this.idSucursal;
        }

        public void setIdUsuario(int idUsuario) {
            this.idUsuario = idUsuario;
        }

        public int getIdUsuario() {
            return this.idUsuario;
        }

        public void setDosificacion(int dosificacion) {
            this.dosificacion = dosificacion;
        }

        public int getDosificacion() {
            return this.dosificacion;
        }

        public override String ToString()
        {
            return "id: " + this.id
                 + "\ncodigo: " + this.codigo
                 + "\nnombreCompleto: " + this.nombreCompleto
                 + "\nestado: " + this.estado
                 + "\nidSucursal: " + this.idSucursal
                 + "\nidUsuario: " + this.idUsuario
                 + "\ndosificacion: " + this.dosificacion;
        }
    }
}
