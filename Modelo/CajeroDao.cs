﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Diagnostics;

namespace Modelo
{
    public class CajeroDao
    {
        private cobranza_patentesDataSetTableAdapters.cajeroTableAdapter tableAdapter = new cobranza_patentesDataSetTableAdapters.cajeroTableAdapter();
        
        public DataTable getCajeroDataTableByIdSucursal(int idSucursal) {
            DataTable dataTable = new DataTable();
            dataTable = tableAdapter.GetByIdSucursal(idSucursal);
            return dataTable;
        }

        public Cajero getById(int id) {
            Cajero cajero = null;
            DataTable dataTable = tableAdapter.GetById(id);
            if (dataTable.Rows.Count > 0)
            {
                cajero = loadDataRowToModel(dataTable.Rows[0]);
            }
            return cajero;
        }

        public Cajero getByCodigoIdEntiFin(String codigo, int idEntiFin) {
            Cajero cajero = null;
            DataTable dataTable = tableAdapter.GetByCodigoIdEntidadFinanciera(codigo, idEntiFin);
            if (dataTable.Rows.Count > 0)
            {
                cajero = loadDataRowToModel(dataTable.Rows[0]);
            }
            return cajero;
        }

        public Cajero getByIdUsuario(int idUsuario) {
            Cajero cajero = null;
            DataTable dataTable = tableAdapter.GetByIdUsuario(idUsuario);
            if (dataTable.Rows.Count > 0)
            {
                cajero = loadDataRowToModel(dataTable.Rows[0]);
            }
            return cajero;
        }

        public int insert(Cajero cajero) {
            int id = int.Parse(tableAdapter.InsertQuery(cajero.getIdSucursal(), cajero.getCodigo(), cajero.getNombreCompleto(), cajero.getEstado(), cajero.getIdUsuario(), cajero.getDosificacion()).ToString());
            return id;
        }

        public void update(Cajero cajero) {
            tableAdapter.UpdateQuery(cajero.getIdSucursal(), cajero.getCodigo(), cajero.getNombreCompleto(), cajero.getEstado(), cajero.getIdUsuario(), cajero.getDosificacion(), cajero.getId());
        }

        public void delete(Cajero cajero) {
            tableAdapter.DeleteQuery(cajero.getId());
        }

        private Cajero loadDataRowToModel(DataRow dataRow) {
            Cajero cajero = new Cajero();

            cajero.setId((int)dataRow["id"]);
            cajero.setCodigo((String)dataRow["codigo"]);
            cajero.setNombreCompleto((String)dataRow["nombreCompleto"]);
            cajero.setIdSucursal((int)dataRow["idSucursal"]);
            cajero.setIdUsuario((int)dataRow["idUsuario"]);
            cajero.setEstado((String)dataRow["estado"]);
            if (!String.IsNullOrEmpty(dataRow["dosificacion"].ToString()))
            {
                cajero.setDosificacion((int)dataRow["dosificacion"]);
            }

            return cajero;
        }
    }
}
