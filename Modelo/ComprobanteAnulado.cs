﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo
{
    public class ComprobanteAnulado
    {
        private int id;
        private int idCajero;
        private int nroComprobante;
        private String motivo;
        private DateTime fechaAnulacion;

        public ComprobanteAnulado() { }

        public void setId(int id) {
            this.id = id;
        }

        public int getId() {
            return this.id;
        }

        public void setMotivo(String motivo) {
            this.motivo = motivo;
        }

        public String getMotivo() {
            return this.motivo;
        }

        public void setIdCajero(int idCajero) {
            this.idCajero = idCajero;
        }

        public int getIdCajero() {
            return this.idCajero;
        }

        public void setNroComprobante(int nroComprobante) {
            this.nroComprobante = nroComprobante;
        }

        public int getNroComprobante()
        {
            return this.nroComprobante;
        }

        public void setFechaAnulacion(DateTime fechaAnulacion) {
            this.fechaAnulacion = fechaAnulacion;
        }

        public DateTime getFechaAnulacion()
        {
            return this.fechaAnulacion;
        }

        public override String ToString()
        {
            return "id: " + this.id
                 + "\nmotivo: " + this.motivo
                 + "\nnroComprobante: " + this.nroComprobante
                 + "\nfechaAnulacion: " + this.fechaAnulacion
                 + "\nidCajero: " + this.idCajero;
        }
    }
}
