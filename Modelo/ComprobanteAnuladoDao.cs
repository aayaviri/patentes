﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Diagnostics;

namespace Modelo
{
    public class ComprobanteAnuladoDao
    {
        private cobranza_patentesDataSetTableAdapters.comprobantes_anuladosTableAdapter tableAdapter = new cobranza_patentesDataSetTableAdapters.comprobantes_anuladosTableAdapter();
        
        public DataTable getComprobanteAnuladoDataTableByIdCajeroFechas(int idCajero, DateTime fechaInicio, DateTime fechaFin) {
            DataTable dataTable = new DataTable();
            dataTable = tableAdapter.GetByIdCajeroFechas(idCajero, fechaInicio, fechaFin);
            return dataTable;
        }

        /*public ComprobanteAnulado getById(int id) {
            ComprobanteAnulado comprobanteAnulado = null;
            DataTable dataTable = tableAdapter.GetById(id);
            if (dataTable.Rows.Count > 0)
            {
                comprobanteAnulado = loadDataRowToModel(dataTable.Rows[0]);
            }
            return comprobanteAnulado;
        }*/

        public int insert(ComprobanteAnulado comprobanteAnulado) {
            int id = int.Parse(tableAdapter.InsertQuery(
                comprobanteAnulado.getNroComprobante(), 
                comprobanteAnulado.getFechaAnulacion(), 
                comprobanteAnulado.getMotivo(), 
                comprobanteAnulado.getIdCajero()).ToString());
            return id;
        }
        /*
        public void update(ComprobanteAnulado comprobanteAnulado) {
            tableAdapter.UpdateQuery(comprobanteAnulado.getIdSucursal(), comprobanteAnulado.getCodigo(), comprobanteAnulado.getNombreCompleto(), comprobanteAnulado.getEstado(), comprobanteAnulado.getIdUsuario(), comprobanteAnulado.getDosificacion(), comprobanteAnulado.getId());
        }

        public void delete(ComprobanteAnulado comprobanteAnulado) {
            tableAdapter.DeleteQuery(comprobanteAnulado.getId());
        }
        
        private ComprobanteAnulado loadDataRowToModel(DataRow dataRow) {
            ComprobanteAnulado comprobanteAnulado = new ComprobanteAnulado();

            comprobanteAnulado.setId((int)dataRow["id"]);
            comprobanteAnulado.setCodigo((String)dataRow["codigo"]);
            comprobanteAnulado.setNombreCompleto((String)dataRow["nombreCompleto"]);
            comprobanteAnulado.setIdSucursal((int)dataRow["idSucursal"]);
            comprobanteAnulado.setIdUsuario((int)dataRow["idUsuario"]);
            comprobanteAnulado.setEstado((String)dataRow["estado"]);
            if (!String.IsNullOrEmpty(dataRow["dosificacion"].ToString()))
            {
                comprobanteAnulado.setDosificacion((int)dataRow["dosificacion"]);
            }

            return comprobanteAnulado;
        }
         * */
    }
}
