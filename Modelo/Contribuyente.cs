﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo
{
    public class Contribuyente
    {
        private int id;
        private String nombre;
        private String apellidoPaterno;
        private String apellidoMaterno;
        private String ci;
        private String direccion;
        private String password;

        public Contribuyente() { }

        public Contribuyente(int id, String nombre, String apellidoPaterno, String apellidoMaterno, String ci, String direccion, String password) {
            this.id = id;
            this.nombre = nombre;
            this.apellidoPaterno = apellidoPaterno;
            this.apellidoMaterno = apellidoMaterno;
            this.ci = ci;
            this.direccion = direccion;
            this.password = password;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getId() {
            return this.id;
        }

        public void setNombre(String nombre) {
            this.nombre = nombre;
        }

        public String getNombre() {
            return this.nombre;
        }

        public void setApellidoPaterno(String apellidoPaterno) {
            this.apellidoPaterno = apellidoPaterno;
        }

        public String getApellidoPaterno() {
            return this.apellidoPaterno;
        }

        public void setApellidoMaterno(String apellidoMaterno) {
            this.apellidoMaterno = apellidoMaterno;
        }

        public String getApellidoMaterno() {
            return this.apellidoMaterno;
        }

        public void setCi(String ci) {
            this.ci = ci;
        }

        public String getCi() {
            return this.ci;
        }

        public void setDireccion(String direccion) {
            this.direccion = direccion;
        }

        public String getDireccion() {
            return this.direccion;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getPassword() {
            return this.password;
        }

        public override String ToString()
        {
            return "id: " + this.id
                 + "\nnombre: " + this.nombre
                 + "\napellidoPaterno: " + this.apellidoPaterno
                 + "\napellidoMaterno: " + this.apellidoMaterno
                 + "\nci: " + this.ci
                 + "\ndireccion: " + this.direccion
                 + "\npassword: " + this.password;
        }
    }
}
