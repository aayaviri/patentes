﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Modelo
{
    public class ContribuyenteDao
    {
        cobranza_patentesDataSetTableAdapters.contribuyenteTableAdapter adapContribuyente = new cobranza_patentesDataSetTableAdapters.contribuyenteTableAdapter();
        
        public Contribuyente getById(int id)
        {
            Contribuyente contribuyente = null;
            DataTable dataTable = adapContribuyente.GetById(id);
            if (dataTable.Rows.Count > 0)
            {
                contribuyente = loadDataRowToModel(dataTable.Rows[0]);
            }
            return contribuyente;
        }

        public Contribuyente getContribuyenteByCi(String ci)
        {
            Contribuyente contribuyente = null;
            DataTable dataTable = adapContribuyente.GetByCi(ci);
            if (dataTable.Rows.Count > 0)
            {
                contribuyente = loadDataRowToModel(dataTable.Rows[0]);
            }
            return contribuyente;
        }

        public DataTable getContribuyenteDataTableBySearch(String search) {
            DataTable dataTable = new DataTable();
            dataTable = adapContribuyente.GetBySearch(search);
            return dataTable;
        }

        public int insert(Contribuyente contribuyente) {
            int id = int.Parse(adapContribuyente.InsertQuery(contribuyente.getNombre(), contribuyente.getApellidoPaterno(), contribuyente.getApellidoMaterno(), contribuyente.getCi(), contribuyente.getDireccion(), Helper.CalculateMD5Hash(contribuyente.getPassword())).ToString());
            return id;
        }

        public void update(Contribuyente contribuyente) {
            Contribuyente contribuyenteAnterior = getById(contribuyente.getId());
            String password = contribuyenteAnterior.getPassword();

            if (!contribuyente.getPassword().Equals(contribuyenteAnterior.getPassword()))
            {
                password = Helper.CalculateMD5Hash(contribuyente.getPassword());
            }

            adapContribuyente.UpdateQuery(contribuyente.getNombre(), contribuyente.getApellidoPaterno(), contribuyente.getApellidoMaterno(), contribuyente.getCi(), contribuyente.getDireccion(), password, contribuyente.getId());
        }

        public void delete(Contribuyente contribuyente) {
            adapContribuyente.DeleteQuery(contribuyente.getId());
        }

        private Contribuyente loadDataRowToModel(DataRow dataRow)
        {
            Contribuyente contribuyente = new Contribuyente();

            contribuyente.setId((int)dataRow["id"]);
            contribuyente.setNombre((String)dataRow["nombre"]);
            contribuyente.setApellidoPaterno((String)dataRow["apellidoPaterno"]);
            contribuyente.setApellidoMaterno((String)dataRow["apellidoMaterno"]);
            contribuyente.setCi((String)dataRow["ci"]);
            contribuyente.setDireccion((String)dataRow["direccion"]);
            contribuyente.setPassword((String)dataRow["password"]);

            return contribuyente;
        }
    }
}
