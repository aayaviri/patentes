﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo
{
    public class EntidadFinanciera
    {
        private int id;
        private String nit;
        private String nombre;
        private String estado;
        private int maxCajeros;
        private List<Sucursal> sucursales = new List<Sucursal>();

        public EntidadFinanciera(int id, String nit, String nombre, String estado, int maxCajeros) {
            this.id = id;
            this.nit = nit;
            this.nombre = nombre;
            this.estado = estado;
            this.maxCajeros = maxCajeros;
        }

        public EntidadFinanciera() { }

        public void setId(int id) {
            this.id = id;
        }

        public int getId() {
            return this.id;
        }

        public void setNit(String nit) {
            this.nit = nit;
        }

        public String getNit() {
            return this.nit;
        }

        public void setNombre(String nombre) {
            this.nombre = nombre;
        }

        public String getNombre() {
            return this.nombre;
        }

        public void setEstado(String estado) {
            this.estado = estado;
        }

        public String getEstado() {
            return this.estado;
        }

        public void setMaxCajeros(int maxCajeros) {
            this.maxCajeros = maxCajeros;
        }

        public int getMaxCajeros() {
            return this.maxCajeros;
        }

        public List<Sucursal> getSucursales() {
            return this.sucursales;
        }

        public override String ToString()
        {
            return "id: " + this.id
                 + "\nnit: " + this.nit
                 + "\nnombre: " + this.nombre
                 + "\nestado: " + this.estado
                 + "\nmaxCajeros: " + this.maxCajeros;
        }
    }
}
