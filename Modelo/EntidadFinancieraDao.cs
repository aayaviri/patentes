﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Diagnostics;

namespace Modelo
{
    public class EntidadFinancieraDao
    {
        private cobranza_patentesDataSetTableAdapters.entidad_financieraTableAdapter tableAdapter = new cobranza_patentesDataSetTableAdapters.entidad_financieraTableAdapter();
        
        public DataTable getEntidadFinancieraDataTable() {
            DataTable dataTable = new DataTable();
            dataTable = tableAdapter.GetData();
            return dataTable;
        }

        public EntidadFinanciera getById(int id) {
            EntidadFinanciera entiFin = null;
            DataTable dataTable = tableAdapter.GetById(id);
            if (dataTable.Rows.Count > 0)
            {
                entiFin = loadDataRowToModel(dataTable.Rows[0]);
            }
            return entiFin;
        }

        public int insert(EntidadFinanciera entiFin) {
            int id = int.Parse(tableAdapter.InsertQuery(entiFin.getNit(), entiFin.getNombre(), entiFin.getEstado(), entiFin.getMaxCajeros()).ToString());
            return id;
        }

        public void update(EntidadFinanciera entiFin) {
            tableAdapter.UpdateQuery(entiFin.getNit(), entiFin.getNombre(), entiFin.getEstado(), entiFin.getMaxCajeros(), entiFin.getId());
        }

        public void delete(EntidadFinanciera entiFin) {
            tableAdapter.DeleteQuery(entiFin.getId());
        }

        public EntidadFinanciera loadDataRowToModel(DataRow dataRow) {
            EntidadFinanciera entiFin = new EntidadFinanciera();

            entiFin.setId((int)dataRow["id"]);
            entiFin.setNit((String)dataRow["nit"]);
            entiFin.setNombre((String)dataRow["nombre"]);
            entiFin.setMaxCajeros((int)dataRow["maxCajeros"]);
            entiFin.setEstado((String)dataRow["estado"]);

            return entiFin;
        }
    }
}
