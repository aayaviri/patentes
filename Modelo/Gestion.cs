﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo
{
    public class Gestion
    {
        private int id;
        private DateTime fechaUfv;
        private Double ufv;
        private int gestion;
        private Double interesUfv;
        private Double multaIDF;
        private Double descuentoAplicado;
        private DateTime fechaLimiteDescuentoAplicado;

        public Gestion() { }

        public void setId(int id)
        {
            this.id = id;
        }

        public int getId()
        {
            return this.id;
        }

        public void setFechaUfv(DateTime fechaUfv)
        {
            this.fechaUfv = fechaUfv;
        }

        public DateTime getFechaUfv()
        {
            return this.fechaUfv;
        }

        public void setUfv(Double ufv)
        {
            this.ufv = ufv;
        }

        public Double getUfv()
        {
            return this.ufv;
        }

        public void setGestion(int gestion)
        {
            this.gestion = gestion;
        }

        public int getGestion()
        {
            return this.gestion;
        }

        public void setInteresUfv(Double interesUfv)
        {
            this.interesUfv = interesUfv;
        }

        public Double getInteresUfv()
        {
            return this.interesUfv;
        }

        public void setMultaIDF(Double multaIDF)
        {
            this.multaIDF = multaIDF;
        }

        public Double getMultaIDF()
        {
            return this.multaIDF;
        }

        public void setDescuentoAplicado(Double descuentoAplicado)
        {
            this.descuentoAplicado = descuentoAplicado;
        }

        public Double getDescuentoAplicado()
        {
            return this.descuentoAplicado;
        }

        public void setFechaLimiteDescuentoAplicado(DateTime fechaLimiteDescuentoAplicado)
        {
            this.fechaLimiteDescuentoAplicado = fechaLimiteDescuentoAplicado;
        }

        public DateTime getFechaLimiteDescuentoAplicado()
        {
            return this.fechaLimiteDescuentoAplicado;
        }

        public override String ToString()
        {
            return "id: " + this.id
                 + "\nfechaUfv: " + this.fechaUfv
                 + "\nufv: " + this.ufv
                 + "\ngestion: " + this.gestion
                 + "\ninteresUfv: " + this.interesUfv
                 + "\nmultaIDF: " + this.multaIDF
                 + "\ndescuentoAplicado: " + this.descuentoAplicado
                 + "\nfechaLimiteDescuentoAplicado: " + this.fechaLimiteDescuentoAplicado;
        }
    }
}
