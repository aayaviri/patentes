﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Diagnostics;

using System.Diagnostics;

namespace Modelo
{
    public class GestionDao
    {
        private cobranza_patentesDataSetTableAdapters.gestionTableAdapter tableAdapter = new cobranza_patentesDataSetTableAdapters.gestionTableAdapter();

        public Gestion getByGestion(int numeroGestion)
        {
            Gestion gestion = null;
            DataTable dataTable = tableAdapter.GetByGestion(numeroGestion);
            if (dataTable.Rows.Count > 0)
            {
                gestion = loadDataRowToModel(dataTable.Rows[0]);
            }
            return gestion;
        }

        public Gestion getLastGestion()
        {
            Gestion gestion = null;
            DataTable dataTable = tableAdapter.GetLastGestion();
            if (dataTable.Rows.Count > 0)
            {
                gestion = loadDataRowToModel(dataTable.Rows[0]);
            }
            return gestion;
        }

        public int insert(Gestion gestion)
        {
            int id = int.Parse(tableAdapter.InsertQuery(
                gestion.getFechaUfv(),
                gestion.getUfv(),
                gestion.getGestion(),
                gestion.getInteresUfv(),
                gestion.getMultaIDF(),
                gestion.getDescuentoAplicado(),
                gestion.getFechaLimiteDescuentoAplicado()).ToString());
            return id;
        }

        public void update(Gestion gestion)
        {
            tableAdapter.UpdateQuery(
                gestion.getId(),
                gestion.getFechaUfv(),
                gestion.getUfv(),
                gestion.getGestion(),
                gestion.getInteresUfv(),
                gestion.getMultaIDF(),
                gestion.getDescuentoAplicado(),
                gestion.getFechaLimiteDescuentoAplicado());
        }

        public void delete(Gestion gestion)
        {
            tableAdapter.DeleteQuery(gestion.getId());
        }

        private Gestion loadDataRowToModel(DataRow dataRow)
        {
            Gestion gestion = new Gestion();

            gestion.setId((int)dataRow["id"]);
            gestion.setFechaUfv((DateTime)dataRow["fechaUfv"]);
            gestion.setUfv((Double)dataRow["ufv"]);
            gestion.setGestion((int)dataRow["gestion"]);
            gestion.setInteresUfv((Double)dataRow["interesUfv"]);
            gestion.setMultaIDF((Double)dataRow["multaIDF"]);

            if (!String.IsNullOrEmpty(dataRow["descuentoAplicado"].ToString()))
            {
                gestion.setDescuentoAplicado((Double)(dataRow["descuentoAplicado"]));
            }
            if (!String.IsNullOrEmpty(dataRow["fechaLimiteDescuentoAplicado"].ToString()))
            {
                gestion.setFechaLimiteDescuentoAplicado((DateTime)dataRow["fechaLimiteDescuentoAplicado"]);
            }

            return gestion;
        }
    }
}
