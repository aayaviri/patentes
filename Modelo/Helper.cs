﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.Threading;
using System.Diagnostics;

namespace Modelo
{
    public static class Helper
    {
        private static Process procesoPlink;

        public static String CalculateMD5Hash(String input)
        {
            // step 1, calculate MD5 hash from input
            MD5 md5 = MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("x2"));
            }
            return sb.ToString();
        }

        public static void ExecuteCommandSync(object command)
        {
            try
            {
                String plinkLocation = AppDomain.CurrentDomain.BaseDirectory + @"Resources\plink.exe";
                //System.Diagnostics.ProcessStartInfo procStartInfo =
                //    new System.Diagnostics.ProcessStartInfo("c:\\plink\\plink.exe", command.ToString());
                Debug.WriteLine(plinkLocation);
                System.Diagnostics.ProcessStartInfo procStartInfo =
                    new System.Diagnostics.ProcessStartInfo(plinkLocation, command.ToString());
                procStartInfo.RedirectStandardOutput = false;
                procStartInfo.RedirectStandardInput = false;
                procStartInfo.UseShellExecute = true;
                // Do not create the black window.
                procStartInfo.CreateNoWindow = false;
                procStartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                // Now we create a process, assign its ProcessStartInfo and start it
                procesoPlink = new System.Diagnostics.Process();
                procesoPlink.StartInfo = procStartInfo;
                procesoPlink.Start();
            }
            catch (Exception objException)
            {
                Debug.WriteLine(objException.Message);
                // Log the exception
            }
            
        }

        public static void ExecuteCommandAsync(string command)
        {
            try
            {
                //Asynchronously start the Thread to process the Execute command request.
                Thread objThread = new Thread(new ParameterizedThreadStart(ExecuteCommandSync));
                //Make the thread as background thread.
                objThread.IsBackground = true;
                //Set the Priority of the thread.
                objThread.Priority = ThreadPriority.AboveNormal;
                //Start the thread.
                objThread.Start(command);
            }
            catch (ThreadStartException objException)
            {
                Debug.WriteLine(objException.Message);
                // Log the exception
            }
            catch (ThreadAbortException objException)
            {
                Debug.WriteLine(objException.Message);
                // Log the exception
            }
            catch (Exception objException)
            {
                Debug.WriteLine(objException.Message);
                // Log the exception
            }
        }

        public static void terminateProcesoPlink() {
            try
            {
                procesoPlink.Kill();
            }
            catch (Exception ex) {
                Debug.WriteLine(ex.Message);
            }
        }
    }
}
