﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo
{
    public class Log
    {
        private int id;
        private DateTime fechaHora;
        private String descripcion;
        private int idUsuario;
        private String rol;

        public Log() { }

        public void setId(int id) {
            this.id = id;
        }

        public int getId() {
            return this.id;
        }

        public void setFechaHora(DateTime fechaHora) {
            this.fechaHora = fechaHora;
        }

        public DateTime getFechaHora()
        {
            return this.fechaHora;
        }

        public void setDescripcion(String descripcion) {
            this.descripcion = descripcion;
        }

        public String getDescripcion() {
            return this.descripcion;
        }

        public void setIdUsuario(int idUsuario)
        {
            this.idUsuario = idUsuario;
        }

        public int getIdUsuario()
        {
            return this.idUsuario;
        }

        public void setRol(String rol) {
            this.rol = rol;
        }

        public String getRol() {
            return this.rol;
        }

        public override String ToString() {
            return "id: " + this.id
                 + "\nfechaHora: " + this.fechaHora
                 + "\ndescripcion: " + this.descripcion
                 + "\nidUsuario: " + this.idUsuario
                 + "\nrol: " + this.rol;
        }

    }
}
