﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Modelo
{
    public class LogDao
    {
        private cobranza_patentesDataSetTableAdapters.logTableAdapter tableAdapter = new cobranza_patentesDataSetTableAdapters.logTableAdapter();

        public void insert(Log log)
        {
            int? idUsuario = null;
            if (log.getIdUsuario() != 0) {
                idUsuario = log.getIdUsuario();
            }
            tableAdapter.InsertQuery(
                log.getFechaHora(), 
                log.getDescripcion(), 
                idUsuario,
                log.getRol());
        }

        public DataTable getByFechas(DateTime fechaInicio, DateTime fechaFin)
        {
            DataTable dtResult = tableAdapter.GetByFechas(fechaInicio, fechaFin);
            return dtResult;
        }
    }
}
