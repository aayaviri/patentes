﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Modelo
{
    public class Pago
    {
        private int id;
        private int idSitioMunicipal;
        private int idCajero;
        private String estado;
        private DateTime fechaPago;
        private Double ufvPago;
        private int nroComprobante;
        private int gestion;
        private Double impuestoDeterminadoBs;
        private Double ufvVenc;
        private Double impuestoDeterminadoUfv;
        private Double descuentoUfv;
        private Double interesUfv;
        private Double multaIDF;
        private Double descuentoAplicado;
        private Double deudaTributariaUfv;
        private Double total;

        public Pago() { }

        public void setId(int id) {
            this.id = id;
        }

        public int getId() {
            return this.id;
        }

        public void setIdSitioMunicipal(int idSitioMunicipal)
        {
            this.idSitioMunicipal = idSitioMunicipal;
        }

        public int getIdSitioMunicipal()
        {
            return this.idSitioMunicipal;
        }

        public void setIdCajero(int idCajero)
        {
            this.idCajero = idCajero;
        }

        public int getIdCajero()
        {
            return this.idCajero;
        }

        public void setEstado(String estado)
        {
            this.estado = estado;
        }

        public String getEstado()
        {
            return this.estado;
        }

        public void setFechaPago(DateTime fechaPago)
        {
            this.fechaPago = fechaPago;
        }

        public DateTime getFechaPago()
        {
            return this.fechaPago;
        }

        public void setUfvPago(Double ufvPago)
        {
            this.ufvPago = ufvPago;
        }

        public Double getUfvPago()
        {
            return this.ufvPago;
        }

        public void setNroComprobante(int nroComprobante)
        {
            this.nroComprobante = nroComprobante;
        }

        public int getNroComprobante()
        {
            return this.nroComprobante;
        }

        public void setGestion(int gestion)
        {
            this.gestion = gestion;
        }

        public int getGestion()
        {
            return this.gestion;
        }

        public void setImpuestoDeterminadoBs(Double impuestoDeterminadoBs)
        {
            this.impuestoDeterminadoBs = impuestoDeterminadoBs;
        }

        public Double getImpuestoDeterminadoBs()
        {
            return this.impuestoDeterminadoBs;
        }

        public void setUfvVenc(Double ufvVenc)
        {
            this.ufvVenc = ufvVenc;
        }

        public Double getUfvVenc()
        {
            return this.ufvVenc;
        }

        public void setImpuestoDeterminadoUfv(Double impuestoDeterminadoUfv)
        {
            this.impuestoDeterminadoUfv = impuestoDeterminadoUfv;
        }

        public Double getImpuestoDeterminadoUfv()
        {
            return this.impuestoDeterminadoUfv;
        }

        public void setDescuentoUfv(Double descuentoUfv)
        {
            this.descuentoUfv = descuentoUfv;
        }

        public Double getDescuentoUfv()
        {
            return this.descuentoUfv;
        }

        public void setInteresUfv(Double interesUfv)
        {
            this.interesUfv = interesUfv;
        }

        public Double getInteresUfv()
        {
            return this.interesUfv;
        }

        public void setMultaIDF(Double multaIDF)
        {
            this.multaIDF = multaIDF;
        }

        public Double getMultaIDF()
        {
            return this.multaIDF;
        }

        public void setDescuentoAplicado(Double descuentoAplicado)
        {
            this.descuentoAplicado = descuentoAplicado;
        }

        public Double getDescuentoAplicado()
        {
            return this.descuentoAplicado;
        }

        public void setDeudaTributariaUfv(Double deudaTributariaUfv)
        {
            this.deudaTributariaUfv = deudaTributariaUfv;
        }

        public Double getDeudaTributariaUfv()
        {
            return this.deudaTributariaUfv;
        }

        public void setTotal(Double total)
        {
            this.total = total;
        }

        public Double getTotal()
        {
            return this.total;
        }

        public override String ToString()
        {
            return "id: " + this.id
                 + "\nidSitioMunicipal: " + this.idSitioMunicipal
                 + "\nidCajero: " + this.idCajero
                 + "\nestado: " + this.estado
                 + "\nfechaPago: " + this.fechaPago
                 + "\nufvPago: " + this.ufvPago
                 + "\nnroComprobante: " + this.nroComprobante
                 + "\ngestion: " + this.gestion
                 + "\nimpuestoDeterminadoBs: " + this.impuestoDeterminadoBs
                 + "\nufvVenc: " + this.ufvVenc
                 + "\nimpuestoDeterminadoUfv: " + this.impuestoDeterminadoUfv
                 + "\ndescuentoUfv: " + this.descuentoUfv
                 + "\ninteresUfv: " + this.interesUfv
                 + "\nmultaIDF: " + this.multaIDF
                 + "\ndescuentoAplicado: " + this.descuentoAplicado
                 + "\ndeudaTributariaUfv: " + this.deudaTributariaUfv
                 + "\ntotal: " + this.total;
        }
    }
}
