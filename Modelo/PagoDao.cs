﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using System.Diagnostics;

namespace Modelo
{
    public class PagoDao
    {
        cobranza_patentesDataSetTableAdapters.pagoTableAdapter adapPago = new cobranza_patentesDataSetTableAdapters.pagoTableAdapter();
        
        public Pago getById(int id)
        {
            Pago pago = null;
            DataTable dataTable = adapPago.GetById(id);
            if (dataTable.Rows.Count > 0)
            {
                pago = loadDataRowToModel(dataTable.Rows[0]);
            }
            return pago;
        }

        public DataTable getPagoDataTableByNroComprobante(int nroComprobante)
        {
            DataTable dataTable = new DataTable();
            dataTable = adapPago.GetByComprobante(nroComprobante);
            return dataTable;
        }

        public DataTable getPagoDataTableByIdSitioMunicipalEstado(int id, String estado)
        {
            DataTable dataTable = new DataTable();
            dataTable = adapPago.GetByIdSitioMunicipalEstado(id, estado);
            return dataTable;
        }

        public int insert(Pago pago) {
            int id = int.Parse(adapPago.InsertQuery(
                pago.getIdSitioMunicipal(), 
                pago.getIdCajero(), 
                pago.getEstado(), 
                pago.getFechaPago(), 
                pago.getUfvPago(), 
                pago.getNroComprobante(), 
                pago.getGestion(), 
                pago.getImpuestoDeterminadoBs(), 
                pago.getUfvVenc(), 
                pago.getImpuestoDeterminadoUfv(), 
                pago.getDescuentoUfv(), 
                pago.getInteresUfv(), 
                pago.getMultaIDF(), 
                pago.getDescuentoAplicado(), 
                pago.getDeudaTributariaUfv(), 
                pago.getTotal()
                ).ToString());
            return id;
        }

        public void update(Pago pago) {
            adapPago.UpdateQuery(
                pago.getIdSitioMunicipal(),
                pago.getIdCajero(),
                pago.getEstado(),
                pago.getFechaPago(),
                pago.getUfvPago(),
                pago.getNroComprobante(),
                pago.getGestion(),
                pago.getImpuestoDeterminadoBs(),
                pago.getUfvVenc(),
                pago.getImpuestoDeterminadoUfv(),
                pago.getDescuentoUfv(),
                pago.getInteresUfv(),
                pago.getMultaIDF(),
                pago.getDescuentoAplicado(),
                pago.getDeudaTributariaUfv(),
                pago.getTotal(),
                pago.getId()
                );
        }

        public void delete(Pago pago) {
            adapPago.DeleteQuery(pago.getId());
        }

        public Pago loadDataRowToModel(DataRow dataRow)
        {
            Pago pago = new Pago();

            pago.setId((int)dataRow["id"]);
            pago.setIdSitioMunicipal((int)dataRow["idSitioMunicipal"]);
            pago.setIdCajero((int)dataRow["idCajero"]);
            pago.setEstado((String)dataRow["estado"]);
            if (!String.IsNullOrEmpty(dataRow["fechaPago"].ToString()))
                pago.setFechaPago((DateTime)dataRow["fechaPago"]);
            pago.setUfvPago((Double)dataRow["ufvPago"]);
            if (!String.IsNullOrEmpty(dataRow["nroComprobante"].ToString()))
                pago.setNroComprobante((int)dataRow["nroComprobante"]);
            pago.setGestion((int)dataRow["gestion"]);
            pago.setImpuestoDeterminadoBs((Double)dataRow["impuestoDeterminadoBs"]);
            pago.setUfvVenc((Double)dataRow["ufvVenc"]);
            pago.setImpuestoDeterminadoUfv((Double)dataRow["impuestoDeterminadoUfv"]);
            pago.setDescuentoUfv((Double)dataRow["descuentoUfv"]);
            pago.setInteresUfv((Double)dataRow["interesUfv"]);
            pago.setMultaIDF((Double)dataRow["multaIDF"]);
            pago.setDescuentoAplicado((Double)dataRow["descuentoAplicado"]);
            pago.setDeudaTributariaUfv((Double)dataRow["deudaTributariaUfv"]);
            pago.setTotal((Double)dataRow["total"]);

            return pago;
        }
    }
}
