﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
namespace Modelo
{
    public class ReporteDao
    {
        public DataTable getPagadosIdEntidadFechas(int idEntidadFinanciera, DateTime fechaInicio, DateTime fechaFin) { 
            cobranza_patentesDataSetTableAdapters.reporte_pagosTableAdapter tableAdapter = new cobranza_patentesDataSetTableAdapters.reporte_pagosTableAdapter();
            DataTable dtResult = tableAdapter.GetByIdEntidadFinancieraFechas(idEntidadFinanciera, fechaInicio, fechaFin);
            return dtResult;
        }

        public DataTable getPagosPagadosDataTableByIdCajeroFechas(int idCajero, DateTime fechaInicio, DateTime fechaFin)
        {
            cobranza_patentesDataSetTableAdapters.reporte_pagosTableAdapter tableAdapter = new cobranza_patentesDataSetTableAdapters.reporte_pagosTableAdapter();
            DataTable dtResult = tableAdapter.GetPagadosByCajeroFechas(idCajero, fechaInicio, fechaFin);
            return dtResult;
        }

        public DataTable getPagosPagadosDataTableByIdSucursalFechas(int idSucursal, DateTime fechaInicio, DateTime fechaFin)
        {
            cobranza_patentesDataSetTableAdapters.reporte_pagosTableAdapter tableAdapter = new cobranza_patentesDataSetTableAdapters.reporte_pagosTableAdapter();
            DataTable dtResult = tableAdapter.GetByIdSucursalFechas(fechaInicio, fechaFin, idSucursal);
            return dtResult;
        }

        public DataTable getComprobantesAnuladosByIdSucursalFechas(int idSucursal, DateTime fechaInicio, DateTime fechaFin)
        {
            cobranza_patentesDataSetTableAdapters.reporte_anuladosTableAdapter tableAdapter = new cobranza_patentesDataSetTableAdapters.reporte_anuladosTableAdapter();
            DataTable dtResult = tableAdapter.GetByIdSucursalFechas(idSucursal, fechaInicio, fechaFin);
            return dtResult;
        }

        public DataTable getComprobantesAnuladosByIdEntidadFinancieraFechas(int idEntidadFinanciera, DateTime fechaInicio, DateTime fechaFin)
        {
            cobranza_patentesDataSetTableAdapters.reporte_anuladosTableAdapter tableAdapter = new cobranza_patentesDataSetTableAdapters.reporte_anuladosTableAdapter();
            DataTable dtResult = tableAdapter.GetByIdSucursalFechas(idEntidadFinanciera, fechaInicio, fechaFin);
            return dtResult;
        }

        public DataTable getResumenEntidadesFechas(DateTime fechaInicio, DateTime fechaFin) {
            cobranza_patentesDataSetTableAdapters.reporte_pagosTableAdapter tableAdapter = new cobranza_patentesDataSetTableAdapters.reporte_pagosTableAdapter();
            DataTable dtResult = tableAdapter.GetResumenEntidadesFechas(fechaInicio, fechaFin);
            return dtResult;
        }

        public DataTable getPagosPagadosDataTableByIdSitioFechas(int idSitio, DateTime fechaInicio, DateTime fechaFin)
        {
            cobranza_patentesDataSetTableAdapters.reporte_pagosTableAdapter tableAdapter = new cobranza_patentesDataSetTableAdapters.reporte_pagosTableAdapter();
            DataTable dtResult = tableAdapter.GetByIdSitioFechas(fechaInicio, fechaFin, idSitio);
            return dtResult;
        }

        public DataTable getResumenSucursalesFechasIdEntidad(DateTime fechaInicio, DateTime fechaFin, int idEntidadFinanciera) {
            cobranza_patentesDataSetTableAdapters.reporte_pagosTableAdapter tableAdapter = new cobranza_patentesDataSetTableAdapters.reporte_pagosTableAdapter();
            DataTable dtResult = tableAdapter.GetResumenSucursalesFechasIdEntidad(fechaInicio, fechaFin, idEntidadFinanciera);
            return dtResult;
        }
    }
}
