﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo
{
    public class RespuestaControl
    {
        private Boolean success;
        private String message;
        private int idAsociado;

        public RespuestaControl() {
            this.success = true;
        }

        public void setSuccess(Boolean success) {
            this.success = success;
        }

        public Boolean getSuccess() {
            return this.success;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getMessage() {
            return this.message;
        }

        public void setIdAsociado(int idAsociado) {
            this.idAsociado = idAsociado;
        }

        public int getIdAsociado() {
            return this.idAsociado;
        }
    }
}
