﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo
{
    public class SitioMunicipal
    {
        private int id;
        private String codigoLicencia;
        private String descripcion;
        private String direccion;
        private String estado;
        private DateTime fechaInicio;
        private String razonSocial;
        private Double superficie;
        private String tipo;
        private int? idContribuyente;

        public SitioMunicipal() { }

        public SitioMunicipal(int id, String codigoLicencia, String descripcion, String direccion,
            String estado, DateTime fechaInicio, String razonSocial, Double superficie,
            String tipo, int idContribuyente) {
            this.id = id;
            this.codigoLicencia = codigoLicencia;
            this.descripcion = descripcion;
            this.direccion = direccion;
            this.estado = estado;
            this.fechaInicio = fechaInicio;
            this.razonSocial = razonSocial;
            this.superficie = superficie;
            this.tipo = tipo;
            this.idContribuyente = idContribuyente;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getId() {
            return this.id;
        }

        public void setCodigoLicencia(String codigoLicencia)
        {
            this.codigoLicencia = codigoLicencia;
        }

        public String getCodigoLicencia()
        {
            return this.codigoLicencia;
        }

        public void setDescripcion(String descripcion)
        {
            this.descripcion = descripcion;
        }

        public String getDescripcion()
        {
            return this.descripcion;
        }

        public void setDireccion(String direccion)
        {
            this.direccion = direccion;
        }

        public String getDireccion()
        {
            return this.direccion;
        }

        public void setEstado(String estado)
        {
            this.estado = estado;
        }

        public String getEstado()
        {
            return this.estado;
        }

        public void setFechaInicio(DateTime fechaInicio)
        {
            this.fechaInicio = fechaInicio;
        }

        public DateTime getFechaInicio()
        {
            return this.fechaInicio;
        }

        public void setRazonSocial(String razonSocial)
        {
            this.razonSocial = razonSocial;
        }

        public String getRazonSocial()
        {
            return this.razonSocial;
        }

        public void setSuperficie(Double superficie)
        {
            this.superficie = superficie;
        }

        public Double getSuperficie()
        {
            return this.superficie;
        }

        public void setTipo(String tipo)
        {
            this.tipo = tipo;
        }

        public String getTipo()
        {
            return this.tipo;
        }

        public void setIdContribuyente(int? idContribuyente)
        {
            this.idContribuyente = idContribuyente;
        }

        public int? getIdContribuyente()
        {
            return this.idContribuyente;
        }

        public override String ToString()
        {
            return "id: " + this.id
                 + "\ncodigoLicencia: " + this.codigoLicencia
                 + "\ndescripcion: " + this.descripcion
                 + "\ndireccion: " + this.direccion
                 + "\nestado: " + this.estado
                 + "\nfechaInicio: " + this.fechaInicio
                 + "\nrazonSocial: " + this.razonSocial
                 + "\nsuperficie: " + this.superficie
                 + "\ntipo: " + this.tipo
                 + "\nidContribuyente: " + this.idContribuyente;
        }
    }
}
