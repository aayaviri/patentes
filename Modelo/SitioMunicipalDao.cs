﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Diagnostics;

namespace Modelo
{
    public class SitioMunicipalDao
    {
        cobranza_patentesDataSetTableAdapters.sitio_municipalTableAdapter tableAdapter= new cobranza_patentesDataSetTableAdapters.sitio_municipalTableAdapter();
        
        public SitioMunicipal getById(int id)
        {
            SitioMunicipal sitioMunicipal = null;
            DataTable dataTable = tableAdapter.GetById(id);
            if (dataTable.Rows.Count > 0)
            {
                sitioMunicipal = loadDataRowToModel(dataTable.Rows[0]);
            }
            return sitioMunicipal;
        }

        public DataTable getSitioMunicipalDataTableBySearch(String searchText) {
            DataTable dataTable = new DataTable();
            dataTable = tableAdapter.GetBySearch(searchText);
            return dataTable;
        }

        public DataTable getSitioMunicipalDataTableBySearchCodigoLicencia(String codigoLicencia) {
            DataTable dataTable = new DataTable();
            dataTable = tableAdapter.GetBySearchCodigoLicencia(codigoLicencia);
            return dataTable;
        }

        public DataTable getSitioMunicipalDataTableBySearchNombreApellidos(String nombre, String apellidos)
        {
            DataTable dataTable = new DataTable();
            dataTable = tableAdapter.GetBySearchNombreApellidos(nombre, apellidos);
            return dataTable;
        }

        public DataTable getSitioMunicipalDataTableBySearchNombreApellidoPaternoMaterno(String nombre, String apellidoPaterno, String apellidoMaterno) {
            DataTable dataTable = new DataTable();
            dataTable = tableAdapter.GetBySearchNombreApellidoPaternoMaterno(nombre, apellidoPaterno, apellidoMaterno);
            return dataTable;
        }

        public DataTable getSitioMunicipalDataTableByIdContribuyente(int id)
        {
            DataTable dataTable = new DataTable();
            dataTable = tableAdapter.GetByIdContribuyente(id);
            return dataTable;
        }

        public int? insert(SitioMunicipal sitioMunicipal) {
            try
            {
            int id = int.Parse(tableAdapter.InsertQuery(
                sitioMunicipal.getCodigoLicencia(),
                sitioMunicipal.getDescripcion(),
                sitioMunicipal.getDireccion(),
                sitioMunicipal.getEstado(),
                sitioMunicipal.getFechaInicio(),
                sitioMunicipal.getRazonSocial(),
                sitioMunicipal.getSuperficie(),
                sitioMunicipal.getTipo(),
                sitioMunicipal.getIdContribuyente()
                ).ToString());
                return id;
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public void update(SitioMunicipal sitioMunicipal) {
            tableAdapter.UpdateQuery(
                sitioMunicipal.getCodigoLicencia(),
                sitioMunicipal.getDescripcion(),
                sitioMunicipal.getDireccion(),
                sitioMunicipal.getEstado(),
                sitioMunicipal.getFechaInicio(),
                sitioMunicipal.getRazonSocial(),
                sitioMunicipal.getSuperficie(),
                sitioMunicipal.getTipo(),
                sitioMunicipal.getIdContribuyente(),
                sitioMunicipal.getId()
                );
        }

        public void delete(SitioMunicipal sitioMunicipal) {
            tableAdapter.DeleteQuery(sitioMunicipal.getId());
        }

        public SitioMunicipal loadDataRowToModel(DataRow dataRow)
        {
            SitioMunicipal sitioMunicipal = new SitioMunicipal();

            sitioMunicipal.setId((int)dataRow["id"]);
            sitioMunicipal.setCodigoLicencia((String)dataRow["codigoLicencia"]);
            sitioMunicipal.setDescripcion((String)dataRow["descripcion"]);
            sitioMunicipal.setDireccion((String)dataRow["direccion"]);
            sitioMunicipal.setEstado((String)dataRow["estado"]);
            sitioMunicipal.setFechaInicio((DateTime)dataRow["fechaInicio"]);
            sitioMunicipal.setRazonSocial((String)dataRow["razonSocial"]);
            sitioMunicipal.setSuperficie((Double)dataRow["superficie"]);
            sitioMunicipal.setTipo((String)dataRow["tipo"]);
            if (!String.IsNullOrEmpty(dataRow["idContribuyente"].ToString()))
            {
                sitioMunicipal.setIdContribuyente((int?)dataRow["idContribuyente"]);
            }
            else {
                sitioMunicipal.setIdContribuyente(null);
            }
            return sitioMunicipal;
        }
    }
}
