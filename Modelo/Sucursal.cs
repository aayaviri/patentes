﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo
{
    public class Sucursal
    {
        private int id;
        private String codigo;
        private String direccion;
        private int idEntidadFinanciera;
        private EntidadFinanciera entidadFinanciera;

        public Sucursal() { }

        public Sucursal(int id, String codigo, String direccion, int idEntidadFinanciera) {
            this.id = id;
            this.codigo = codigo;
            this.direccion = direccion;
            this.idEntidadFinanciera = idEntidadFinanciera;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getId() {
            return this.id;
        }

        public void setCodigo(String codigo) {
            this.codigo = codigo;
        }

        public String getCodigo() {
            return this.codigo;
        }

        public void setDireccion(String direccion) {
            this.direccion = direccion;
        }

        public String getDireccion() {
            return this.direccion;
        }

        public void setIdEntidadFinanciera(int idEntidadFinanciera) {
            this.idEntidadFinanciera = idEntidadFinanciera;
        }

        public int getIdEntidadFinanciera() {
            return this.idEntidadFinanciera;
        }

        public void setEntidadFinanciera(EntidadFinanciera entidadFinanciera) {
            this.entidadFinanciera = entidadFinanciera;
        }

        public EntidadFinanciera getEntidadFinanciera() {
            return this.entidadFinanciera;
        }

        public override String ToString()
        {
            return "id: " + this.id
                 + "\ncodigo: " + this.codigo
                 + "\ndireccion: " + this.direccion
                 + "\nidEntidadFinanciera: " + this.idEntidadFinanciera;
        }
    }
}
