﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Diagnostics;

namespace Modelo
{
    public class SucursalDao
    {
        private cobranza_patentesDataSetTableAdapters.sucursalTableAdapter tableAdapter = new cobranza_patentesDataSetTableAdapters.sucursalTableAdapter();

        public DataTable getSucursalDataTableByIdEntidadFinanciera(int idEntidadFinanciera)
        {
            DataTable dataTable = new DataTable();
            dataTable = tableAdapter.GetByIdEntidadFinanciera(idEntidadFinanciera);
            return dataTable;
        }

        public Sucursal getByCodigoIdEntiFin(String codigo, int idEntiFin)
        {
            Sucursal sucursal = null;
            DataTable dataTable = tableAdapter.GetByCodigoIdEntidadFinanciera(codigo, idEntiFin);
            if (dataTable.Rows.Count > 0)
            {
                sucursal = loadDataRowToModel(dataTable.Rows[0]);
            }
            return sucursal;
        }

        public Sucursal getById(int id)
        {
            Sucursal sucursal = null;
            DataTable dataTable = tableAdapter.GetById(id);
            if (dataTable.Rows.Count > 0)
            {
                sucursal = loadDataRowToModel(dataTable.Rows[0]);
            }
            return sucursal;
        }

        public int insert(Sucursal sucursal)
        {
            int id = int.Parse(tableAdapter.InsertQuery(
                sucursal.getCodigo(), 
                sucursal.getDireccion(),
                sucursal.getIdEntidadFinanciera()).ToString());
            return id;
        }

        public void update(Sucursal sucursal)
        {
            tableAdapter.UpdateQuery(
                sucursal.getCodigo(),
                sucursal.getDireccion(),
                sucursal.getIdEntidadFinanciera(),
                sucursal.getId());
        }

        public void delete(Sucursal sucursal)
        {
            tableAdapter.DeleteQuery(sucursal.getId());
        }

        private Sucursal loadDataRowToModel(DataRow dataRow)
        {
            Sucursal sucursal = new Sucursal();

            sucursal.setId((int)dataRow["id"]);
            sucursal.setCodigo((String)dataRow["codigo"]);
            sucursal.setDireccion((String)dataRow["direccion"]);
            sucursal.setIdEntidadFinanciera((int)dataRow["idEntidadFinanciera"]);

            return sucursal;
        }
    }
}
