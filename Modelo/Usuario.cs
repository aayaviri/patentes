﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo
{
    public class Usuario
    {
        private int id;
        private String login;
        private String password;
        private String rol;

        public Usuario() { }

        public Usuario(int id, String login, String password, String rol) {
            this.id = id;
            this.login = login;
            this.password = password;
            this.rol = rol;
        }

        public Usuario(String login, String password, String rol) {
            this.login = login;
            this.password = password;
            this.rol = rol;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getId() {
            return this.id;
        }

        public void setLogin(String login) {
            this.login = login;
        }

        public String getLogin() {
            return this.login;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getPassword() {
            return this.password;
        }

        public void setRol(String rol) {
            this.rol = rol;
        }

        public String getRol() {
            return this.rol;
        }

        public override String ToString() {
            return "id: " + this.id
                 + "\nlogin: " + this.login
                 + "\npassword: " + this.password
                 + "\nrol: " + this.rol;
        }

    }
}
