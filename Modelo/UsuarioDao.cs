﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Modelo
{
    public class UsuarioDao
    {
        private cobranza_patentesDataSetTableAdapters.usuarioTableAdapter tableAdapter = new cobranza_patentesDataSetTableAdapters.usuarioTableAdapter();
        
        public Usuario getById(int id) {
            Usuario usuario = null;
            DataTable dataTable = tableAdapter.GetById(id);
            if (dataTable.Rows.Count > 0)
            {
                usuario = loadDataRowToModel(dataTable.Rows[0]);
            }
            return usuario;
        }

        public Usuario getByLogin(String login, String password) { 

            Usuario usuario = null;
            DataTable dataTable = tableAdapter.GetByLoginPassword(login, Helper.CalculateMD5Hash(password));
            if (dataTable.Rows.Count == 1) {
                usuario = loadDataRowToModel(dataTable.Rows[0]);
            }
            return usuario;
        }

        public Usuario getByLogin(String login)
        {
            Usuario usuario = null;
            DataTable dataTable = tableAdapter.GetByLogin(login);
            if (dataTable.Rows.Count == 1)
            {
                usuario = loadDataRowToModel(dataTable.Rows[0]);
            }
            return usuario;
        }

        public int insert(Usuario usuario)
        {
            int id = int.Parse(tableAdapter.InsertQuery(
                usuario.getLogin(), 
                Helper.CalculateMD5Hash(usuario.getPassword()), 
                usuario.getRol()).ToString());
            return id;
        }

        public void update(Usuario usuario)
        {
            Usuario usuarioAnterior = getById(usuario.getId());
            String password = usuarioAnterior.getPassword();

            if (!usuario.getPassword().Equals(usuarioAnterior.getPassword())) {
                password = Helper.CalculateMD5Hash(usuario.getPassword());
            }

            tableAdapter.UpdateQuery(
                usuario.getLogin(),
                password,
                usuario.getRol(),
                usuario.getId());
        }

        public void delete(Usuario usuario)
        {
            tableAdapter.DeleteQuery(usuario.getId());
        }

        private Usuario loadDataRowToModel(DataRow dataRow)
        {
            Usuario usuario = new Usuario();

            usuario.setId((int)dataRow["id"]);
            usuario.setLogin((String)dataRow["login"]);
            usuario.setPassword((String)dataRow["password"]);
            usuario.setRol((String)dataRow["rol"]);

            return usuario;
        }
    }
}
