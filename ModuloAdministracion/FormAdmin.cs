﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

using Control;
using Modelo;

namespace ModuloAdministracion
{
    public partial class FormAdmin : Form
    {
        private AdminEntidadFinanciera admin;
        private EntidadFinanciera entiFin;
        private Usuario usuario;
        private Boolean passwordChanged=false;

        public FormAdmin(EntidadFinanciera entiFin)
        {
            InitializeComponent();
            this.admin = new AdminEntidadFinanciera();
            this.usuario = new Usuario();

            this.entiFin = entiFin;
        }

        public FormAdmin(EntidadFinanciera entiFin, AdminEntidadFinanciera admin)
        {
            InitializeComponent();
            this.admin = admin;
            this.entiFin = entiFin;
            this.usuario = UsuarioControl.getUsuarioByid((int)this.admin.getIdUsuario());
            loadModelToForm();
            passwordChanged = true;
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            RespuestaControl validacion = validarFormulario();

            if (validacion.getSuccess())
            {
                guardarFormulario();
            }
            else
            {
                lblErrorMessage.Text = validacion.getMessage();
                lblErrorMessage.Visible = true;
            }
        }

        private void loadModelToForm()
        {
            txtNombre.Text = admin.getNombreCompleto();
            cbxEstado.SelectedItem = admin.getEstado();

            txtLogin.Text = usuario.getLogin();
            txtPassword.Text = usuario.getPassword();
        
        }

        private void loadFormToModel()
        {
            admin.setNombreCompleto(txtNombre.Text);
            admin.setEstado(cbxEstado.SelectedItem.ToString());
            admin.setIdEntidadFinanciera(entiFin.getId());

            usuario.setLogin(txtLogin.Text);
            usuario.setPassword(txtPassword.Text);
        }
        private RespuestaControl validarFormulario()
        {
            RespuestaControl resp = new RespuestaControl();

            if (String.IsNullOrEmpty(txtNombre.Text) || String.IsNullOrWhiteSpace(txtNombre.Text))
            {
                resp.setSuccess(false);
                resp.setMessage(resp.getMessage() + "El nombre es requerido.\n");
            }

            if (cbxEstado.SelectedItem == null || String.IsNullOrEmpty(cbxEstado.SelectedItem.ToString()) || String.IsNullOrWhiteSpace(cbxEstado.SelectedItem.ToString()))
            {
                resp.setSuccess(false);
                resp.setMessage(resp.getMessage() + "El estado es requerido.\n");
            }

            if (String.IsNullOrEmpty(txtLogin.Text) || String.IsNullOrWhiteSpace(txtLogin.Text))
            {
                resp.setSuccess(false);
                resp.setMessage(resp.getMessage() + "El Login es requerido.\n");
            }
            if (!passwordChanged || admin.getId() == 0)
            {
                Regex r = new Regex("^[a-zA-Z]\\w{8,15}$");
                if (!r.IsMatch(txtPassword.Text))
                {
                    resp.setSuccess(false);
                    resp.setMessage(resp.getMessage() + "La contraseña debe tener como mínimo 8 caracteres y nun máximo de 15. También debe comenzar por una letra");
                }

            }
            return resp;
        }
        private void guardarFormulario()
        {
            loadFormToModel();
            if (AdminEntidadFinancieraControl.guardarAdmin(this.admin, this.usuario))
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                MessageBox.Show("Ocurrio un error al guardar el admin");
            }
        }
        
        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtPassword_TextChanged(object sender, EventArgs e)
        {
            passwordChanged = false;
        }

        private void FormAdmin_Load(object sender, EventArgs e)
        {

        }
    }
}
