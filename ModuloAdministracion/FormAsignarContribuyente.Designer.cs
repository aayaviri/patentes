﻿namespace ModuloAdministracion
{
    partial class FormAsignarContribuyente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvContribuyentesEncontrados = new System.Windows.Forms.DataGridView();
            this.textSearch = new System.Windows.Forms.TextBox();
            this.btnBuscarContribuyente = new System.Windows.Forms.Button();
            this.btnSeleccionarContribuyente = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvContribuyentesEncontrados)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvContribuyentesEncontrados
            // 
            this.dgvContribuyentesEncontrados.AllowUserToAddRows = false;
            this.dgvContribuyentesEncontrados.AllowUserToDeleteRows = false;
            this.dgvContribuyentesEncontrados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvContribuyentesEncontrados.Location = new System.Drawing.Point(12, 43);
            this.dgvContribuyentesEncontrados.MultiSelect = false;
            this.dgvContribuyentesEncontrados.Name = "dgvContribuyentesEncontrados";
            this.dgvContribuyentesEncontrados.ReadOnly = true;
            this.dgvContribuyentesEncontrados.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvContribuyentesEncontrados.Size = new System.Drawing.Size(387, 150);
            this.dgvContribuyentesEncontrados.TabIndex = 0;
            // 
            // textSearch
            // 
            this.textSearch.Location = new System.Drawing.Point(12, 12);
            this.textSearch.Name = "textSearch";
            this.textSearch.Size = new System.Drawing.Size(299, 20);
            this.textSearch.TabIndex = 1;
            this.textSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textSearch_KeyDown);
            // 
            // btnBuscarContribuyente
            // 
            this.btnBuscarContribuyente.Image = global::ModuloAdministracion.Properties.Resources.Search_24;
            this.btnBuscarContribuyente.Location = new System.Drawing.Point(317, 6);
            this.btnBuscarContribuyente.Name = "btnBuscarContribuyente";
            this.btnBuscarContribuyente.Size = new System.Drawing.Size(82, 31);
            this.btnBuscarContribuyente.TabIndex = 3;
            this.btnBuscarContribuyente.Text = "Buscar";
            this.btnBuscarContribuyente.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnBuscarContribuyente.UseVisualStyleBackColor = true;
            this.btnBuscarContribuyente.Click += new System.EventHandler(this.btnBuscarContribuyente_Click);
            // 
            // btnSeleccionarContribuyente
            // 
            this.btnSeleccionarContribuyente.Location = new System.Drawing.Point(98, 199);
            this.btnSeleccionarContribuyente.Name = "btnSeleccionarContribuyente";
            this.btnSeleccionarContribuyente.Size = new System.Drawing.Size(91, 39);
            this.btnSeleccionarContribuyente.TabIndex = 10;
            this.btnSeleccionarContribuyente.Text = "Asignar";
            this.btnSeleccionarContribuyente.UseVisualStyleBackColor = true;
            this.btnSeleccionarContribuyente.Click += new System.EventHandler(this.btnSeleccionarContribuyente_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Image = global::ModuloAdministracion.Properties.Resources.cancel_24;
            this.btnCancelar.Location = new System.Drawing.Point(222, 199);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(89, 39);
            this.btnCancelar.TabIndex = 11;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // FormAsignarContribuyente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(412, 250);
            this.ControlBox = false;
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnSeleccionarContribuyente);
            this.Controls.Add(this.btnBuscarContribuyente);
            this.Controls.Add(this.textSearch);
            this.Controls.Add(this.dgvContribuyentesEncontrados);
            this.Name = "FormAsignarContribuyente";
            this.Text = "Asignar Contribuyente";
            ((System.ComponentModel.ISupportInitialize)(this.dgvContribuyentesEncontrados)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvContribuyentesEncontrados;
        private System.Windows.Forms.TextBox textSearch;
        private System.Windows.Forms.Button btnBuscarContribuyente;
        private System.Windows.Forms.Button btnSeleccionarContribuyente;
        private System.Windows.Forms.Button btnCancelar;
    }
}