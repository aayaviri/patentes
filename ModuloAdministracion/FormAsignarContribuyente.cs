﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Modelo;
using Control;

namespace ModuloAdministracion
{
    public partial class FormAsignarContribuyente : Form
    {
        private SitioMunicipal sitioMunicipal;

        public FormAsignarContribuyente(SitioMunicipal sitioMunicipal)
        {
            InitializeComponent();
            this.sitioMunicipal = sitioMunicipal;
        }

        private void btnBuscarContribuyente_Click(object sender, EventArgs e)
        {
            loadContribuyentes(textSearch.Text);
        }

        private void loadContribuyentes(String searchText)
        {
            if (searchText.Length >= 4)
            {
                DataTable dataTable = ContribuyenteControl.buscarContribuyentes(searchText);
                dgvContribuyentesEncontrados.DataSource = dataTable;
                dgvContribuyentesEncontrados.Columns["id"].Visible = false;
                dgvContribuyentesEncontrados.Columns["password"].Visible = false;
                dgvContribuyentesEncontrados.Refresh();
            }
            else
            {
                MessageBox.Show("El texto a buscar debe tener al menos 4 caracteres.");
            }
        }

        private void textSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData.Equals(Keys.Return))
            {
                loadContribuyentes(textSearch.Text);
            }
        }

        private void btnSeleccionarContribuyente_Click(object sender, EventArgs e)
        {
            asignarContribuyente();
        }

        private void asignarContribuyente() {
            
            if (dgvContribuyentesEncontrados.SelectedRows.Count > 0)
            {
                int idContribuyente = (int)dgvContribuyentesEncontrados.SelectedRows[0].Cells["id"].Value;
                Contribuyente contribuyente = ContribuyenteControl.getContribuyenteById(idContribuyente);
                DataTable pagosPendientes = PagoControl.getPagosPendientesPorIdSitioMunicipal(sitioMunicipal.getId());
                if (pagosPendientes.Rows.Count > 0)
                {
                    MessageBox.Show("El sitio no puede ser transferido por que tiene pagos pendientes de pago.");
                }
                else {
                    sitioMunicipal.setRazonSocial(contribuyente.getNombre() + " " + contribuyente.getApellidoPaterno() + " " + contribuyente.getApellidoMaterno());
                    sitioMunicipal.setIdContribuyente(idContribuyente);
                    sitioMunicipal.setFechaInicio(DateTime.Today);
                    SitioMunicipalControl.guardarSitioMunicipal(sitioMunicipal);
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
            }
            else {
                MessageBox.Show("Debe seleccionar un contribuyente primero");
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
