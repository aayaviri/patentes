﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

using Modelo;
using Control;

namespace ModuloAdministracion
{
    public partial class FormCambioPassword : Form
    {
        private Usuario usuario;

        public FormCambioPassword(Usuario usuario)
        {
            this.usuario = usuario;
            InitializeComponent();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (validarFormulario())
            {
                guardarFormulario();
            }
        }

        private Boolean validarFormulario() {
            Boolean resp = true;
            Regex r = new Regex("^[a-zA-Z]\\w{8,15}$");
            lblErrorMessage.Text = " ";
            if ((usuario.getPassword() != Helper.CalculateMD5Hash(txtPasswordAntiguo.Text)))
            {
                resp = false;
                lblErrorMessage.Text = "La contraseña actual es incorrecta ";
                lblErrorMessage.Visible = true;
            }

            if (!r.IsMatch(txtNuevoPassword.Text))
            {
                resp = false;
                lblErrorMessage.Text = "La contraseña debe tener como mínimo 8 caracteres y \nun máximo de 15. También debe comenzar por una letra";
                lblErrorMessage.Visible = true;
            }

            if (!txtConfirmarPassword.Text.Equals(txtNuevoPassword.Text)) {
                resp = false;
                lblErrorMessage.Text = "Los campos no coinciden";
                lblErrorMessage.Visible = true;
            }

            
            return resp;
        }
        
        private void guardarFormulario() {
            usuario.setPassword(txtNuevoPassword.Text);
            RespuestaControl resp = UsuarioControl.guardarUsuario(usuario);
            if (resp.getSuccess())
            {
                MessageBox.Show("Contraseña Cambiada Correctamente");
                this.DialogResult = DialogResult.OK;
            }
            else {
                MessageBox.Show("Ocurrio un error al guardar la contraseña.");
            }
            
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
