﻿namespace ModuloAdministracion
{
    partial class FormConfiguracion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCambiarPassword = new System.Windows.Forms.Button();
            this.btnReporteEntiFin = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnEstablecerDescuento = new System.Windows.Forms.Button();
            this.btnVerLog = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnCambiarPassword
            // 
            this.btnCambiarPassword.Location = new System.Drawing.Point(26, 61);
            this.btnCambiarPassword.Name = "btnCambiarPassword";
            this.btnCambiarPassword.Size = new System.Drawing.Size(166, 46);
            this.btnCambiarPassword.TabIndex = 2;
            this.btnCambiarPassword.Text = "Cambiar Password";
            this.btnCambiarPassword.UseVisualStyleBackColor = true;
            this.btnCambiarPassword.Click += new System.EventHandler(this.btnCambiarPassword_Click);
            // 
            // btnReporteEntiFin
            // 
            this.btnReporteEntiFin.Image = global::ModuloAdministracion.Properties.Resources.pdf_32;
            this.btnReporteEntiFin.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnReporteEntiFin.Location = new System.Drawing.Point(217, 113);
            this.btnReporteEntiFin.Name = "btnReporteEntiFin";
            this.btnReporteEntiFin.Size = new System.Drawing.Size(166, 46);
            this.btnReporteEntiFin.TabIndex = 6;
            this.btnReporteEntiFin.Text = "Detalle Entidades Financieras";
            this.btnReporteEntiFin.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnReporteEntiFin.UseVisualStyleBackColor = true;
            this.btnReporteEntiFin.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Image = global::ModuloAdministracion.Properties.Resources.pdf_32;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(217, 165);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(166, 46);
            this.button1.TabIndex = 5;
            this.button1.Text = "Resumen de Entidades Financieras";
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnEstablecerDescuento
            // 
            this.btnEstablecerDescuento.Image = global::ModuloAdministracion.Properties.Resources.offer_32;
            this.btnEstablecerDescuento.Location = new System.Drawing.Point(26, 165);
            this.btnEstablecerDescuento.Name = "btnEstablecerDescuento";
            this.btnEstablecerDescuento.Size = new System.Drawing.Size(166, 46);
            this.btnEstablecerDescuento.TabIndex = 4;
            this.btnEstablecerDescuento.Text = "Establecer Descuento";
            this.btnEstablecerDescuento.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnEstablecerDescuento.UseVisualStyleBackColor = true;
            this.btnEstablecerDescuento.Click += new System.EventHandler(this.btnEstablecerDescuento_Click);
            // 
            // btnVerLog
            // 
            this.btnVerLog.Image = global::ModuloAdministracion.Properties.Resources.log_zoom_36;
            this.btnVerLog.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnVerLog.Location = new System.Drawing.Point(26, 113);
            this.btnVerLog.Name = "btnVerLog";
            this.btnVerLog.Size = new System.Drawing.Size(166, 46);
            this.btnVerLog.TabIndex = 3;
            this.btnVerLog.Text = "Ver Log del Sistema";
            this.btnVerLog.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnVerLog.UseVisualStyleBackColor = true;
            this.btnVerLog.Click += new System.EventHandler(this.btnVerLog_Click);
            // 
            // FormConfiguracion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(609, 382);
            this.Controls.Add(this.btnReporteEntiFin);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnEstablecerDescuento);
            this.Controls.Add(this.btnVerLog);
            this.Controls.Add(this.btnCambiarPassword);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormConfiguracion";
            this.Text = "FormConfiguracion";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCambiarPassword;
        private System.Windows.Forms.Button btnVerLog;
        private System.Windows.Forms.Button btnEstablecerDescuento;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnReporteEntiFin;
    }
}