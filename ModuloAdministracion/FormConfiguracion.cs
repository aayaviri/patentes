﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Modelo;
using Control;

namespace ModuloAdministracion
{
    public partial class FormConfiguracion : Form
    {
        private Usuario usuario;
        private Gestion gestionActual;

        public FormConfiguracion(Gestion gestionActual, Usuario usuario)
        {
            InitializeComponent();
            this.usuario = usuario;
            this.gestionActual = gestionActual;
        }

        private void btnEstablecerDescuento_Click(object sender, EventArgs e)
        {
            FormDescuento formDescuento = new FormDescuento(this.gestionActual);
            formDescuento.ShowDialog();
        }

        private void btnVerLog_Click(object sender, EventArgs e)
        {
            FormReporteLog formLog = new FormReporteLog();
            formLog.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FormReporteEntidadFinanciera formReporte = new FormReporteEntidadFinanciera();
            formReporte.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FormReporteResumenEntidades formResumenEntidades = new FormReporteResumenEntidades();
            formResumenEntidades.ShowDialog();
        }

        private void btnCambiarPassword_Click(object sender, EventArgs e)
        {
            FormCambioPassword formCabioPass = new FormCambioPassword(this.usuario);
            formCabioPass.ShowDialog();
        }
    }
}
