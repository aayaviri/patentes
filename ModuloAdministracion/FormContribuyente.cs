﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Control;
using Modelo;
using System.Text.RegularExpressions;

namespace ModuloAdministracion
{
    public partial class FormContribuyente : Form
    {
        private Contribuyente contribuyente;
        private Boolean passwordChanged = false;
        private Cajero cajero;

        public FormContribuyente()
        {
            InitializeComponent();
            this.contribuyente = new Contribuyente();
            this.cajero = new Cajero();
        }

        public FormContribuyente(Contribuyente contribuyente)
        {
            InitializeComponent();
            this.contribuyente = contribuyente;
            loadModelToForm();
            passwordChanged = true;
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            RespuestaControl validacion = validarFormulario();

            if (validacion.getSuccess())
            {
                guardarFormulario();
            }
            else
            {
                lblErrorMessage.Text = validacion.getMessage();
                lblErrorMessage.Visible = true;
            }
        }

        private void loadModelToForm()
        {
            txtCi.Text = contribuyente.getCi();
            txtDireccion.Text = contribuyente.getDireccion();
            txtNombre.Text = contribuyente.getNombre();
            txtApellidoPaterno.Text = contribuyente.getApellidoPaterno();
            txtApellidoMaterno.Text = contribuyente.getApellidoMaterno();
            txtPassword.Text = contribuyente.getPassword();
        }

        private void loadFormToModel()
        {
            contribuyente.setCi(txtCi.Text);
            contribuyente.setDireccion(txtDireccion.Text);
            contribuyente.setNombre(txtNombre.Text);
            contribuyente.setApellidoPaterno(txtApellidoPaterno.Text);
            contribuyente.setApellidoMaterno(txtApellidoMaterno.Text);
            contribuyente.setPassword(txtPassword.Text);
        }

        private RespuestaControl validarFormulario()
        {
            RespuestaControl resp = new RespuestaControl();

            if (String.IsNullOrEmpty(txtCi.Text) || String.IsNullOrWhiteSpace(txtCi.Text))
            {
                resp.setSuccess(false);
                resp.setMessage(resp.getMessage() + "La Cedula de Identidad es requerido.\n");
            }
            if (String.IsNullOrEmpty(txtNombre.Text) || String.IsNullOrWhiteSpace(txtNombre.Text))
            {
                resp.setSuccess(false);
                resp.setMessage(resp.getMessage() + "El Nombre es requerido.\n");
            }
            if (String.IsNullOrEmpty(txtApellidoPaterno.Text) || String.IsNullOrWhiteSpace(txtApellidoPaterno.Text))
            {
                resp.setSuccess(false);
                resp.setMessage(resp.getMessage() + "El Apellido Paterno es requerido.\n");
            }
            if (String.IsNullOrEmpty(txtApellidoMaterno.Text) || String.IsNullOrWhiteSpace(txtApellidoMaterno.Text))
            {
                resp.setSuccess(false);
                resp.setMessage(resp.getMessage() + "El Apellido Materno es requerido.\n");
            }
            if (String.IsNullOrEmpty(txtDireccion.Text) || String.IsNullOrWhiteSpace(txtDireccion.Text))
            {
                resp.setSuccess(false);
                resp.setMessage(resp.getMessage() + "La direccion es requerido.\n");
            }
            if ( contribuyente.getId() == 0 || !passwordChanged)
            {
                Regex r = new Regex("^[a-zA-Z]\\w{8,15}$");
                if (!r.IsMatch(txtPassword.Text))
                {
                    resp.setSuccess(false);
                    resp.setMessage(resp.getMessage() + "La contraseña debe tener como mínimo 8 caracteres y nun máximo de 15. También debe comenzar por una letra");
                }

            }
        
            return resp;
        }

        public string nombre;
        private void guardarFormulario()
        {
            loadFormToModel();
            RespuestaControl resp = ContribuyenteControl.guardarContribuyente(this.contribuyente);
            if (resp.getSuccess())
            {
                nombre = txtNombre.Text;
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                MessageBox.Show(resp.getMessage());
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtPassword_TextChanged(object sender, EventArgs e)
        {
            this.passwordChanged = false;
        }
    }
}
