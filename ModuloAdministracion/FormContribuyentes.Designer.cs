﻿namespace ModuloAdministracion
{
    partial class FormContribuyentes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvContribuyentes = new System.Windows.Forms.DataGridView();
            this.txtBuscarContribuyente = new System.Windows.Forms.TextBox();
            this.btnLimpiar = new System.Windows.Forms.Button();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.btnModificar = new System.Windows.Forms.Button();
            this.btnNuevo = new System.Windows.Forms.Button();
            this.btnBuscarContribuyente = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvContribuyentes)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvContribuyentes
            // 
            this.dgvContribuyentes.AllowUserToAddRows = false;
            this.dgvContribuyentes.AllowUserToDeleteRows = false;
            this.dgvContribuyentes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvContribuyentes.Location = new System.Drawing.Point(12, 48);
            this.dgvContribuyentes.MultiSelect = false;
            this.dgvContribuyentes.Name = "dgvContribuyentes";
            this.dgvContribuyentes.ReadOnly = true;
            this.dgvContribuyentes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvContribuyentes.Size = new System.Drawing.Size(587, 142);
            this.dgvContribuyentes.TabIndex = 0;
            // 
            // txtBuscarContribuyente
            // 
            this.txtBuscarContribuyente.Location = new System.Drawing.Point(12, 17);
            this.txtBuscarContribuyente.Name = "txtBuscarContribuyente";
            this.txtBuscarContribuyente.Size = new System.Drawing.Size(449, 20);
            this.txtBuscarContribuyente.TabIndex = 1;
            this.txtBuscarContribuyente.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBuscarContribuyente_KeyDown);
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.Image = global::ModuloAdministracion.Properties.Resources.Clear;
            this.btnLimpiar.Location = new System.Drawing.Point(555, 12);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(36, 31);
            this.btnLimpiar.TabIndex = 6;
            this.btnLimpiar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnLimpiar.UseVisualStyleBackColor = true;
            this.btnLimpiar.Click += new System.EventHandler(this.btnLimpiar_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.Image = global::ModuloAdministracion.Properties.Resources.Delete_24;
            this.btnEliminar.Location = new System.Drawing.Point(188, 196);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(82, 31);
            this.btnEliminar.TabIndex = 5;
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnEliminar.UseVisualStyleBackColor = true;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // btnModificar
            // 
            this.btnModificar.Image = global::ModuloAdministracion.Properties.Resources.Modify_24;
            this.btnModificar.Location = new System.Drawing.Point(100, 196);
            this.btnModificar.Name = "btnModificar";
            this.btnModificar.Size = new System.Drawing.Size(82, 31);
            this.btnModificar.TabIndex = 4;
            this.btnModificar.Text = "Modificar";
            this.btnModificar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnModificar.UseVisualStyleBackColor = true;
            this.btnModificar.Click += new System.EventHandler(this.btnModificar_Click);
            // 
            // btnNuevo
            // 
            this.btnNuevo.Image = global::ModuloAdministracion.Properties.Resources.Add_24;
            this.btnNuevo.Location = new System.Drawing.Point(12, 196);
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(82, 31);
            this.btnNuevo.TabIndex = 3;
            this.btnNuevo.Text = "Nuevo";
            this.btnNuevo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnNuevo.UseVisualStyleBackColor = true;
            this.btnNuevo.Click += new System.EventHandler(this.btnNuevo_Click);
            // 
            // btnBuscarContribuyente
            // 
            this.btnBuscarContribuyente.Image = global::ModuloAdministracion.Properties.Resources.Search_24;
            this.btnBuscarContribuyente.Location = new System.Drawing.Point(467, 12);
            this.btnBuscarContribuyente.Name = "btnBuscarContribuyente";
            this.btnBuscarContribuyente.Size = new System.Drawing.Size(82, 31);
            this.btnBuscarContribuyente.TabIndex = 2;
            this.btnBuscarContribuyente.Text = "Buscar";
            this.btnBuscarContribuyente.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnBuscarContribuyente.UseVisualStyleBackColor = true;
            this.btnBuscarContribuyente.Click += new System.EventHandler(this.btnBuscarContribuyente_Click);
            // 
            // FormContribuyentes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(611, 374);
            this.Controls.Add(this.btnLimpiar);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.btnModificar);
            this.Controls.Add(this.btnNuevo);
            this.Controls.Add(this.btnBuscarContribuyente);
            this.Controls.Add(this.txtBuscarContribuyente);
            this.Controls.Add(this.dgvContribuyentes);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormContribuyentes";
            this.Text = "FormSitiosMunicipales";
            ((System.ComponentModel.ISupportInitialize)(this.dgvContribuyentes)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvContribuyentes;
        private System.Windows.Forms.TextBox txtBuscarContribuyente;
        private System.Windows.Forms.Button btnBuscarContribuyente;
        private System.Windows.Forms.Button btnNuevo;
        private System.Windows.Forms.Button btnModificar;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.Button btnLimpiar;
    }
}