﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

using Control;
using Modelo;

namespace ModuloAdministracion
{
    public partial class FormContribuyentes : Form
    {
        public FormContribuyentes()
        {
            InitializeComponent();
        }

        private void loadDgvContribuyentes(String searchText)
        {
            if (searchText.Length >= 4)
            {
                DataTable dataTable = ContribuyenteControl.buscarContribuyentes(searchText);
                dgvContribuyentes.DataSource = dataTable;
                dgvContribuyentes.Columns["id"].Visible = false;
                dgvContribuyentes.Columns["password"].Visible = false;
                dgvContribuyentes.Refresh();
            }
            else {
                MessageBox.Show("El texto a buscar debe tener al menos 4 caracteres.");
            }
        }

        private void mostrarFormNuevo()
        {
            FormContribuyente formContribuyente = new FormContribuyente();
            formContribuyente.ShowDialog();
            if (formContribuyente.DialogResult.Equals(DialogResult.OK))
            {
                loadDgvContribuyentes(formContribuyente.nombre);
            }
        }

        private void mostrarFormModificar()
        {
            if (dgvContribuyentes.SelectedRows.Count == 1)
            {
                int idContribuyente = (int)dgvContribuyentes.SelectedRows[0].Cells[0].Value;
                Contribuyente contribuyente = ContribuyenteControl.getContribuyenteById(idContribuyente);
                FormContribuyente formContribuyente = new FormContribuyente(contribuyente);
                formContribuyente.ShowDialog();
                if (formContribuyente.DialogResult.Equals(DialogResult.OK))
                {
                    loadDgvContribuyentes(formContribuyente.nombre);
                }
            }
            else
            {
                MessageBox.Show("Debe seleccionar un registro primero");
            }
        }

        private void mostrarFormEliminar() {
            if (dgvContribuyentes.SelectedRows.Count == 1)
            {
                DialogResult eliminar = MessageBox.Show("Esta seguro que desea eliminar el contribuyente seleccionado?", "Confirmación", MessageBoxButtons.YesNo);
                if (eliminar.Equals(DialogResult.Yes))
                {
                    int idContribuyente = (int)dgvContribuyentes.SelectedRows[0].Cells[0].Value;
                    Contribuyente contribuyente = ContribuyenteControl.getContribuyenteById(idContribuyente);
                    RespuestaControl resp = ContribuyenteControl.eliminarContribuyente(contribuyente);
                    if (resp.getSuccess())
                    {
                        loadDgvContribuyentes(txtBuscarContribuyente.Text);
                    }
                    else {
                        MessageBox.Show(resp.getMessage());
                    }
                }
            }
            else {
                MessageBox.Show("Debe seleccionar un registro primero.");
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            mostrarFormNuevo();
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            mostrarFormModificar();
        }

        private void btnBuscarContribuyente_Click(object sender, EventArgs e)
        {
            loadDgvContribuyentes(txtBuscarContribuyente.Text);
        }

        private void txtBuscarContribuyente_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData.Equals(Keys.Return))
            {
                loadDgvContribuyentes(txtBuscarContribuyente.Text);
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            mostrarFormEliminar();
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            txtBuscarContribuyente.Text = "";
            if (dgvContribuyentes.DataSource != null)
            {
                ((DataTable)dgvContribuyentes.DataSource).Rows.Clear();
            }
        
        }
    }
}
