﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Modelo;
using Control;

namespace ModuloAdministracion
{
    public partial class FormDescuento : Form
    {
        private Gestion gestionActual;

        public FormDescuento(Gestion gestionActual)
        {
            InitializeComponent();
            this.gestionActual = gestionActual;
            loadModelToForm();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            RespuestaControl validacion = validarFormulario();

            if (validacion.getSuccess())
            {
                guardarFormulario();
            }
            else
            {
                lblErrorMessage.Text = validacion.getMessage();
                lblErrorMessage.Visible = true;
            }
        }

        private void guardarFormulario()
        {
            loadFormToModel();
            RespuestaControl resp = GestionControl.guardarGestion(gestionActual);
            if (resp.getSuccess())
            {

                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                MessageBox.Show(resp.getMessage());
            }
        }

        private RespuestaControl validarFormulario()
        {
            RespuestaControl resp = new RespuestaControl();
            Double result;

            if (String.IsNullOrEmpty(txtDescuentoAplicado.Text) || String.IsNullOrWhiteSpace(txtDescuentoAplicado.Text))
            {
                resp.setSuccess(false);
                resp.setMessage(resp.getMessage() + "El código es requerido.\n");
            }
            else if (!Double.TryParse(txtDescuentoAplicado.Text, out result))
            {
                resp.setSuccess(false);
                resp.setMessage("Debe introducir un número.\n");
            }
            if (dtpFechaLimite.Value.Year < gestionActual.getGestion())
            {
                resp.setSuccess(false);
                resp.setMessage("La fecha seleccionada debe estar dentro de la gestion actual.\n");
            }

            return resp;
        }

        private void loadModelToForm()
        {
            txtDescuentoAplicado.Text = gestionActual.getDescuentoAplicado().ToString();
            dtpFechaLimite.Value = gestionActual.getFechaLimiteDescuentoAplicado();
        }

        private void loadFormToModel()
        {
            gestionActual.setDescuentoAplicado(Double.Parse(txtDescuentoAplicado.Text));
            gestionActual.setFechaLimiteDescuentoAplicado(dtpFechaLimite.Value);
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
