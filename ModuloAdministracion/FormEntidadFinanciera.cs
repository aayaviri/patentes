﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Modelo;
using Control;

namespace ModuloAdministracion
{
    public partial class FormEntidadFinanciera : Form
    {
        private EntidadFinanciera entiFin;
        private Usuario usuario;
        private Cajero cajero;

        public FormEntidadFinanciera()
        {
            InitializeComponent();
            this.entiFin = new EntidadFinanciera();
            this.usuario = new Usuario();
            this.cajero = new Cajero();
        }

        public FormEntidadFinanciera(EntidadFinanciera entiFin)
        {
            InitializeComponent();
            this.entiFin = entiFin;
            loadModelToForm();
        }

        private void loadModelToForm()
        {
            txtNit.Text = entiFin.getNit();
            txtNombre.Text = entiFin.getNombre();
            txtMaxCajeros.Text = entiFin.getMaxCajeros().ToString();
            cbxEstado.SelectedItem = entiFin.getEstado();
        }

        private void loadFormToModel()
        {
            entiFin.setNit(txtNit.Text);
            entiFin.setNombre(txtNombre.Text);
            entiFin.setMaxCajeros(int.Parse(txtMaxCajeros.Text));
            entiFin.setEstado(cbxEstado.SelectedItem.ToString());
        }
        private RespuestaControl validarFormulario()
        {
            RespuestaControl resp = new RespuestaControl();

            if (String.IsNullOrEmpty(txtNit.Text) || String.IsNullOrWhiteSpace(txtNit.Text))
            {
                resp.setSuccess(false);
                resp.setMessage(resp.getMessage() + "El nit es requerido.\n");
            }

            if (String.IsNullOrEmpty(txtNombre.Text) || String.IsNullOrWhiteSpace(txtNombre.Text))
            {
                resp.setSuccess(false);
                resp.setMessage(resp.getMessage() + "El nombre es requerido.\n");
            }

            if (String.IsNullOrEmpty(txtMaxCajeros.Text) || String.IsNullOrWhiteSpace(txtMaxCajeros.Text))
            {
                resp.setSuccess(false);
                resp.setMessage(resp.getMessage() + "El numero maximo de cajeros es requerido.\n");
            }

            if (cbxEstado.SelectedItem == null || String.IsNullOrEmpty(cbxEstado.SelectedItem.ToString()) || String.IsNullOrWhiteSpace(cbxEstado.SelectedItem.ToString()))
            {
                resp.setSuccess(false);
                resp.setMessage(resp.getMessage() + "El estado es requerido.\n");
            }
            return resp;
        }

        private void guardarFormulario() {
            loadFormToModel();
            if (EntidadFinancieraControl.guardarEntidadFinanciera(this.entiFin))
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else {
                MessageBox.Show("Ocurrio un error al guardar la Entidad Financiera");
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            RespuestaControl validacion = validarFormulario();

            if (validacion.getSuccess())
            {
                guardarFormulario();
            }
            else
            {
                lblErrorMessage.Text = validacion.getMessage();
                lblErrorMessage.Visible = true;
            }
        }
    }
}
