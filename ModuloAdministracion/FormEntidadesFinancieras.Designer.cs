﻿namespace ModuloAdministracion
{
    partial class FormEntidadesFinancieras
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvEntidadesFinancieras = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnActualizarEntiFin = new System.Windows.Forms.Button();
            this.btnNuevaEntidadFinanciera = new System.Windows.Forms.Button();
            this.btnEliminarEntiFin = new System.Windows.Forms.Button();
            this.btnModificarEntiFin = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnActualizarAdmin = new System.Windows.Forms.Button();
            this.btnModificarAdmin = new System.Windows.Forms.Button();
            this.btnNuevoAdmin = new System.Windows.Forms.Button();
            this.dgvAdminEntiFin = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEntidadesFinancieras)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAdminEntiFin)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvEntidadesFinancieras
            // 
            this.dgvEntidadesFinancieras.AllowUserToAddRows = false;
            this.dgvEntidadesFinancieras.AllowUserToDeleteRows = false;
            this.dgvEntidadesFinancieras.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEntidadesFinancieras.Location = new System.Drawing.Point(6, 19);
            this.dgvEntidadesFinancieras.MultiSelect = false;
            this.dgvEntidadesFinancieras.Name = "dgvEntidadesFinancieras";
            this.dgvEntidadesFinancieras.ReadOnly = true;
            this.dgvEntidadesFinancieras.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvEntidadesFinancieras.Size = new System.Drawing.Size(575, 125);
            this.dgvEntidadesFinancieras.TabIndex = 1;
            this.dgvEntidadesFinancieras.SelectionChanged += new System.EventHandler(this.dgvEntidadesFinancieras_SelectionChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnActualizarEntiFin);
            this.groupBox1.Controls.Add(this.btnNuevaEntidadFinanciera);
            this.groupBox1.Controls.Add(this.dgvEntidadesFinancieras);
            this.groupBox1.Controls.Add(this.btnEliminarEntiFin);
            this.groupBox1.Controls.Add(this.btnModificarEntiFin);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(587, 187);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Entidades Financieras";
            // 
            // btnActualizarEntiFin
            // 
            this.btnActualizarEntiFin.Image = global::ModuloAdministracion.Properties.Resources.refresh_blue_24;
            this.btnActualizarEntiFin.Location = new System.Drawing.Point(496, 150);
            this.btnActualizarEntiFin.Name = "btnActualizarEntiFin";
            this.btnActualizarEntiFin.Size = new System.Drawing.Size(85, 31);
            this.btnActualizarEntiFin.TabIndex = 5;
            this.btnActualizarEntiFin.Text = "Actualizar";
            this.btnActualizarEntiFin.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnActualizarEntiFin.UseVisualStyleBackColor = true;
            this.btnActualizarEntiFin.Click += new System.EventHandler(this.btnActualizarEntiFin_Click);
            // 
            // btnNuevaEntidadFinanciera
            // 
            this.btnNuevaEntidadFinanciera.Image = global::ModuloAdministracion.Properties.Resources.Add_24;
            this.btnNuevaEntidadFinanciera.Location = new System.Drawing.Point(6, 150);
            this.btnNuevaEntidadFinanciera.Name = "btnNuevaEntidadFinanciera";
            this.btnNuevaEntidadFinanciera.Size = new System.Drawing.Size(82, 31);
            this.btnNuevaEntidadFinanciera.TabIndex = 2;
            this.btnNuevaEntidadFinanciera.Text = "Nuevo";
            this.btnNuevaEntidadFinanciera.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnNuevaEntidadFinanciera.UseVisualStyleBackColor = true;
            this.btnNuevaEntidadFinanciera.Click += new System.EventHandler(this.btnNuevaEntidadFinanciera_Click);
            // 
            // btnEliminarEntiFin
            // 
            this.btnEliminarEntiFin.Image = global::ModuloAdministracion.Properties.Resources.Delete_24;
            this.btnEliminarEntiFin.Location = new System.Drawing.Point(182, 150);
            this.btnEliminarEntiFin.Name = "btnEliminarEntiFin";
            this.btnEliminarEntiFin.Size = new System.Drawing.Size(82, 31);
            this.btnEliminarEntiFin.TabIndex = 4;
            this.btnEliminarEntiFin.Text = "Eliminar";
            this.btnEliminarEntiFin.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnEliminarEntiFin.UseVisualStyleBackColor = true;
            this.btnEliminarEntiFin.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // btnModificarEntiFin
            // 
            this.btnModificarEntiFin.Image = global::ModuloAdministracion.Properties.Resources.Modify_24;
            this.btnModificarEntiFin.Location = new System.Drawing.Point(94, 150);
            this.btnModificarEntiFin.Name = "btnModificarEntiFin";
            this.btnModificarEntiFin.Size = new System.Drawing.Size(82, 31);
            this.btnModificarEntiFin.TabIndex = 3;
            this.btnModificarEntiFin.Text = "Modificar";
            this.btnModificarEntiFin.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnModificarEntiFin.UseVisualStyleBackColor = true;
            this.btnModificarEntiFin.Click += new System.EventHandler(this.btnModificar_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnActualizarAdmin);
            this.groupBox2.Controls.Add(this.btnModificarAdmin);
            this.groupBox2.Controls.Add(this.btnNuevoAdmin);
            this.groupBox2.Controls.Add(this.dgvAdminEntiFin);
            this.groupBox2.Location = new System.Drawing.Point(12, 205);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(587, 135);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Administradores de Entidad Financiera";
            // 
            // btnActualizarAdmin
            // 
            this.btnActualizarAdmin.Image = global::ModuloAdministracion.Properties.Resources.refresh_blue_24;
            this.btnActualizarAdmin.Location = new System.Drawing.Point(496, 98);
            this.btnActualizarAdmin.Name = "btnActualizarAdmin";
            this.btnActualizarAdmin.Size = new System.Drawing.Size(85, 31);
            this.btnActualizarAdmin.TabIndex = 6;
            this.btnActualizarAdmin.Text = "Actualizar";
            this.btnActualizarAdmin.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnActualizarAdmin.UseVisualStyleBackColor = true;
            this.btnActualizarAdmin.Click += new System.EventHandler(this.btnActualizarAdmin_Click);
            // 
            // btnModificarAdmin
            // 
            this.btnModificarAdmin.Image = global::ModuloAdministracion.Properties.Resources.Modify_24;
            this.btnModificarAdmin.Location = new System.Drawing.Point(94, 98);
            this.btnModificarAdmin.Name = "btnModificarAdmin";
            this.btnModificarAdmin.Size = new System.Drawing.Size(82, 31);
            this.btnModificarAdmin.TabIndex = 2;
            this.btnModificarAdmin.Text = "Modificar";
            this.btnModificarAdmin.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnModificarAdmin.UseVisualStyleBackColor = true;
            this.btnModificarAdmin.Click += new System.EventHandler(this.btnModificarAdmin_Click);
            // 
            // btnNuevoAdmin
            // 
            this.btnNuevoAdmin.Image = global::ModuloAdministracion.Properties.Resources.Add_24;
            this.btnNuevoAdmin.Location = new System.Drawing.Point(6, 98);
            this.btnNuevoAdmin.Name = "btnNuevoAdmin";
            this.btnNuevoAdmin.Size = new System.Drawing.Size(82, 31);
            this.btnNuevoAdmin.TabIndex = 1;
            this.btnNuevoAdmin.Text = "Nuevo";
            this.btnNuevoAdmin.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnNuevoAdmin.UseVisualStyleBackColor = true;
            this.btnNuevoAdmin.Click += new System.EventHandler(this.btnNuevoAdmin_Click);
            // 
            // dgvAdminEntiFin
            // 
            this.dgvAdminEntiFin.AllowUserToAddRows = false;
            this.dgvAdminEntiFin.AllowUserToDeleteRows = false;
            this.dgvAdminEntiFin.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAdminEntiFin.Location = new System.Drawing.Point(6, 19);
            this.dgvAdminEntiFin.MultiSelect = false;
            this.dgvAdminEntiFin.Name = "dgvAdminEntiFin";
            this.dgvAdminEntiFin.ReadOnly = true;
            this.dgvAdminEntiFin.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAdminEntiFin.Size = new System.Drawing.Size(575, 73);
            this.dgvAdminEntiFin.TabIndex = 0;
            // 
            // FormEntidadesFinancieras
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(611, 351);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormEntidadesFinancieras";
            this.Text = "FormEntidadesFinancieras";
            ((System.ComponentModel.ISupportInitialize)(this.dgvEntidadesFinancieras)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAdminEntiFin)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvEntidadesFinancieras;
        private System.Windows.Forms.Button btnNuevaEntidadFinanciera;
        private System.Windows.Forms.Button btnModificarEntiFin;
        private System.Windows.Forms.Button btnEliminarEntiFin;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dgvAdminEntiFin;
        private System.Windows.Forms.Button btnModificarAdmin;
        private System.Windows.Forms.Button btnNuevoAdmin;
        private System.Windows.Forms.Button btnActualizarEntiFin;
        private System.Windows.Forms.Button btnActualizarAdmin;
    }
}