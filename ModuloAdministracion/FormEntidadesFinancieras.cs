﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Modelo;
using Control;

namespace ModuloAdministracion
{
    public partial class FormEntidadesFinancieras : Form
    {
        private EntidadFinanciera entiFin;

        public FormEntidadesFinancieras()
        {
            InitializeComponent();
            loadEntidadesFinancieras();
        }

        private void btnNuevaEntidadFinanciera_Click(object sender, EventArgs e)
        {
            mostrarFormNuevo();
        }

        private void loadEntidadesFinancieras() {
            DataTable dataTable = EntidadFinancieraControl.cargarEntidadesFinancieras();
            dgvEntidadesFinancieras.DataSource = dataTable;
            dgvEntidadesFinancieras.Columns["id"].Visible = false;
            dgvEntidadesFinancieras.Refresh();
        }

        private void loadAdminEntidadFinanciera() {
            if (dgvEntidadesFinancieras.SelectedRows.Count > 0)
            {
                int index = (int)dgvEntidadesFinancieras.SelectedRows[0].Index;
                DataTable dtEntiFin = (DataTable)dgvEntidadesFinancieras.DataSource;
                EntidadFinancieraDao entiFinDao = new EntidadFinancieraDao();
                this.entiFin = entiFinDao.loadDataRowToModel(dtEntiFin.Rows[index]);
                
                DataTable dataTable = AdminEntidadFinancieraControl.getAdminEntidadFinancieraByIdEntidadFinanciera(this.entiFin.getId());
                dgvAdminEntiFin.DataSource = dataTable;
                dgvAdminEntiFin.Columns["id"].Visible = false;
                dgvAdminEntiFin.Columns["idEntidadFinanciera"].Visible = false;
                dgvAdminEntiFin.Columns["idUsuario"].Visible = false;
                dgvAdminEntiFin.Refresh();
            }
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            mostrarFormModificar();
        }

        private void mostrarFormNuevo() {
            FormEntidadFinanciera formEntiFin = new FormEntidadFinanciera();
            formEntiFin.ShowDialog();
            if (formEntiFin.DialogResult.Equals(DialogResult.OK))
            {
                loadEntidadesFinancieras();
            }
        }

        private void mostrarFormModificar() {
            if (dgvEntidadesFinancieras.SelectedRows.Count == 1)
            {
                int idEntiFin = (int)dgvEntidadesFinancieras.SelectedRows[0].Cells[0].Value;
                EntidadFinanciera entiFin = EntidadFinancieraControl.getEntidadFinancieraById(idEntiFin);
                FormEntidadFinanciera formEntiFin = new FormEntidadFinanciera(entiFin);
                formEntiFin.ShowDialog();
                if (formEntiFin.DialogResult.Equals(DialogResult.OK))
                {
                    loadEntidadesFinancieras();
                }
            }
            else
            {
                MessageBox.Show("Debe seleccionar un registro primero");
            }
        }

        private void mostrarFormEliminar()
        {
            if (dgvEntidadesFinancieras.SelectedRows.Count == 1)
            {
                DialogResult eliminar = MessageBox.Show("Esta seguro que desea eliminar la Entidad Financiera seleccionada?", "Confirmación", MessageBoxButtons.YesNo);
                if (eliminar.Equals(DialogResult.Yes))
                {
                    int idEntiFin = (int)dgvEntidadesFinancieras.SelectedRows[0].Cells[0].Value;
                    EntidadFinanciera entiFin = EntidadFinancieraControl.getEntidadFinancieraById(idEntiFin);
                    RespuestaControl resp = EntidadFinancieraControl.eliminarEntidadFinanciera(entiFin);
                    if (resp.getSuccess())
                    {
                        loadEntidadesFinancieras();
                    }
                    else
                    {
                        MessageBox.Show(resp.getMessage());
                    }
                }
            }
            else
            {
                MessageBox.Show("Debe seleccionar un registro primero.");
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            mostrarFormEliminar();
        }

        private void dgvEntidadesFinancieras_SelectionChanged(object sender, EventArgs e)
        {
            loadAdminEntidadFinanciera();
        }

        private void btnNuevoAdmin_Click(object sender, EventArgs e)
        {
            mostrarFormNuevoAdmin();
        }

        private void mostrarFormNuevoAdmin()
        {
            if (dgvAdminEntiFin.Rows.Count < 1)
            {
                FormAdmin formAdmin = new FormAdmin(entiFin);
                formAdmin.ShowDialog();
                if (formAdmin.DialogResult.Equals(DialogResult.OK))
                {
                    loadAdminEntidadFinanciera();
                }
            }
            else {
                MessageBox.Show("Solo puede registrar un administrador por Entidad Financiera");
            }
        }

        private void mostrarFormModificarAdmin()
        {
            if (dgvAdminEntiFin.SelectedRows.Count == 1)
            {
                int idAdmin = (int)dgvAdminEntiFin.SelectedRows[0].Cells[0].Value;
                AdminEntidadFinanciera admin = AdminEntidadFinancieraControl.getById(idAdmin);

                FormAdmin formSucursal = new FormAdmin(entiFin, admin);
                formSucursal.ShowDialog();
                if (formSucursal.DialogResult.Equals(DialogResult.OK))
                {
                    loadAdminEntidadFinanciera();
                }
            }
            else
            {
                MessageBox.Show("Debe seleccionar un registro primero");
            }
        }

        private void btnModificarAdmin_Click(object sender, EventArgs e)
        {
            mostrarFormModificarAdmin();
        }

        private void btnActualizarEntiFin_Click(object sender, EventArgs e)
        {
            loadEntidadesFinancieras();
        }

        private void btnActualizarAdmin_Click(object sender, EventArgs e)
        {
            loadAdminEntidadFinanciera();
        }

       
    }
}
