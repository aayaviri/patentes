﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Modelo;
using Control;

namespace ModuloAdministracion
{
    public partial class FormGestion : Form
    {
        private Gestion ultimaGestion;
        private Gestion nuevaGestion;

        public FormGestion(Gestion ultimaGestion, Gestion nuevaGestion)
        {
            InitializeComponent();
            this.ultimaGestion = ultimaGestion;
            this.nuevaGestion = nuevaGestion;
        }

        private void loadModelToForm()
        {
            txtUltimaGestion.Text = ultimaGestion.getGestion().ToString();
            txtNuevaGestion.Text = nuevaGestion.getGestion().ToString();
        }

        private void loadFormToModel()
        {
            ultimaGestion.setUfv(Double.Parse(txtUfvVencida.Text));
            ultimaGestion.setFechaUfv(new DateTime(ultimaGestion.getGestion(), 12, 31));
            ultimaGestion.setMultaIDF(Double.Parse(txtMultaIDF.Text));
            ultimaGestion.setInteresUfv(Double.Parse(txtInteresUfv.Text));

            nuevaGestion.setFechaUfv(new DateTime(nuevaGestion.getGestion(), 01, 01));
        }

        private void guardarFormulario()
        {
            loadFormToModel();
            RespuestaControl resp;
            resp = GestionControl.guardarGestion(this.nuevaGestion);
            if (resp.getSuccess())
            {
                resp = GestionControl.guardarGestion(this.ultimaGestion);
                if (resp.getSuccess())
                {
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                } else {
                    MessageBox.Show(resp.getMessage());
                }
            }
            else
            {
                MessageBox.Show(resp.getMessage());
            }
        }
    }
}
