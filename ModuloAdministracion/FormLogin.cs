﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Control;
using Modelo;
using System.Diagnostics;

namespace ModuloAdministracion
{
    public partial class FormLogin : Form
    {
        private Usuario usuario;

        public FormLogin()
        {
            InitializeComponent();
          //  Helper.ExecuteCommandAsync("-v -ssh -2 -P 443 -N -L 18566:localhost:3306 -pw patentes alcaldia@192.168.3.10");
            usuario = new Usuario();
        }

        private void loadFormToUsuario() {
            usuario.setLogin(txtLogin.Text);
            usuario.setPassword(txtPassword.Text);
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            procesarLogin();
        }

        private void mostrarErrorAutentificado() {
            lblMensajeError.Text = "Nombre de Usuario o contraseña incorrectos.";
            lblMensajeError.Visible = true;
        }

        private void procesarLogin() {
            loadFormToUsuario();
            lblMensajeError.Visible = false;
            validarUsuario(UsuarioControl.validarLogin(this.usuario));
        }

        private void validarUsuario(Usuario usuario) {
            if (usuario == null)
            {
                mostrarErrorAutentificado();
            }
            else {
                //TODO: se debe cambiar la forma de manejar roles
                if (usuario.getRol().Equals("administrador"))
                {
                    FormMain formMain = new FormMain(usuario);
                    this.Hide();
                    formMain.Show();
                }
                else {
                    mostrarErrorAutentificado();
                }
            }
        }

        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData.Equals(Keys.Return))
            {
                procesarLogin();
            }
        }

        private void txtLogin_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData.Equals(Keys.Return))
            {
                procesarLogin();
            }
        }

        private void FormLogin_FormClosed(object sender, FormClosedEventArgs e)
        {
       //     Helper.terminateProcesoPlink();
            
        }

     
    }
}
