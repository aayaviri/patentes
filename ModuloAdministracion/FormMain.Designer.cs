﻿namespace ModuloAdministracion
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblLogin = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.btnMenu6 = new System.Windows.Forms.Button();
            this.btnMenu5 = new System.Windows.Forms.Button();
            this.btnMenu2 = new System.Windows.Forms.Button();
            this.btnMenu1 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblLogin
            // 
            this.lblLogin.AutoSize = true;
            this.lblLogin.Location = new System.Drawing.Point(510, 31);
            this.lblLogin.Name = "lblLogin";
            this.lblLogin.Size = new System.Drawing.Size(83, 13);
            this.lblLogin.TabIndex = 0;
            this.lblLogin.Text = "Nombre Usuario";
            this.lblLogin.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblLogin.Visible = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.btnMenu6);
            this.panel1.Controls.Add(this.btnMenu5);
            this.panel1.Controls.Add(this.btnMenu2);
            this.panel1.Controls.Add(this.btnMenu1);
            this.panel1.Controls.Add(this.lblLogin);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(605, 70);
            this.panel1.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.SystemColors.Control;
            this.label1.Font = new System.Drawing.Font("Trebuchet MS", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.SteelBlue;
            this.label1.Location = new System.Drawing.Point(306, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(299, 70);
            this.label1.TabIndex = 7;
            this.label1.Text = "ADMINISTRACIÓN DE COBRO DE PATENTES";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(267, 417);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(74, 27);
            this.button1.TabIndex = 6;
            this.button1.Text = "Salir";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnMenu6
            // 
            this.btnMenu6.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnMenu6.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnMenu6.Image = global::ModuloAdministracion.Properties.Resources.settings_48;
            this.btnMenu6.Location = new System.Drawing.Point(225, 0);
            this.btnMenu6.Name = "btnMenu6";
            this.btnMenu6.Size = new System.Drawing.Size(75, 70);
            this.btnMenu6.TabIndex = 6;
            this.btnMenu6.UseVisualStyleBackColor = true;
            this.btnMenu6.Click += new System.EventHandler(this.btnMenu6_Click);
            // 
            // btnMenu5
            // 
            this.btnMenu5.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnMenu5.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnMenu5.Image = global::ModuloAdministracion.Properties.Resources.users_48;
            this.btnMenu5.Location = new System.Drawing.Point(150, 0);
            this.btnMenu5.Name = "btnMenu5";
            this.btnMenu5.Size = new System.Drawing.Size(75, 70);
            this.btnMenu5.TabIndex = 5;
            this.btnMenu5.UseVisualStyleBackColor = true;
            this.btnMenu5.Click += new System.EventHandler(this.btnMenu5_Click);
            // 
            // btnMenu2
            // 
            this.btnMenu2.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnMenu2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnMenu2.Image = global::ModuloAdministracion.Properties.Resources.map_48;
            this.btnMenu2.Location = new System.Drawing.Point(75, 0);
            this.btnMenu2.Name = "btnMenu2";
            this.btnMenu2.Size = new System.Drawing.Size(75, 70);
            this.btnMenu2.TabIndex = 2;
            this.btnMenu2.UseVisualStyleBackColor = true;
            this.btnMenu2.Click += new System.EventHandler(this.btnMenu2_Click);
            // 
            // btnMenu1
            // 
            this.btnMenu1.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnMenu1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnMenu1.Image = global::ModuloAdministracion.Properties.Resources.coffee_store_48;
            this.btnMenu1.Location = new System.Drawing.Point(0, 0);
            this.btnMenu1.Name = "btnMenu1";
            this.btnMenu1.Size = new System.Drawing.Size(75, 70);
            this.btnMenu1.TabIndex = 1;
            this.toolTip.SetToolTip(this.btnMenu1, "Entidades Financieras");
            this.btnMenu1.UseVisualStyleBackColor = true;
            this.btnMenu1.Click += new System.EventHandler(this.btnMenu1_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(605, 453);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.IsMdiContainer = true;
            this.MaximizeBox = false;
            this.Name = "FormMain";
            this.Text = "Sistema de Cobro de Patentes - Modulo Administracion";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormMain_FormClosed);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblLogin;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnMenu2;
        private System.Windows.Forms.Button btnMenu1;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.Button btnMenu5;
        private System.Windows.Forms.Button btnMenu6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
    }
}