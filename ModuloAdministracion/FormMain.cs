﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Modelo;
using Control;

namespace ModuloAdministracion
{
    public partial class FormMain : Form
    {
        private Usuario usuario;
        private Gestion gestionActual;

        public FormMain(Usuario usuario)
        {
            InitializeComponent();
            int numGestionActual = DateTime.Today.Year;
            this.gestionActual = GestionControl.getGestionByNumGestion(numGestionActual);
            this.usuario = usuario;
            this.lblLogin.Text = usuario.getLogin();
            iniciarFormulariosMdi();
            MdiChildren[0].Show();
        }

        private void iniciarFormulariosMdi()
        {
            FormEntidadesFinancieras formEntiFin = new FormEntidadesFinancieras();
            formEntiFin.MdiParent = this;
            formEntiFin.Dock = DockStyle.Fill;

            FormSitiosMunicipales formSitios = new FormSitiosMunicipales();
            formSitios.MdiParent = this;
            formSitios.Dock = DockStyle.Fill;

            FormContribuyentes formContribuyentes = new FormContribuyentes();
            formContribuyentes.MdiParent = this;
            formContribuyentes.Dock = DockStyle.Fill;

            FormConfiguracion formConfiguracion = new FormConfiguracion(gestionActual, usuario);
            formConfiguracion.MdiParent = this;
            formConfiguracion.Dock = DockStyle.Fill;

            toolTip.SetToolTip(btnMenu1, "Entidades Financieras");
            toolTip.SetToolTip(btnMenu2, "Sitios Municipales");
            toolTip.SetToolTip(btnMenu5, "Contribuyentes");
            toolTip.SetToolTip(btnMenu6, "Opciones del Sistema");
        }

        private void btnMenu1_Click(object sender, EventArgs e)
        {
            MdiChildren[0].Show();
            MdiChildren[0].Activate();
        }

        private void btnMenu2_Click(object sender, EventArgs e)
        {
            MdiChildren[1].Show();
            MdiChildren[1].Activate();
        }

        private void btnMenu3_Click(object sender, EventArgs e)
        {
            FormReporteEntidadFinanciera formReporte = new FormReporteEntidadFinanciera();
            formReporte.ShowDialog();
        }

        private void FormMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void btnMenu5_Click(object sender, EventArgs e)
        {
            MdiChildren[2].Show();
            MdiChildren[2].Activate();
        }

        private void generarPagos() {
            int numGestionActual = DateTime.Today.Year;
            
            Gestion gestionAnterior = GestionControl.getGestionByNumGestion(numGestionActual - 1);
            if (gestionActual == null)
            {
                gestionActual = new Gestion();
                gestionActual.setGestion(numGestionActual);
                FormGestion formGestion = new FormGestion(gestionAnterior, gestionActual);
                DialogResult respDialog = formGestion.ShowDialog();
                if (respDialog.Equals(DialogResult.OK))
                {
                    GestionControl.generarPagosGestion(gestionActual);
                }
            }
            else
            {
                MessageBox.Show("Los pagos para la gestión " + gestionActual.getGestion() + " ya fueron procesados.");
            }
        }

        private void btnMenu4_Click(object sender, EventArgs e)
        {
            FormReporteResumenEntidades formResumenEntidades = new FormReporteResumenEntidades();
            formResumenEntidades.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult rs = MessageBox.Show("Cerrar el actual usuario?", "Salir del sistema", MessageBoxButtons.YesNo);
            if (rs == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void btnMenu6_Click(object sender, EventArgs e)
        {
            MdiChildren[3].Show();
            
            MdiChildren[3].Activate();
        }
    }
}
