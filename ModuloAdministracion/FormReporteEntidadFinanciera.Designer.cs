﻿namespace ModuloAdministracion
{
    partial class FormReporteEntidadFinanciera
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.reporte_pagadosDataTableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dtpFechaInicio = new System.Windows.Forms.DateTimePicker();
            this.dtpFechaFin = new System.Windows.Forms.DateTimePicker();
            this.lblFechaFin = new System.Windows.Forms.Label();
            this.lblFechaInicio = new System.Windows.Forms.Label();
            this.rpvEntidadFinanciera = new Microsoft.Reporting.WinForms.ReportViewer();
            this.btnVerReporte = new System.Windows.Forms.Button();
            this.cbxEntidadFinanciera = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.reporte_pagadosDataTableBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // reporte_pagadosDataTableBindingSource
            // 
            this.reporte_pagadosDataTableBindingSource.DataSource = typeof(Modelo.cobranza_patentesDataSet.reporte_pagosDataTable);
            // 
            // dtpFechaInicio
            // 
            this.dtpFechaInicio.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFechaInicio.Location = new System.Drawing.Point(83, 12);
            this.dtpFechaInicio.Name = "dtpFechaInicio";
            this.dtpFechaInicio.Size = new System.Drawing.Size(93, 20);
            this.dtpFechaInicio.TabIndex = 0;
            // 
            // dtpFechaFin
            // 
            this.dtpFechaFin.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFechaFin.Location = new System.Drawing.Point(259, 12);
            this.dtpFechaFin.Name = "dtpFechaFin";
            this.dtpFechaFin.Size = new System.Drawing.Size(95, 20);
            this.dtpFechaFin.TabIndex = 1;
            // 
            // lblFechaFin
            // 
            this.lblFechaFin.AutoSize = true;
            this.lblFechaFin.Location = new System.Drawing.Point(199, 16);
            this.lblFechaFin.Name = "lblFechaFin";
            this.lblFechaFin.Size = new System.Drawing.Size(54, 13);
            this.lblFechaFin.TabIndex = 2;
            this.lblFechaFin.Text = "Fecha Fin";
            // 
            // lblFechaInicio
            // 
            this.lblFechaInicio.AutoSize = true;
            this.lblFechaInicio.Location = new System.Drawing.Point(12, 16);
            this.lblFechaInicio.Name = "lblFechaInicio";
            this.lblFechaInicio.Size = new System.Drawing.Size(65, 13);
            this.lblFechaInicio.TabIndex = 3;
            this.lblFechaInicio.Text = "Fecha Inicio";
            // 
            // rpvEntidadFinanciera
            // 
            reportDataSource1.Name = "pagosPagadosDataTable";
            reportDataSource1.Value = this.reporte_pagadosDataTableBindingSource;
            this.rpvEntidadFinanciera.LocalReport.DataSources.Add(reportDataSource1);
            this.rpvEntidadFinanciera.LocalReport.ReportEmbeddedResource = "ModuloAdministracion.ReportEntidadFinanciera.rdlc";
            this.rpvEntidadFinanciera.Location = new System.Drawing.Point(12, 38);
            this.rpvEntidadFinanciera.Name = "rpvEntidadFinanciera";
            this.rpvEntidadFinanciera.Size = new System.Drawing.Size(868, 492);
            this.rpvEntidadFinanciera.TabIndex = 4;
            // 
            // btnVerReporte
            // 
            this.btnVerReporte.Location = new System.Drawing.Point(625, 9);
            this.btnVerReporte.Name = "btnVerReporte";
            this.btnVerReporte.Size = new System.Drawing.Size(75, 23);
            this.btnVerReporte.TabIndex = 5;
            this.btnVerReporte.Text = "Ver Informe";
            this.btnVerReporte.UseVisualStyleBackColor = true;
            this.btnVerReporte.Click += new System.EventHandler(this.btnVerReporte_Click);
            // 
            // cbxEntidadFinanciera
            // 
            this.cbxEntidadFinanciera.FormattingEnabled = true;
            this.cbxEntidadFinanciera.Location = new System.Drawing.Point(479, 11);
            this.cbxEntidadFinanciera.Name = "cbxEntidadFinanciera";
            this.cbxEntidadFinanciera.Size = new System.Drawing.Size(121, 21);
            this.cbxEntidadFinanciera.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(378, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Entidad Financiera";
            // 
            // FormReporteEntidadFinanciera
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(892, 542);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbxEntidadFinanciera);
            this.Controls.Add(this.btnVerReporte);
            this.Controls.Add(this.rpvEntidadFinanciera);
            this.Controls.Add(this.lblFechaInicio);
            this.Controls.Add(this.lblFechaFin);
            this.Controls.Add(this.dtpFechaFin);
            this.Controls.Add(this.dtpFechaInicio);
            this.MaximizeBox = false;
            this.Name = "FormReporteEntidadFinanciera";
            this.Text = "Reporte de Entidad Financiera";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormReporteEntidadFinanciera_FormClosing);
            this.Load += new System.EventHandler(this.FormReporteEntidadFinanciera_Load);
            ((System.ComponentModel.ISupportInitialize)(this.reporte_pagadosDataTableBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dtpFechaInicio;
        private System.Windows.Forms.DateTimePicker dtpFechaFin;
        private System.Windows.Forms.Label lblFechaFin;
        private System.Windows.Forms.Label lblFechaInicio;
        private Microsoft.Reporting.WinForms.ReportViewer rpvEntidadFinanciera;
        private System.Windows.Forms.Button btnVerReporte;
        private System.Windows.Forms.BindingSource reporte_pagadosDataTableBindingSource;
        private System.Windows.Forms.ComboBox cbxEntidadFinanciera;
        private System.Windows.Forms.Label label1;
    }
}