﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Control;
using Modelo;
using Microsoft.Reporting.WinForms;

namespace ModuloAdministracion
{
    public partial class FormReporteEntidadFinanciera : Form
    {

        public FormReporteEntidadFinanciera()
        {
            InitializeComponent();
        }

        private void FormReporteEntidadFinanciera_Load(object sender, EventArgs e)
        {
            loadComboEntidadFinanciera();
        }

        private void btnVerReporte_Click(object sender, EventArgs e)
        {
            loadReporteEntidadFinanciera();
        }

        private void loadComboEntidadFinanciera()
        {
            cbxEntidadFinanciera.DataSource = EntidadFinancieraControl.cargarEntidadesFinancieras();
            cbxEntidadFinanciera.DisplayMember = "nombre";
            cbxEntidadFinanciera.ValueMember = "id";
        }

        private void loadReporteEntidadFinanciera() {
            DateTime fechaInicio = dtpFechaInicio.Value;
            DateTime fechaFin = dtpFechaFin.Value.AddDays(1);
            int idEntidadFinanciera = (int)cbxEntidadFinanciera.SelectedValue;

            DataTable pagosPagados = ReporteControl.reportePagadosIdEntidadFechas(idEntidadFinanciera, fechaInicio.Date, fechaFin.Date);
            DataTable comprobantesAnulados = ReporteControl.getComprobantesAnuladosPorEntidadFinancieraFechas(idEntidadFinanciera, fechaInicio.Date, fechaFin.Date);

            this.rpvEntidadFinanciera.LocalReport.DataSources.Clear();
            ReportDataSource rprtDTSource = new Microsoft.Reporting.WinForms.ReportDataSource("pagosPagadosDataTable", pagosPagados);
            ReportDataSource rprtDTSource2 = new Microsoft.Reporting.WinForms.ReportDataSource("comprobantesAnuladosDataTable", comprobantesAnulados);
            
            this.rpvEntidadFinanciera.LocalReport.DataSources.Add(rprtDTSource);
            this.rpvEntidadFinanciera.LocalReport.DataSources.Add(rprtDTSource2);

            rpvEntidadFinanciera.LocalReport.SetParameters(new ReportParameter[] {
                new ReportParameter("fechaInicio", fechaInicio.ToShortDateString()),
                new ReportParameter("fechaFin", fechaFin.ToShortDateString())
            });
            
            this.rpvEntidadFinanciera.RefreshReport();
        }

        private void FormReporteEntidadFinanciera_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.rpvEntidadFinanciera.Reset();
        }
    }
}
