﻿namespace ModuloAdministracion
{
    partial class FormReporteEstadosFinancierosSitio
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.pagoDataTableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.rpvEstadosFinanciersoContribuyente = new Microsoft.Reporting.WinForms.ReportViewer();
            ((System.ComponentModel.ISupportInitialize)(this.pagoDataTableBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pagoDataTableBindingSource
            // 
            this.pagoDataTableBindingSource.DataSource = typeof(Modelo.cobranza_patentesDataSet.pagoDataTable);
            // 
            // rpvEstadosFinanciersoContribuyente
            // 
            reportDataSource1.Name = "pagosPendientesDataTable";
            reportDataSource1.Value = this.pagoDataTableBindingSource;
            this.rpvEstadosFinanciersoContribuyente.LocalReport.DataSources.Add(reportDataSource1);
            this.rpvEstadosFinanciersoContribuyente.LocalReport.ReportEmbeddedResource = "ModuloAdministracion.ReportEstadosFinancierosSitio.rdlc";
            this.rpvEstadosFinanciersoContribuyente.Location = new System.Drawing.Point(12, 12);
            this.rpvEstadosFinanciersoContribuyente.Name = "rpvEstadosFinanciersoContribuyente";
            this.rpvEstadosFinanciersoContribuyente.Size = new System.Drawing.Size(868, 447);
            this.rpvEstadosFinanciersoContribuyente.TabIndex = 0;
            // 
            // FormReporteEstadosFinancierosSitio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(892, 471);
            this.Controls.Add(this.rpvEstadosFinanciersoContribuyente);
            this.MaximizeBox = false;
            this.Name = "FormReporteEstadosFinancierosSitio";
            this.Text = "Estados Financieros Sitio";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormReporteEstadosFinancierosSitio_FormClosing);
            this.Load += new System.EventHandler(this.FormReporteEstadosFinancierosContribuyente_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pagoDataTableBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer rpvEstadosFinanciersoContribuyente;
        private System.Windows.Forms.BindingSource pagoDataTableBindingSource;
    }
}