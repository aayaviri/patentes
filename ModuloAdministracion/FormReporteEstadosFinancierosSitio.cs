﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Modelo;
using Control;
using Microsoft.Reporting.WinForms;

namespace ModuloAdministracion
{
    public partial class FormReporteEstadosFinancierosSitio : Form
    {
        private Gestion gestionActual;
        private SitioMunicipal sitioMunicipal;

        public FormReporteEstadosFinancierosSitio(Gestion gestionActual, SitioMunicipal sitioMunicipal)
        {
            InitializeComponent();
            this.gestionActual = gestionActual;
            this.sitioMunicipal = sitioMunicipal;
        }

        private void FormReporteEstadosFinancierosContribuyente_Load(object sender, EventArgs e)
        {
            loadReporte();
        }

        private void loadReporte() {
            Contribuyente contribuyente = ContribuyenteControl.getContribuyenteById((int)sitioMunicipal.getIdContribuyente());

            DataTable pagosDataTable = PagoControl.getPagosPendientesPorIdSitioMunicipal(sitioMunicipal.getId());
            pagosDataTable = PagoControl.calcularPagosPendientes(pagosDataTable, gestionActual, null);
            
            DataTable pagosPagadosDataTable = ReporteControl.reportePagadosIdSitioFechas(sitioMunicipal.getId(), new DateTime(1900,1,1), DateTime.Today.AddDays(1));

            this.rpvEstadosFinanciersoContribuyente.LocalReport.DataSources.Clear();
            ReportDataSource rprtDTSource = new Microsoft.Reporting.WinForms.ReportDataSource("pagosPendientesDataTable", pagosDataTable);
            ReportDataSource rprtDTSource2 = new Microsoft.Reporting.WinForms.ReportDataSource("pagosPagadosDataTable", pagosPagadosDataTable);

            this.rpvEstadosFinanciersoContribuyente.LocalReport.DataSources.Add(rprtDTSource);
            this.rpvEstadosFinanciersoContribuyente.LocalReport.DataSources.Add(rprtDTSource2);

            rpvEstadosFinanciersoContribuyente.LocalReport.SetParameters(new ReportParameter[] {
                new ReportParameter("propietario", contribuyente.getNombre() + " " + contribuyente.getApellidoPaterno() + " " + contribuyente.getApellidoMaterno()),
                new ReportParameter("descripcion", sitioMunicipal.getDescripcion()),
                new ReportParameter("direccion", sitioMunicipal.getDireccion()),
                new ReportParameter("razonSocial", sitioMunicipal.getRazonSocial()),
                new ReportParameter("superficie", sitioMunicipal.getSuperficie().ToString()),
                new ReportParameter("fechaInicio", sitioMunicipal.getFechaInicio().ToShortDateString()),
                new ReportParameter("ufvActual", gestionActual.getUfv().ToString()),
                new ReportParameter("fechaUfv", gestionActual.getFechaUfv().ToShortDateString()),
                new ReportParameter("codigoSitio", sitioMunicipal.getCodigoLicencia())
            });

            this.rpvEstadosFinanciersoContribuyente.RefreshReport();
        }

        private void FormReporteEstadosFinancierosSitio_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.rpvEstadosFinanciersoContribuyente.Reset();
        }
    }
}