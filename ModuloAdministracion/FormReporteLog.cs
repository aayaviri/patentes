﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

using Control;
using Modelo;

namespace ModuloAdministracion
{
    public partial class FormReporteLog : Form
    {
        public FormReporteLog()
        {
            InitializeComponent();
        }

        private void FormReporteLog_Load(object sender, EventArgs e)
        {

        }

        private void btnVerReporte_Click(object sender, EventArgs e)
        {
            loadReporteLog();
        }

        private void FormReporteLog_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.rpvLog.Reset();
        }

        private void loadReporteLog()
        {
            DateTime fechaInicio = dtpFechaInicio.Value;
            DateTime fechaFin = dtpFechaFin.Value.AddDays(1);

            DataTable logDataTable = ReporteControl.getReporteLogFechas(fechaInicio.Date, fechaFin.Date);

            this.rpvLog.LocalReport.DataSources.Clear();
            ReportDataSource rprtDTSource = new Microsoft.Reporting.WinForms.ReportDataSource("logDataTable", logDataTable);

            this.rpvLog.LocalReport.DataSources.Add(rprtDTSource);

            rpvLog.LocalReport.SetParameters(new ReportParameter[] {
                new ReportParameter("fechaInicio", fechaInicio.ToShortDateString()),
                new ReportParameter("fechaFin", fechaFin.ToShortDateString())
            });

            this.rpvLog.RefreshReport();
        }
    }
}
