﻿namespace ModuloAdministracion
{
    partial class FormReporteResumenEntidades
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.rpvEntidadesFinancieras = new Microsoft.Reporting.WinForms.ReportViewer();
            this.btnVerReporte = new System.Windows.Forms.Button();
            this.lblFechaInicio = new System.Windows.Forms.Label();
            this.lblFechaFin = new System.Windows.Forms.Label();
            this.dtpFechaFin = new System.Windows.Forms.DateTimePicker();
            this.dtpFechaInicio = new System.Windows.Forms.DateTimePicker();
            this.reporte_pagosDataTableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.reporte_pagosDataTableBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // rpvEntidadesFinancieras
            // 
            reportDataSource1.Name = "resumenEntidadesDataTable";
            reportDataSource1.Value = this.reporte_pagosDataTableBindingSource;
            this.rpvEntidadesFinancieras.LocalReport.DataSources.Add(reportDataSource1);
            this.rpvEntidadesFinancieras.LocalReport.ReportEmbeddedResource = "ModuloAdministracion.ReportResumenEntidadesFinancieras.rdlc";
            this.rpvEntidadesFinancieras.Location = new System.Drawing.Point(12, 48);
            this.rpvEntidadesFinancieras.Name = "rpvEntidadesFinancieras";
            this.rpvEntidadesFinancieras.Size = new System.Drawing.Size(868, 406);
            this.rpvEntidadesFinancieras.TabIndex = 0;
            // 
            // btnVerReporte
            // 
            this.btnVerReporte.Location = new System.Drawing.Point(380, 11);
            this.btnVerReporte.Name = "btnVerReporte";
            this.btnVerReporte.Size = new System.Drawing.Size(75, 23);
            this.btnVerReporte.TabIndex = 10;
            this.btnVerReporte.Text = "Ver Informe";
            this.btnVerReporte.UseVisualStyleBackColor = true;
            this.btnVerReporte.Click += new System.EventHandler(this.btnVerReporte_Click);
            // 
            // lblFechaInicio
            // 
            this.lblFechaInicio.AutoSize = true;
            this.lblFechaInicio.Location = new System.Drawing.Point(13, 16);
            this.lblFechaInicio.Name = "lblFechaInicio";
            this.lblFechaInicio.Size = new System.Drawing.Size(65, 13);
            this.lblFechaInicio.TabIndex = 9;
            this.lblFechaInicio.Text = "Fecha Inicio";
            // 
            // lblFechaFin
            // 
            this.lblFechaFin.AutoSize = true;
            this.lblFechaFin.Location = new System.Drawing.Point(200, 16);
            this.lblFechaFin.Name = "lblFechaFin";
            this.lblFechaFin.Size = new System.Drawing.Size(54, 13);
            this.lblFechaFin.TabIndex = 8;
            this.lblFechaFin.Text = "Fecha Fin";
            // 
            // dtpFechaFin
            // 
            this.dtpFechaFin.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFechaFin.Location = new System.Drawing.Point(260, 12);
            this.dtpFechaFin.Name = "dtpFechaFin";
            this.dtpFechaFin.Size = new System.Drawing.Size(95, 20);
            this.dtpFechaFin.TabIndex = 7;
            // 
            // dtpFechaInicio
            // 
            this.dtpFechaInicio.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFechaInicio.Location = new System.Drawing.Point(84, 12);
            this.dtpFechaInicio.Name = "dtpFechaInicio";
            this.dtpFechaInicio.Size = new System.Drawing.Size(93, 20);
            this.dtpFechaInicio.TabIndex = 6;
            // 
            // reporte_pagosDataTableBindingSource
            // 
            this.reporte_pagosDataTableBindingSource.DataSource = typeof(Modelo.cobranza_patentesDataSet.reporte_pagosDataTable);
            // 
            // FormReporteResumenEntidades
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(892, 466);
            this.Controls.Add(this.btnVerReporte);
            this.Controls.Add(this.lblFechaInicio);
            this.Controls.Add(this.lblFechaFin);
            this.Controls.Add(this.dtpFechaFin);
            this.Controls.Add(this.dtpFechaInicio);
            this.Controls.Add(this.rpvEntidadesFinancieras);
            this.Name = "FormReporteResumenEntidades";
            this.Text = "Resumen Entidades Financieras";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormReporteResumenEntidades_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.reporte_pagosDataTableBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer rpvEntidadesFinancieras;
        private System.Windows.Forms.Button btnVerReporte;
        private System.Windows.Forms.Label lblFechaInicio;
        private System.Windows.Forms.Label lblFechaFin;
        private System.Windows.Forms.DateTimePicker dtpFechaFin;
        private System.Windows.Forms.DateTimePicker dtpFechaInicio;
        private System.Windows.Forms.BindingSource reporte_pagosDataTableBindingSource;
    }
}