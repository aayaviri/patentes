﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Control;
using Modelo;
using Microsoft.Reporting.WinForms;

namespace ModuloAdministracion
{
    public partial class FormReporteResumenEntidades : Form
    {
        public FormReporteResumenEntidades()
        {
            InitializeComponent();
        }

        private void btnVerReporte_Click(object sender, EventArgs e)
        {
            loadReporteEntidadesFinancieras();
        }

        private void loadReporteEntidadesFinancieras()
        {
            DateTime fechaInicio = dtpFechaInicio.Value;
            DateTime fechaFin = dtpFechaFin.Value.AddDays(1);

            DataTable pagosDataTable = ReporteControl.getResumenEntidadesFechas(fechaInicio.Date, fechaFin.Date);

            this.rpvEntidadesFinancieras.LocalReport.DataSources.Clear();
            ReportDataSource rprtDTSource = new Microsoft.Reporting.WinForms.ReportDataSource("resumenEntidadesDataTable", pagosDataTable);

            this.rpvEntidadesFinancieras.LocalReport.DataSources.Add(rprtDTSource);

            rpvEntidadesFinancieras.LocalReport.SetParameters(new ReportParameter[] {
                new ReportParameter("fechaInicio", fechaInicio.ToShortDateString()),
                new ReportParameter("fechaFin", fechaFin.ToShortDateString())
            });

            this.rpvEntidadesFinancieras.RefreshReport();
        }

        private void FormReporteResumenEntidades_FormClosing(object sender, FormClosingEventArgs e)
        {
            rpvEntidadesFinancieras.Reset();
        }
    }
}
