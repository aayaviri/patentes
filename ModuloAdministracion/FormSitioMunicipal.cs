﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Control;
using Modelo;

namespace ModuloAdministracion
{
    public partial class FormSitioMunicipal : Form
    {
        private SitioMunicipal sitioMunicipal;
        public SitioMunicipal Sitiomunicipal;

        public FormSitioMunicipal()
        {
            InitializeComponent();
            this.sitioMunicipal = new SitioMunicipal();
        }

        public FormSitioMunicipal(SitioMunicipal sitioMunicipal) {
            InitializeComponent();
            this.sitioMunicipal = sitioMunicipal;
            loadModelToForm();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            RespuestaControl validacion = validarFormulario();

            if (validacion.getSuccess())
            {
                guardarFormulario();
            }
            else
            {
                lblErrorMessage.Text = validacion.getMessage();
                lblErrorMessage.Visible = true;
            }
        }

        private void loadModelToForm()
        {
            txtCodigoLicencia.Text = sitioMunicipal.getCodigoLicencia();
            txtDescripcion.Text = sitioMunicipal.getDescripcion();
            txtDireccion.Text = sitioMunicipal.getDireccion();
            txtRazonSocial.Text = sitioMunicipal.getRazonSocial();
            txtSuperficie.Text = sitioMunicipal.getSuperficie().ToString();
            txtTipo.Text = sitioMunicipal.getTipo();
            cbxEstado.SelectedItem = sitioMunicipal.getEstado();
            dtPickerFechaInicio.Value = sitioMunicipal.getFechaInicio();
        }

        private void loadFormToModel()
        {
            sitioMunicipal.setCodigoLicencia(txtCodigoLicencia.Text);
            sitioMunicipal.setDescripcion(txtDescripcion.Text);
            sitioMunicipal.setDireccion(txtDireccion.Text);
            sitioMunicipal.setRazonSocial(txtRazonSocial.Text);
            sitioMunicipal.setSuperficie(Double.Parse(txtSuperficie.Text));
            sitioMunicipal.setTipo(txtTipo.Text);
            sitioMunicipal.setEstado(cbxEstado.SelectedItem.ToString());
            sitioMunicipal.setFechaInicio(dtPickerFechaInicio.Value);
        }
        private RespuestaControl validarFormulario()
        {
            RespuestaControl resp = new RespuestaControl();

            if (String.IsNullOrEmpty(txtCodigoLicencia.Text) || String.IsNullOrWhiteSpace(txtCodigoLicencia.Text))
            {
                resp.setSuccess(false);
                resp.setMessage(resp.getMessage() + "El código de licencia es requerido.\n");
            }

            if (String.IsNullOrEmpty(txtDescripcion.Text) || String.IsNullOrWhiteSpace(txtDescripcion.Text))
            {
                resp.setSuccess(false);
                resp.setMessage(resp.getMessage() + "La descripcion es requerido.\n");
            }
            if (String.IsNullOrEmpty(txtDireccion.Text) || String.IsNullOrWhiteSpace(txtDireccion.Text))
            {
                resp.setSuccess(false);
                resp.setMessage(resp.getMessage() + "La direccion es requerido.\n");
            }

            if (String.IsNullOrEmpty(txtRazonSocial.Text) || String.IsNullOrWhiteSpace(txtRazonSocial.Text))
            {
                resp.setSuccess(false);
                resp.setMessage(resp.getMessage() + "La razon social es requerido.\n");
            }
            if (String.IsNullOrEmpty(txtSuperficie.Text) || String.IsNullOrWhiteSpace(txtSuperficie.Text) || !isNumeric(txtSuperficie.Text))
            {
                resp.setSuccess(false);
                resp.setMessage(resp.getMessage() + "La superficie es requerido.\n");
            }
            if (String.IsNullOrEmpty(txtTipo.Text) || String.IsNullOrWhiteSpace(txtTipo.Text))
            {
                resp.setSuccess(false);
                resp.setMessage(resp.getMessage() + "El tipo es requerido.\n");
            }
            if (cbxEstado.SelectedItem == null || String.IsNullOrEmpty(cbxEstado.SelectedItem.ToString()) || String.IsNullOrWhiteSpace(cbxEstado.SelectedItem.ToString()))
            {
                resp.setSuccess(false);
                resp.setMessage(resp.getMessage() + "El estado es requerido.\n");
            }
            return resp;
        }
        public bool isNumeric(String valor) {
            try
            {
                Convert.ToDouble(valor);
                return true;              
            }
            catch (Exception)
            {

                return false;
            }
        }
        private void guardarFormulario()
        {
          
            loadFormToModel();
            if (SitioMunicipalControl.guardarSitioMunicipal(this.sitioMunicipal))
            {
                this.Sitiomunicipal = this.sitioMunicipal;
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                MessageBox.Show("La licencia ya está en uso");
            }
            

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
