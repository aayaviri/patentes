﻿namespace ModuloAdministracion
{
    partial class FormSitiosMunicipales
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvSitiosMunicipales = new System.Windows.Forms.DataGridView();
            this.btnAsignarContribuyente = new System.Windows.Forms.Button();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.btnModificar = new System.Windows.Forms.Button();
            this.btnNuevo = new System.Windows.Forms.Button();
            this.btnEstadoCuentas = new System.Windows.Forms.Button();
            this.btnBuscarCodigoLicencia = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtApellidoPaterno = new System.Windows.Forms.TextBox();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.txtCodigoLicencia = new System.Windows.Forms.TextBox();
            this.btnLimpiarNombre = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txtApellidoMaterno = new System.Windows.Forms.TextBox();
            this.btnBuscarNombreApellidos = new System.Windows.Forms.Button();
            this.btnLimpiarCodigoLicencia = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSitiosMunicipales)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvSitiosMunicipales
            // 
            this.dgvSitiosMunicipales.AllowUserToAddRows = false;
            this.dgvSitiosMunicipales.AllowUserToDeleteRows = false;
            this.dgvSitiosMunicipales.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSitiosMunicipales.Location = new System.Drawing.Point(12, 64);
            this.dgvSitiosMunicipales.MultiSelect = false;
            this.dgvSitiosMunicipales.Name = "dgvSitiosMunicipales";
            this.dgvSitiosMunicipales.ReadOnly = true;
            this.dgvSitiosMunicipales.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSitiosMunicipales.Size = new System.Drawing.Size(587, 142);
            this.dgvSitiosMunicipales.TabIndex = 0;
            // 
            // btnAsignarContribuyente
            // 
            this.btnAsignarContribuyente.Location = new System.Drawing.Point(464, 212);
            this.btnAsignarContribuyente.Name = "btnAsignarContribuyente";
            this.btnAsignarContribuyente.Size = new System.Drawing.Size(131, 31);
            this.btnAsignarContribuyente.TabIndex = 6;
            this.btnAsignarContribuyente.Text = "Asignar Contribuyente";
            this.btnAsignarContribuyente.UseVisualStyleBackColor = true;
            this.btnAsignarContribuyente.Click += new System.EventHandler(this.btnAsignarContribuyente_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.Image = global::ModuloAdministracion.Properties.Resources.Delete_24;
            this.btnEliminar.Location = new System.Drawing.Point(221, 229);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(82, 31);
            this.btnEliminar.TabIndex = 5;
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnEliminar.UseVisualStyleBackColor = true;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // btnModificar
            // 
            this.btnModificar.Image = global::ModuloAdministracion.Properties.Resources.Modify_24;
            this.btnModificar.Location = new System.Drawing.Point(115, 229);
            this.btnModificar.Name = "btnModificar";
            this.btnModificar.Size = new System.Drawing.Size(82, 31);
            this.btnModificar.TabIndex = 4;
            this.btnModificar.Text = "Modificar";
            this.btnModificar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnModificar.UseVisualStyleBackColor = true;
            this.btnModificar.Click += new System.EventHandler(this.btnModificar_Click);
            // 
            // btnNuevo
            // 
            this.btnNuevo.Image = global::ModuloAdministracion.Properties.Resources.Add_24;
            this.btnNuevo.Location = new System.Drawing.Point(12, 229);
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(82, 31);
            this.btnNuevo.TabIndex = 3;
            this.btnNuevo.Text = "Nuevo";
            this.btnNuevo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnNuevo.UseVisualStyleBackColor = true;
            this.btnNuevo.Click += new System.EventHandler(this.btnNuevo_Click);
            // 
            // btnEstadoCuentas
            // 
            this.btnEstadoCuentas.Location = new System.Drawing.Point(464, 249);
            this.btnEstadoCuentas.Name = "btnEstadoCuentas";
            this.btnEstadoCuentas.Size = new System.Drawing.Size(131, 31);
            this.btnEstadoCuentas.TabIndex = 7;
            this.btnEstadoCuentas.Text = "Ver Estado de Cuentas";
            this.btnEstadoCuentas.UseVisualStyleBackColor = true;
            this.btnEstadoCuentas.Click += new System.EventHandler(this.btnEstadoCuentas_Click);
            // 
            // btnBuscarCodigoLicencia
            // 
            this.btnBuscarCodigoLicencia.BackColor = System.Drawing.SystemColors.Control;
            this.btnBuscarCodigoLicencia.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBuscarCodigoLicencia.Image = global::ModuloAdministracion.Properties.Resources.Search_24;
            this.btnBuscarCodigoLicencia.Location = new System.Drawing.Point(105, 30);
            this.btnBuscarCodigoLicencia.Name = "btnBuscarCodigoLicencia";
            this.btnBuscarCodigoLicencia.Size = new System.Drawing.Size(24, 24);
            this.btnBuscarCodigoLicencia.TabIndex = 10;
            this.btnBuscarCodigoLicencia.UseVisualStyleBackColor = false;
            this.btnBuscarCodigoLicencia.Click += new System.EventHandler(this.btnBuscarCodigoLicencia_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(320, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "Ap. Paterno:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(216, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "Nombre:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "Codigo Licencia";
            // 
            // txtApellidoPaterno
            // 
            this.txtApellidoPaterno.Location = new System.Drawing.Point(298, 32);
            this.txtApellidoPaterno.Name = "txtApellidoPaterno";
            this.txtApellidoPaterno.Size = new System.Drawing.Size(105, 20);
            this.txtApellidoPaterno.TabIndex = 12;
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(179, 32);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(112, 20);
            this.txtNombre.TabIndex = 11;
            // 
            // txtCodigoLicencia
            // 
            this.txtCodigoLicencia.Location = new System.Drawing.Point(12, 34);
            this.txtCodigoLicencia.Name = "txtCodigoLicencia";
            this.txtCodigoLicencia.Size = new System.Drawing.Size(87, 20);
            this.txtCodigoLicencia.TabIndex = 9;
            // 
            // btnLimpiarNombre
            // 
            this.btnLimpiarNombre.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnLimpiarNombre.Image = global::ModuloAdministracion.Properties.Resources.Clear;
            this.btnLimpiarNombre.Location = new System.Drawing.Point(569, 28);
            this.btnLimpiarNombre.Name = "btnLimpiarNombre";
            this.btnLimpiarNombre.Size = new System.Drawing.Size(26, 24);
            this.btnLimpiarNombre.TabIndex = 18;
            this.btnLimpiarNombre.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnLimpiarNombre.UseVisualStyleBackColor = true;
            this.btnLimpiarNombre.Click += new System.EventHandler(this.btnLimpiarNombre_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(436, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 13);
            this.label4.TabIndex = 19;
            this.label4.Text = "Ap. Paterno:";
            // 
            // txtApellidoMaterno
            // 
            this.txtApellidoMaterno.Location = new System.Drawing.Point(409, 32);
            this.txtApellidoMaterno.Name = "txtApellidoMaterno";
            this.txtApellidoMaterno.Size = new System.Drawing.Size(113, 20);
            this.txtApellidoMaterno.TabIndex = 16;
            // 
            // btnBuscarNombreApellidos
            // 
            this.btnBuscarNombreApellidos.BackColor = System.Drawing.SystemColors.Control;
            this.btnBuscarNombreApellidos.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBuscarNombreApellidos.Image = global::ModuloAdministracion.Properties.Resources.Search_24;
            this.btnBuscarNombreApellidos.Location = new System.Drawing.Point(538, 28);
            this.btnBuscarNombreApellidos.Name = "btnBuscarNombreApellidos";
            this.btnBuscarNombreApellidos.Size = new System.Drawing.Size(24, 24);
            this.btnBuscarNombreApellidos.TabIndex = 17;
            this.btnBuscarNombreApellidos.UseVisualStyleBackColor = false;
            this.btnBuscarNombreApellidos.Click += new System.EventHandler(this.btnBuscarNombreApellidos_Click);
            // 
            // btnLimpiarCodigoLicencia
            // 
            this.btnLimpiarCodigoLicencia.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnLimpiarCodigoLicencia.Image = global::ModuloAdministracion.Properties.Resources.Clear;
            this.btnLimpiarCodigoLicencia.Location = new System.Drawing.Point(135, 29);
            this.btnLimpiarCodigoLicencia.Name = "btnLimpiarCodigoLicencia";
            this.btnLimpiarCodigoLicencia.Size = new System.Drawing.Size(26, 24);
            this.btnLimpiarCodigoLicencia.TabIndex = 20;
            this.btnLimpiarCodigoLicencia.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnLimpiarCodigoLicencia.UseVisualStyleBackColor = true;
            this.btnLimpiarCodigoLicencia.Click += new System.EventHandler(this.btnLimpiarCodigoLicencia_Click);
            // 
            // FormSitiosMunicipales
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(611, 374);
            this.Controls.Add(this.btnLimpiarCodigoLicencia);
            this.Controls.Add(this.btnLimpiarNombre);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtApellidoMaterno);
            this.Controls.Add(this.btnBuscarNombreApellidos);
            this.Controls.Add(this.btnBuscarCodigoLicencia);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtApellidoPaterno);
            this.Controls.Add(this.txtNombre);
            this.Controls.Add(this.txtCodigoLicencia);
            this.Controls.Add(this.btnEstadoCuentas);
            this.Controls.Add(this.btnAsignarContribuyente);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.btnModificar);
            this.Controls.Add(this.btnNuevo);
            this.Controls.Add(this.dgvSitiosMunicipales);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormSitiosMunicipales";
            this.Text = "FormSitiosMunicipales";
            ((System.ComponentModel.ISupportInitialize)(this.dgvSitiosMunicipales)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvSitiosMunicipales;
        private System.Windows.Forms.Button btnNuevo;
        private System.Windows.Forms.Button btnModificar;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.Button btnAsignarContribuyente;
        private System.Windows.Forms.Button btnEstadoCuentas;
        private System.Windows.Forms.Button btnBuscarCodigoLicencia;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtApellidoPaterno;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.TextBox txtCodigoLicencia;
        private System.Windows.Forms.Button btnLimpiarNombre;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtApellidoMaterno;
        private System.Windows.Forms.Button btnBuscarNombreApellidos;
        private System.Windows.Forms.Button btnLimpiarCodigoLicencia;
    }
}