﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

using Control;
using Modelo;

namespace ModuloAdministracion
{
    public partial class FormSitiosMunicipales : Form
    {
        public FormSitiosMunicipales()
        {
            InitializeComponent();
        }

        private void buscarSitiosCodigoLicencia(string codigo)
        {
            if (txtCodigoLicencia.Text.Length >= 5)
            {
                DataTable dataTable = SitioMunicipalControl.buscarSitiosMunicipalesPorCodigo(codigo);

                dgvSitiosMunicipales.DataSource = dataTable;
                dgvSitiosMunicipales.Columns["id"].Visible = false;
                dgvSitiosMunicipales.Columns["descripcion"].Visible = false;
                dgvSitiosMunicipales.Columns["idContribuyente"].Visible = false;
                dgvSitiosMunicipales.Columns["razonSocial"].Visible = false;

                dgvSitiosMunicipales.Columns["codigoLicencia"].HeaderText = "Codigo Licencia";
                dgvSitiosMunicipales.Columns["direccion"].HeaderText = "Dirección";
                dgvSitiosMunicipales.Columns["estado"].HeaderText = "Estado";
                dgvSitiosMunicipales.Columns["fechaInicio"].HeaderText = "Fecha Inicio";
                dgvSitiosMunicipales.Columns["superficie"].HeaderText = "Superficie";
                dgvSitiosMunicipales.Columns["tipo"].HeaderText = "Tipo";
                dgvSitiosMunicipales.Columns["contribuyente"].HeaderText = "Contribuyente";

                dgvSitiosMunicipales.Refresh();
            }
            else
            {
                MessageBox.Show("Debe introducir al menos 5 caracteres.");
            }
        }
       

        private void mostrarFormNuevo()
        {
            FormSitioMunicipal formSitioMunicipal = new FormSitioMunicipal();
            formSitioMunicipal.ShowDialog();
            SitioMunicipal creado = formSitioMunicipal.Sitiomunicipal;
            if (formSitioMunicipal.DialogResult.Equals(DialogResult.OK))
            {
                buscarSitiosCodigoLicencia(creado.getCodigoLicencia());
            }
        }

        private void mostrarFormModificar()
        {
            if (dgvSitiosMunicipales.SelectedRows.Count == 1)
            {
                int idSitio = (int)dgvSitiosMunicipales.SelectedRows[0].Cells[0].Value;
                SitioMunicipal sitio = SitioMunicipalControl.getSitioMunicipalById(idSitio);
                FormSitioMunicipal formSitioMunicipal = new FormSitioMunicipal(sitio);
                formSitioMunicipal.ShowDialog();
                if (formSitioMunicipal.DialogResult.Equals(DialogResult.OK))
                {
                    buscarSitiosCodigoLicencia(txtCodigoLicencia.Text);
                }
            }
            else
            {
                MessageBox.Show("Debe seleccionar un registro primero");
            }
        }

        private void mostrarFormEliminar()
        {
            if (dgvSitiosMunicipales.SelectedRows.Count == 1)
            {
                DialogResult eliminar = MessageBox.Show("Esta seguro que desea eliminar el contribuyente seleccionado?", "Confirmación", MessageBoxButtons.YesNo);
                if (eliminar.Equals(DialogResult.Yes))
                {
                    int idSitio = (int)dgvSitiosMunicipales.SelectedRows[0].Cells[0].Value;
                    SitioMunicipal sitioMunicipal = SitioMunicipalControl.getSitioMunicipalById(idSitio);
                    RespuestaControl resp = SitioMunicipalControl.eliminarContribuyente(sitioMunicipal);
                    if (resp.getSuccess())
                    {
                        buscarSitiosCodigoLicencia(txtCodigoLicencia.Text);
                    }
                    else
                    {
                        MessageBox.Show(resp.getMessage());
                    }
                }
            }
            else
            {
                MessageBox.Show("Debe seleccionar un registro primero.");
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            mostrarFormNuevo();
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            mostrarFormModificar();
        }


        private void btnAsignarContribuyente_Click(object sender, EventArgs e)
        {
            if (dgvSitiosMunicipales.SelectedRows.Count > 0)
            {
                int idSitio = (int)dgvSitiosMunicipales.SelectedRows[0].Cells[0].Value;
                SitioMunicipal sitio = SitioMunicipalControl.getSitioMunicipalById(idSitio);
                FormAsignarContribuyente asignContribuyente = new FormAsignarContribuyente(sitio);
                DialogResult resp = asignContribuyente.ShowDialog();
                if (resp.Equals(DialogResult.OK))
                {
                    buscarSitiosCodigoLicencia(txtCodigoLicencia.Text);
                }
            }
        }

        private void btnEstadoCuentas_Click(object sender, EventArgs e)
        {
            verEstadoCuentasSitio();
        }

        private void buscarSitiosNombreApellidos()
        {
            if (txtNombre.Text.Length >= 2 && txtApellidoPaterno.Text.Length >= 2 && txtApellidoMaterno.Text.Length >= 2)
            {
                DataTable dataTable = SitioMunicipalControl.buscarSitiosMunicipalesPorNombreApellidos(txtNombre.Text, txtApellidoPaterno.Text, txtApellidoMaterno.Text);

                dgvSitiosMunicipales.DataSource = dataTable;
                dgvSitiosMunicipales.Columns["id"].Visible = false;
                dgvSitiosMunicipales.Columns["descripcion"].Visible = false;
                dgvSitiosMunicipales.Columns["idContribuyente"].Visible = false;
                dgvSitiosMunicipales.Columns["razonSocial"].Visible = false;

                dgvSitiosMunicipales.Columns["codigoLicencia"].HeaderText = "Codigo Licencia";
                dgvSitiosMunicipales.Columns["direccion"].HeaderText = "Dirección";
                dgvSitiosMunicipales.Columns["estado"].HeaderText = "Estado";
                dgvSitiosMunicipales.Columns["fechaInicio"].HeaderText = "Fecha Inicio";
                dgvSitiosMunicipales.Columns["superficie"].HeaderText = "Superficie";
                dgvSitiosMunicipales.Columns["tipo"].HeaderText = "Tipo";
                dgvSitiosMunicipales.Columns["contribuyente"].HeaderText = "Contribuyente";

                dgvSitiosMunicipales.Refresh();
            }
            else
            {
                MessageBox.Show("Los campos del contribuyente deben llevar al menos 2 caracteres cada uno.");
            }
        }

        private void verEstadoCuentasSitio() {
            if (dgvSitiosMunicipales.SelectedRows.Count > 0) {
                int idSitio = (int)dgvSitiosMunicipales.SelectedRows[0].Cells["id"].Value;
                SitioMunicipal sitioMunicipal = SitioMunicipalControl.getSitioMunicipalById(idSitio);
                Gestion gestionActual = GestionControl.obtenerUltimaGestion();
                FormReporteEstadosFinancierosSitio formReporte = new FormReporteEstadosFinancierosSitio(gestionActual, sitioMunicipal);
                formReporte.ShowDialog();
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            mostrarFormEliminar();
        }

        private void btnBuscarCodigoLicencia_Click(object sender, EventArgs e)
        {
            buscarSitiosCodigoLicencia(txtCodigoLicencia.Text);
        }
        public void limpiarFormulario() {
            txtNombre.Text = "";
            txtApellidoPaterno.Text = "";
            txtApellidoMaterno.Text = "";
            txtCodigoLicencia.Text = "";
            if (dgvSitiosMunicipales.DataSource != null)
                ((DataTable)dgvSitiosMunicipales.DataSource).Rows.Clear();
        }
        private void btnLimpiarCodigoLicencia_Click(object sender, EventArgs e)
        {
            limpiarFormulario();
            
        }

        private void btnBuscarNombreApellidos_Click(object sender, EventArgs e)
        {
            buscarSitiosNombreApellidos();
        }

        private void btnLimpiarNombre_Click(object sender, EventArgs e)
        {
            limpiarFormulario();
        }
    }
}
