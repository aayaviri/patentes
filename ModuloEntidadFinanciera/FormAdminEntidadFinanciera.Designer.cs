﻿namespace ModuloEntidadFinanciera
{
    partial class FormAdminEntidadFinanciera
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gboxMain = new System.Windows.Forms.GroupBox();
            this.btnActualizarSucursales = new System.Windows.Forms.Button();
            this.btnEliminarSucursal = new System.Windows.Forms.Button();
            this.btnModificarSucursal = new System.Windows.Forms.Button();
            this.btnNuevaSucursal = new System.Windows.Forms.Button();
            this.dgvSucursales = new System.Windows.Forms.DataGridView();
            this.dgvCajeros = new System.Windows.Forms.DataGridView();
            this.gboxCajeros = new System.Windows.Forms.GroupBox();
            this.btnActualizarCajeros = new System.Windows.Forms.Button();
            this.btnEliminarCajero = new System.Windows.Forms.Button();
            this.btnModificarCajero = new System.Windows.Forms.Button();
            this.btnNuevoCajero = new System.Windows.Forms.Button();
            this.gboxMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSucursales)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCajeros)).BeginInit();
            this.gboxCajeros.SuspendLayout();
            this.SuspendLayout();
            // 
            // gboxMain
            // 
            this.gboxMain.Controls.Add(this.btnActualizarSucursales);
            this.gboxMain.Controls.Add(this.btnEliminarSucursal);
            this.gboxMain.Controls.Add(this.btnModificarSucursal);
            this.gboxMain.Controls.Add(this.btnNuevaSucursal);
            this.gboxMain.Controls.Add(this.dgvSucursales);
            this.gboxMain.Location = new System.Drawing.Point(12, 12);
            this.gboxMain.Name = "gboxMain";
            this.gboxMain.Size = new System.Drawing.Size(856, 236);
            this.gboxMain.TabIndex = 0;
            this.gboxMain.TabStop = false;
            this.gboxMain.Text = "Sucursales";
            // 
            // btnActualizarSucursales
            // 
            this.btnActualizarSucursales.Image = global::ModuloEntidadFinanciera.Properties.Resources.refresh_blue_24;
            this.btnActualizarSucursales.Location = new System.Drawing.Point(765, 199);
            this.btnActualizarSucursales.Name = "btnActualizarSucursales";
            this.btnActualizarSucursales.Size = new System.Drawing.Size(85, 31);
            this.btnActualizarSucursales.TabIndex = 4;
            this.btnActualizarSucursales.Text = "Actualizar";
            this.btnActualizarSucursales.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnActualizarSucursales.UseVisualStyleBackColor = true;
            this.btnActualizarSucursales.Click += new System.EventHandler(this.btnActualizarSucursales_Click);
            // 
            // btnEliminarSucursal
            // 
            this.btnEliminarSucursal.Image = global::ModuloEntidadFinanciera.Properties.Resources.Delete_24;
            this.btnEliminarSucursal.Location = new System.Drawing.Point(182, 199);
            this.btnEliminarSucursal.Name = "btnEliminarSucursal";
            this.btnEliminarSucursal.Size = new System.Drawing.Size(82, 31);
            this.btnEliminarSucursal.TabIndex = 3;
            this.btnEliminarSucursal.Text = "Eliminar";
            this.btnEliminarSucursal.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnEliminarSucursal.UseVisualStyleBackColor = true;
            this.btnEliminarSucursal.Click += new System.EventHandler(this.btnEliminarSucursal_Click);
            // 
            // btnModificarSucursal
            // 
            this.btnModificarSucursal.Image = global::ModuloEntidadFinanciera.Properties.Resources.Modify_24;
            this.btnModificarSucursal.Location = new System.Drawing.Point(94, 199);
            this.btnModificarSucursal.Name = "btnModificarSucursal";
            this.btnModificarSucursal.Size = new System.Drawing.Size(82, 31);
            this.btnModificarSucursal.TabIndex = 2;
            this.btnModificarSucursal.Text = "Modificar";
            this.btnModificarSucursal.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnModificarSucursal.UseVisualStyleBackColor = true;
            this.btnModificarSucursal.Click += new System.EventHandler(this.btnModificarSucursal_Click);
            // 
            // btnNuevaSucursal
            // 
            this.btnNuevaSucursal.Image = global::ModuloEntidadFinanciera.Properties.Resources.Add_24;
            this.btnNuevaSucursal.Location = new System.Drawing.Point(6, 199);
            this.btnNuevaSucursal.Name = "btnNuevaSucursal";
            this.btnNuevaSucursal.Size = new System.Drawing.Size(82, 31);
            this.btnNuevaSucursal.TabIndex = 1;
            this.btnNuevaSucursal.Text = "Nuevo";
            this.btnNuevaSucursal.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnNuevaSucursal.UseVisualStyleBackColor = true;
            this.btnNuevaSucursal.Click += new System.EventHandler(this.btnNuevaSucursal_Click);
            // 
            // dgvSucursales
            // 
            this.dgvSucursales.AllowUserToAddRows = false;
            this.dgvSucursales.AllowUserToDeleteRows = false;
            this.dgvSucursales.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSucursales.Location = new System.Drawing.Point(6, 19);
            this.dgvSucursales.MultiSelect = false;
            this.dgvSucursales.Name = "dgvSucursales";
            this.dgvSucursales.ReadOnly = true;
            this.dgvSucursales.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSucursales.Size = new System.Drawing.Size(844, 174);
            this.dgvSucursales.TabIndex = 0;
            this.dgvSucursales.SelectionChanged += new System.EventHandler(this.dgvSucursales_SelectionChanged);
            // 
            // dgvCajeros
            // 
            this.dgvCajeros.AllowUserToAddRows = false;
            this.dgvCajeros.AllowUserToDeleteRows = false;
            this.dgvCajeros.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCajeros.Location = new System.Drawing.Point(6, 19);
            this.dgvCajeros.MultiSelect = false;
            this.dgvCajeros.Name = "dgvCajeros";
            this.dgvCajeros.ReadOnly = true;
            this.dgvCajeros.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCajeros.Size = new System.Drawing.Size(844, 161);
            this.dgvCajeros.TabIndex = 1;
            // 
            // gboxCajeros
            // 
            this.gboxCajeros.AutoSize = true;
            this.gboxCajeros.Controls.Add(this.btnActualizarCajeros);
            this.gboxCajeros.Controls.Add(this.btnEliminarCajero);
            this.gboxCajeros.Controls.Add(this.btnModificarCajero);
            this.gboxCajeros.Controls.Add(this.btnNuevoCajero);
            this.gboxCajeros.Controls.Add(this.dgvCajeros);
            this.gboxCajeros.Location = new System.Drawing.Point(12, 254);
            this.gboxCajeros.Name = "gboxCajeros";
            this.gboxCajeros.Size = new System.Drawing.Size(856, 236);
            this.gboxCajeros.TabIndex = 2;
            this.gboxCajeros.TabStop = false;
            this.gboxCajeros.Text = "Cajeros";
            // 
            // btnActualizarCajeros
            // 
            this.btnActualizarCajeros.Image = global::ModuloEntidadFinanciera.Properties.Resources.refresh_blue_24;
            this.btnActualizarCajeros.Location = new System.Drawing.Point(765, 186);
            this.btnActualizarCajeros.Name = "btnActualizarCajeros";
            this.btnActualizarCajeros.Size = new System.Drawing.Size(85, 31);
            this.btnActualizarCajeros.TabIndex = 8;
            this.btnActualizarCajeros.Text = "Actualizar";
            this.btnActualizarCajeros.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnActualizarCajeros.UseVisualStyleBackColor = true;
            this.btnActualizarCajeros.Click += new System.EventHandler(this.btnActualizarCajeros_Click);
            // 
            // btnEliminarCajero
            // 
            this.btnEliminarCajero.Image = global::ModuloEntidadFinanciera.Properties.Resources.Delete_24;
            this.btnEliminarCajero.Location = new System.Drawing.Point(182, 186);
            this.btnEliminarCajero.Name = "btnEliminarCajero";
            this.btnEliminarCajero.Size = new System.Drawing.Size(82, 31);
            this.btnEliminarCajero.TabIndex = 7;
            this.btnEliminarCajero.Text = "Eliminar";
            this.btnEliminarCajero.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnEliminarCajero.UseVisualStyleBackColor = true;
            this.btnEliminarCajero.Click += new System.EventHandler(this.btnEliminarCajero_Click);
            // 
            // btnModificarCajero
            // 
            this.btnModificarCajero.Image = global::ModuloEntidadFinanciera.Properties.Resources.Modify_24;
            this.btnModificarCajero.Location = new System.Drawing.Point(94, 186);
            this.btnModificarCajero.Name = "btnModificarCajero";
            this.btnModificarCajero.Size = new System.Drawing.Size(82, 31);
            this.btnModificarCajero.TabIndex = 6;
            this.btnModificarCajero.Text = "Modificar";
            this.btnModificarCajero.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnModificarCajero.UseVisualStyleBackColor = true;
            this.btnModificarCajero.Click += new System.EventHandler(this.btnModificarCajero_Click);
            // 
            // btnNuevoCajero
            // 
            this.btnNuevoCajero.Image = global::ModuloEntidadFinanciera.Properties.Resources.Add_24;
            this.btnNuevoCajero.Location = new System.Drawing.Point(6, 186);
            this.btnNuevoCajero.Name = "btnNuevoCajero";
            this.btnNuevoCajero.Size = new System.Drawing.Size(82, 31);
            this.btnNuevoCajero.TabIndex = 5;
            this.btnNuevoCajero.Text = "Nuevo";
            this.btnNuevoCajero.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnNuevoCajero.UseVisualStyleBackColor = true;
            this.btnNuevoCajero.Click += new System.EventHandler(this.btnNuevoCajero_Click);
            // 
            // FormAdminEntidadFinanciera
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(880, 504);
            this.Controls.Add(this.gboxCajeros);
            this.Controls.Add(this.gboxMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormAdminEntidadFinanciera";
            this.Text = "FormAdminEntidadFinanciera";
            this.gboxMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSucursales)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCajeros)).EndInit();
            this.gboxCajeros.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gboxMain;
        private System.Windows.Forms.Button btnActualizarSucursales;
        private System.Windows.Forms.Button btnEliminarSucursal;
        private System.Windows.Forms.Button btnModificarSucursal;
        private System.Windows.Forms.Button btnNuevaSucursal;
        private System.Windows.Forms.DataGridView dgvSucursales;
        private System.Windows.Forms.DataGridView dgvCajeros;
        private System.Windows.Forms.GroupBox gboxCajeros;
        private System.Windows.Forms.Button btnActualizarCajeros;
        private System.Windows.Forms.Button btnEliminarCajero;
        private System.Windows.Forms.Button btnModificarCajero;
        private System.Windows.Forms.Button btnNuevoCajero;
    }
}