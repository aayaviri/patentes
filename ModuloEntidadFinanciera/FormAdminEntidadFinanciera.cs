﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Modelo;
using Control;
using System.Xml;
using System.ServiceModel.Syndication;
using System.Diagnostics;

namespace ModuloEntidadFinanciera
{
    public partial class FormAdminEntidadFinanciera : Form
    {
        private EntidadFinanciera entiFin;
        private Usuario usuario;

        public FormAdminEntidadFinanciera(EntidadFinanciera entiFin, Usuario usuario)
        {
            InitializeComponent();
            this.entiFin = entiFin;
            this.usuario = usuario;
            loadDgvSucursales();
        }

        private void loadDgvSucursales() {
            dgvSucursales.DataSource = SucursalControl.listarSurcursalesPorEntidadFinanciera(entiFin.getId());
            dgvSucursales.Columns["id"].Visible = false;
            dgvSucursales.Columns["idEntidadFinanciera"].Visible = false;

            dgvSucursales.Columns["direccion"].Width = 450;
            
            dgvSucursales.Refresh();
        }

        private void loadDgvCajeros() { 
            int idSucursal = (int)dgvSucursales.SelectedRows[0].Cells["id"].Value;
            dgvCajeros.DataSource = CajeroControl.listarCajerosPorIdSucursal(idSucursal);
            dgvCajeros.Columns["id"].Visible = false;
            dgvCajeros.Columns["idSucursal"].Visible = false;
            dgvCajeros.Columns["dosificacion"].Visible = false;
            dgvCajeros.Columns["idUsuario"].Visible = false;

            dgvCajeros.Columns["nombreCompleto"].Width = 350;
            dgvCajeros.Refresh();
        }

        private void mostrarFormNuevaSucursal()
        {
            FormSucursal formSucursal = new FormSucursal(entiFin, usuario);
            formSucursal.ShowDialog();
            if (formSucursal.DialogResult.Equals(DialogResult.OK))
            {
                loadDgvSucursales();
            }
        }

        private void mostrarFormModificarSucursal()
        {
            if (dgvSucursales.SelectedRows.Count == 1)
            {
                int idSucursal = (int)dgvSucursales.SelectedRows[0].Cells[0].Value;
                Sucursal sucursal = SucursalControl.getSucursalById(idSucursal);

                FormSucursal formSucursal = new FormSucursal(entiFin, sucursal, usuario);
                formSucursal.ShowDialog();
                if (formSucursal.DialogResult.Equals(DialogResult.OK))
                {
                    loadDgvSucursales();
                }
            }
            else
            {
                MessageBox.Show("Debe seleccionar un registro primero");
            }
        }

        private void mostrarFormEliminarSucursal() {
            if (dgvSucursales.SelectedRows.Count == 1)
            {
                DialogResult eliminar = MessageBox.Show("Esta seguro que desea eliminar la sucursal seleccionada?", "Confirmación", MessageBoxButtons.YesNo);
                if (eliminar.Equals(DialogResult.Yes))
                {
                    int idSucursal = (int)dgvSucursales.SelectedRows[0].Cells[0].Value;
                    Sucursal sucursal = SucursalControl.getSucursalById(idSucursal);
                    RespuestaControl resp = SucursalControl.eliminarSucursal(sucursal);
                    if (resp.getSuccess())
                    {
                        loadDgvSucursales();
                    }
                    else
                    {
                        MessageBox.Show(resp.getMessage());
                    }
                }
            }
            else
            {
                MessageBox.Show("Debe seleccionar un registro primero.");
            }
        }
        public int cantidadCajerosRegistrados() {
            int cajerosActual = 0;
            for (int i = 0; i < dgvSucursales.Rows.Count; i++)
            {
                int idSucursal = (int)dgvSucursales.Rows[i].Cells["id"].Value;
                DataTable cajeros = CajeroControl.listarCajerosPorIdSucursal(idSucursal);
                cajerosActual += cajeros.Rows.Count;
            }
            return cajerosActual;
        }
        private void mostrarFormNuevoCajero()
        {
            int cajeroMax = entiFin.getMaxCajeros();
            if (cantidadCajerosRegistrados() >= cajeroMax) {
                MessageBox.Show("Se alcanzo el máximo de cajeros");
                return;
            }
            FormCajero formCajero = new FormCajero(entiFin);
            formCajero.ShowDialog();
            if (formCajero.DialogResult.Equals(DialogResult.OK))
            {
                loadDgvCajeros();
            }
        }

        private void mostrarFormModificarCajero()
        {
            if (dgvCajeros.SelectedRows.Count == 1)
            {
                int idCajero = (int)dgvCajeros.SelectedRows[0].Cells["id"].Value;
                Cajero cajero = CajeroControl.getCajeroById(idCajero);

                FormCajero formCajero = new FormCajero(entiFin, cajero);
                formCajero.ShowDialog();
                if (formCajero.DialogResult.Equals(DialogResult.OK))
                {
                    loadDgvCajeros();
                }
            }
            else
            {
                MessageBox.Show("Debe seleccionar un registro primero");
            }
        }

        private void mostrarFormEliminarCajero()
        {
            if (dgvCajeros.SelectedRows.Count == 1)
            {
                DialogResult eliminar = MessageBox.Show("Esta seguro que desea eliminar el cajero seleccionado?", "Confirmación", MessageBoxButtons.YesNo);
                if (eliminar.Equals(DialogResult.Yes))
                {
                    int idCajero = (int)dgvCajeros.SelectedRows[0].Cells[0].Value;
                    Cajero cajero = CajeroControl.getCajeroById(idCajero);
                    RespuestaControl resp = CajeroControl.eliminarCajero(cajero);
                    if (resp.getSuccess())
                    {
                        loadDgvCajeros();
                    }
                    else
                    {
                        MessageBox.Show(resp.getMessage());
                    }
                }
            }
            else
            {
                MessageBox.Show("Debe seleccionar un registro primero.");
            }
        }

        private void btnNuevaSucursal_Click(object sender, EventArgs e)
        {
            mostrarFormNuevaSucursal();
        }

        private void btnActualizarSucursales_Click(object sender, EventArgs e)
        {
            loadDgvSucursales();
        }

        private void btnModificarSucursal_Click(object sender, EventArgs e)
        {
            mostrarFormModificarSucursal();
        }

        private void dgvSucursales_SelectionChanged(object sender, EventArgs e)
        {
            if(dgvSucursales.SelectedRows.Count > 0) {
                loadDgvCajeros();
            }
        }

        private void btnNuevoCajero_Click(object sender, EventArgs e)
        {
            mostrarFormNuevoCajero();
        }

        private void btnModificarCajero_Click(object sender, EventArgs e)
        {
            mostrarFormModificarCajero();
        }

        private void btnActualizarCajeros_Click(object sender, EventArgs e)
        {
            loadDgvCajeros();
        }

        private void verFeeds() {
            String url = "http://www.bcb.gob.bo/rss_bcb.php";
            XmlReader reader = XmlReader.Create(url);
            SyndicationFeed feed = SyndicationFeed.Load(reader);
            reader.Close();
            foreach (SyndicationItem item in feed.Items)
            {
                Debug.WriteLine("item.Title: "+item.Title.Text);
                Debug.WriteLine("item.Summary: " + item.Summary.Text);
            }
        }

        private void btnEliminarSucursal_Click(object sender, EventArgs e)
        {
            mostrarFormEliminarSucursal();
        }

        private void btnEliminarCajero_Click(object sender, EventArgs e)
        {
            mostrarFormEliminarCajero();
        }
    }
}
