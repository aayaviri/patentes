﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Modelo;
using Control;

namespace ModuloEntidadFinanciera
{
    public partial class FormAnularComprobante : Form
    {
        private Cajero cajero;
        private ComprobanteAnulado comprobanteAnulado;

        public FormAnularComprobante(Cajero cajero)
        {
            InitializeComponent();
            this.cajero = cajero;
            this.comprobanteAnulado = new ComprobanteAnulado();
        }

        private void loadFormToModel()
        {
            comprobanteAnulado.setIdCajero(this.cajero.getId());
            comprobanteAnulado.setNroComprobante(int.Parse(txtNroComprobante.Text));
            comprobanteAnulado.setMotivo(txtMotivo.Text);
        }

        private RespuestaControl validarFormulario()
        {
            RespuestaControl resp = new RespuestaControl();

            if (String.IsNullOrEmpty(txtNroComprobante.Text) || String.IsNullOrWhiteSpace(txtNroComprobante.Text))
            {
                resp.setSuccess(false);
                resp.setMessage(resp.getMessage() + "El numero de comprobante es requerido.\n");
            }

            if (String.IsNullOrEmpty(txtMotivo.Text) || String.IsNullOrWhiteSpace(txtMotivo.Text))
            {
                resp.setSuccess(false);
                resp.setMessage(resp.getMessage() + "El motivo es requerido.\n");
            }

            return resp;
        }

        private void guardarFormulario()
        {
            loadFormToModel();
            DialogResult dialogResp = MessageBox.Show("Esta seguro de anular el comprobante " + this.comprobanteAnulado.getNroComprobante() + "?", "Confirmación.", MessageBoxButtons.YesNo);
            if (dialogResp.Equals(DialogResult.Yes)) {
                RespuestaControl resp = ComprobanteAnuladoControl.guardarComprobanteAnulado(this.comprobanteAnulado);
                if (resp.getSuccess())
                {
                    this.DialogResult = DialogResult.OK;
                    MessageBox.Show(resp.getMessage());
                    this.Close();
                }
                else
                {
                    MessageBox.Show(resp.getMessage());
                }
            }
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            RespuestaControl validacion = validarFormulario();

            if (validacion.getSuccess())
            {
                guardarFormulario();
            }
            else
            {
                lblErrorMessage.Text = validacion.getMessage();
                lblErrorMessage.Visible = true;
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
