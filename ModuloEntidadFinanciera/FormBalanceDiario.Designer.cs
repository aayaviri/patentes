﻿namespace ModuloEntidadFinanciera
{
    partial class FormBalanceDiario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rpvBalanceDiario = new Microsoft.Reporting.WinForms.ReportViewer();
            this.SuspendLayout();
            // 
            // rpvBalanceDiario
            // 
            this.rpvBalanceDiario.LocalReport.ReportEmbeddedResource = "ModuloEntidadFinanciera.ReporteBalanceDiario.rdlc";
            this.rpvBalanceDiario.Location = new System.Drawing.Point(12, 12);
            this.rpvBalanceDiario.Name = "rpvBalanceDiario";
            this.rpvBalanceDiario.Size = new System.Drawing.Size(652, 459);
            this.rpvBalanceDiario.TabIndex = 0;
            // 
            // FormBalanceDiario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(676, 483);
            this.Controls.Add(this.rpvBalanceDiario);
            this.MaximizeBox = false;
            this.Name = "FormBalanceDiario";
            this.Text = "Balance Diario de Caja";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormBalanceDiario_FormClosing);
            this.Load += new System.EventHandler(this.FormBalanceDiario_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer rpvBalanceDiario;
    }
}