﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

using Control;
using Modelo;
using Microsoft.Reporting.WinForms;

namespace ModuloEntidadFinanciera
{
    public partial class FormBalanceDiario : Form
    {
        private Cajero cajero;

        public FormBalanceDiario(Cajero cajero)
        {
            InitializeComponent();
            this.cajero = cajero;
        }

        private void FormBalanceDiario_Load(object sender, EventArgs e)
        {
            DateTime fechaIni = DateTime.Today;
            DateTime fechaFin = fechaIni.AddDays(1);
            DataTable pagosPagados = ReporteControl.getPagosPagadosPorCajeroFechas(cajero.getId(), fechaIni.Date, fechaFin.Date);
            DataTable comprobantesAnulados = ComprobanteAnuladoControl.listarComprobantesAnuladosIdCajeroFechas(cajero.getId(), fechaIni.Date, fechaFin.Date);

            Sucursal sucursal = SucursalControl.getSucursalById(cajero.getIdSucursal());
            EntidadFinanciera entiFin = EntidadFinancieraControl.getEntidadFinancieraById(sucursal.getIdEntidadFinanciera());
            
            this.rpvBalanceDiario.LocalReport.DataSources.Clear();
            ReportDataSource rprtDTSource = new Microsoft.Reporting.WinForms.ReportDataSource("pagosPagadosDataTable", pagosPagados);
            ReportDataSource rprtDTSource2 = new Microsoft.Reporting.WinForms.ReportDataSource("comprobantesAnuladosDataTable", comprobantesAnulados);
            this.rpvBalanceDiario.LocalReport.DataSources.Add(rprtDTSource);
            this.rpvBalanceDiario.LocalReport.DataSources.Add(rprtDTSource2);
            rpvBalanceDiario.LocalReport.SetParameters(new ReportParameter[] {
                new ReportParameter("nombreCajero", cajero.getNombreCompleto()),
                new ReportParameter("fecha", fechaIni.ToShortDateString()),
                new ReportParameter("sucursal", sucursal.getCodigo() + " - " + sucursal.getDireccion()),
                new ReportParameter("entidadFinanciera", entiFin.getNombre())
            });
            this.rpvBalanceDiario.RefreshReport();
            Usuario usuario = UsuarioControl.getUsuarioByid(cajero.getIdUsuario());
            LogControl.logGenerarBalanceDiario(usuario);
        }

        private void FormBalanceDiario_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.rpvBalanceDiario.Reset();
        }
    }
}
