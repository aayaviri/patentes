﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

using Control;
using Modelo;

namespace ModuloEntidadFinanciera
{
    public partial class FormCajero : Form
    {
        private Cajero cajero;
        private EntidadFinanciera entiFin;
        private Usuario usuario;
        private Boolean passwordChanged = false;

        public FormCajero(EntidadFinanciera entiFin)
        {
            InitializeComponent();
            this.cajero = new Cajero();
            this.usuario = new Usuario();
            this.entiFin = entiFin;
            loadComboSucursal();
        }

        public FormCajero(EntidadFinanciera entiFin, Cajero cajero)
        {
            InitializeComponent();
            this.cajero = cajero;
            this.entiFin = entiFin;
            this.usuario = UsuarioControl.getUsuarioByid(this.cajero.getIdUsuario());
            loadComboSucursal();
            loadModelToForm();
            passwordChanged = true;
        }

        private void loadComboSucursal() {
            cbxSucursal.DataSource = SucursalControl.listarSurcursalesPorEntidadFinanciera(entiFin.getId());
            cbxSucursal.DisplayMember = "codigo";
            cbxSucursal.ValueMember = "id";
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            RespuestaControl validacion = validarFormulario();

            if (validacion.getSuccess())
            {
                guardarFormulario();
         
            }
            else {
                lblErrorMessage.Text = validacion.getMessage();
                lblErrorMessage.Visible = true;
            }
        }

        private void loadModelToForm()
        {
            txtCodigo.Text = cajero.getCodigo();
            txtNombre.Text = cajero.getNombreCompleto();
            cbxEstado.SelectedItem = cajero.getEstado();
            cbxSucursal.SelectedValue = cajero.getIdSucursal();

            txtLogin.Text = usuario.getLogin();
            txtPassword.Text = usuario.getPassword();
        }

        private void loadFormToModel()
        {
            cajero.setCodigo(txtCodigo.Text);
            cajero.setNombreCompleto(txtNombre.Text);
            cajero.setEstado(cbxEstado.SelectedItem.ToString());
            cajero.setIdSucursal((int)cbxSucursal.SelectedValue);

            usuario.setLogin(txtLogin.Text);
            usuario.setPassword(txtPassword.Text);
        }

        private void guardarFormulario()
        {
            loadFormToModel();
            RespuestaControl resp = CajeroControl.guardarCajero(this.cajero, this.usuario);
            if (resp.getSuccess())
            {
                
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                MessageBox.Show(resp.getMessage());
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private RespuestaControl validarFormulario()
        {
            RespuestaControl resp = new RespuestaControl();
            
            if(String.IsNullOrEmpty(txtCodigo.Text) || String.IsNullOrWhiteSpace(txtCodigo.Text)){
                resp.setSuccess(false);
                resp.setMessage(resp.getMessage() + "El código es requerido.\n");
            }

            if (String.IsNullOrEmpty(txtNombre.Text) || String.IsNullOrWhiteSpace(txtNombre.Text))
            {
                resp.setSuccess(false);
                resp.setMessage(resp.getMessage() + "El nombre es requerido.\n");
            }

            if (cbxEstado.SelectedItem == null || String.IsNullOrEmpty(cbxEstado.SelectedItem.ToString()) || String.IsNullOrWhiteSpace(cbxEstado.SelectedItem.ToString()))
            {
                resp.setSuccess(false);
                resp.setMessage(resp.getMessage() + "El estado es requerido.\n");
            }

            if (String.IsNullOrEmpty(cbxSucursal.SelectedValue.ToString()) || String.IsNullOrWhiteSpace(cbxSucursal.SelectedValue.ToString())) {
                resp.setSuccess(false);
                resp.setMessage(resp.getMessage() + "La sucursal es requerida.\n");
            }

            if (String.IsNullOrEmpty(txtLogin.Text) || String.IsNullOrWhiteSpace(txtLogin.Text)) {
                resp.setSuccess(false);
                resp.setMessage(resp.getMessage() + "El login es requerido.\n");
            }

            if (!passwordChanged || cajero.getId() == 0)
            {
                Regex r = new Regex("^[a-zA-Z]\\w{8,15}$");
                if (!r.IsMatch(txtPassword.Text))
                {
                    resp.setSuccess(false);
                    resp.setMessage(resp.getMessage() + "La contraseña debe tener como mínimo 8 caracteres y \nun máximo de 15. También debe comenzar por una letra");
                }

            }
            return resp;
        }

        private void txtPassword_TextChanged(object sender, EventArgs e)
        {
            passwordChanged = false;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }
    }
}
