﻿namespace ModuloEntidadFinanciera
{
    partial class FormCambioDosificacion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtNumActual = new System.Windows.Forms.TextBox();
            this.txtNumNuevo = new System.Windows.Forms.TextBox();
            this.lblNumActual = new System.Windows.Forms.Label();
            this.lblNuevoNumero = new System.Windows.Forms.Label();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.lblErrorMessage = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtNumActual
            // 
            this.txtNumActual.Location = new System.Drawing.Point(157, 15);
            this.txtNumActual.Margin = new System.Windows.Forms.Padding(4);
            this.txtNumActual.Name = "txtNumActual";
            this.txtNumActual.ReadOnly = true;
            this.txtNumActual.Size = new System.Drawing.Size(132, 22);
            this.txtNumActual.TabIndex = 0;
            // 
            // txtNumNuevo
            // 
            this.txtNumNuevo.Location = new System.Drawing.Point(157, 47);
            this.txtNumNuevo.Margin = new System.Windows.Forms.Padding(4);
            this.txtNumNuevo.Name = "txtNumNuevo";
            this.txtNumNuevo.Size = new System.Drawing.Size(132, 22);
            this.txtNumNuevo.TabIndex = 1;
            this.txtNumNuevo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNumNuevo_KeyDown);
            // 
            // lblNumActual
            // 
            this.lblNumActual.AutoSize = true;
            this.lblNumActual.Location = new System.Drawing.Point(16, 18);
            this.lblNumActual.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNumActual.Name = "lblNumActual";
            this.lblNumActual.Size = new System.Drawing.Size(101, 17);
            this.lblNumActual.TabIndex = 3;
            this.lblNumActual.Text = "Numero Actual";
            // 
            // lblNuevoNumero
            // 
            this.lblNuevoNumero.AutoSize = true;
            this.lblNuevoNumero.Location = new System.Drawing.Point(16, 50);
            this.lblNuevoNumero.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNuevoNumero.Name = "lblNuevoNumero";
            this.lblNuevoNumero.Size = new System.Drawing.Size(129, 17);
            this.lblNuevoNumero.TabIndex = 4;
            this.lblNuevoNumero.Text = "Nueva Dosificación";
            // 
            // btnGuardar
            // 
            this.btnGuardar.Image = global::ModuloEntidadFinanciera.Properties.Resources.Save_24;
            this.btnGuardar.Location = new System.Drawing.Point(26, 113);
            this.btnGuardar.Margin = new System.Windows.Forms.Padding(4);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(119, 48);
            this.btnGuardar.TabIndex = 2;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Image = global::ModuloEntidadFinanciera.Properties.Resources.cancel_24;
            this.btnCancelar.Location = new System.Drawing.Point(170, 113);
            this.btnCancelar.Margin = new System.Windows.Forms.Padding(4);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(119, 48);
            this.btnCancelar.TabIndex = 5;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // lblErrorMessage
            // 
            this.lblErrorMessage.ForeColor = System.Drawing.Color.Red;
            this.lblErrorMessage.Location = new System.Drawing.Point(12, 77);
            this.lblErrorMessage.Name = "lblErrorMessage";
            this.lblErrorMessage.Size = new System.Drawing.Size(288, 32);
            this.lblErrorMessage.TabIndex = 6;
            this.lblErrorMessage.Text = "label1";
            this.lblErrorMessage.Visible = false;
            // 
            // FormCambioDosificacion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(312, 172);
            this.ControlBox = false;
            this.Controls.Add(this.lblErrorMessage);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.lblNuevoNumero);
            this.Controls.Add(this.lblNumActual);
            this.Controls.Add(this.txtNumNuevo);
            this.Controls.Add(this.txtNumActual);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FormCambioDosificacion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Cambio de Dosificación";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtNumActual;
        private System.Windows.Forms.TextBox txtNumNuevo;
        private System.Windows.Forms.Label lblNumActual;
        private System.Windows.Forms.Label lblNuevoNumero;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Label lblErrorMessage;
    }
}