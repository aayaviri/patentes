﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Control;
using Modelo;

namespace ModuloEntidadFinanciera
{
    public partial class FormCambioDosificacion : Form
    {

        private Cajero cajero;
        public FormCambioDosificacion(Cajero cajero)
        {
            InitializeComponent();
            this.cajero = cajero;
            loadModelToForm();
        }

        private void loadModelToForm() {
            txtNumActual.Text = cajero.getDosificacion().ToString();
        }
        private RespuestaControl validarFormulario()
        {
            RespuestaControl resp = new RespuestaControl();

            if (String.IsNullOrEmpty(txtNumNuevo.Text) || String.IsNullOrWhiteSpace(txtNumNuevo.Text))
            {
                resp.setSuccess(false);
                resp.setMessage(resp.getMessage() + "El numero actual es requerido.\n");
            }

            return resp;
        }
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            RespuestaControl validacion = validarFormulario();

            if (validacion.getSuccess())
            {
                guardarNuevaDosificacion();
            }
            else
            {
                lblErrorMessage.Text = validacion.getMessage();
                lblErrorMessage.Visible = true;
            }
        }

        private void guardarNuevaDosificacion() {
            DialogResult res = MessageBox.Show("¿Esta seguro de cambiar la dosificacion?", "Cambio Dosificación", MessageBoxButtons.YesNo);
            if (res.Equals(DialogResult.Yes))
            {
                cajero.setDosificacion(int.Parse(txtNumNuevo.Text));
                RespuestaControl respControl = CajeroControl.guardarCajero(cajero);
                if (respControl.getSuccess())
                {
                    this.Close();
                }
                else {
                    MessageBox.Show(respControl.getMessage(), "Error");
                }
            }
        }

        private void txtNumNuevo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData.Equals(Keys.Return))
            {
                guardarNuevaDosificacion();
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
