﻿namespace ModuloEntidadFinanciera
{
    partial class FormCobranza
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtApellidoMaterno = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtApellidoPaterno = new System.Windows.Forms.TextBox();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.txtCodigoLicencia = new System.Windows.Forms.TextBox();
            this.dgvSitiosEncontrados = new System.Windows.Forms.DataGridView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dgvPagosPendientes = new System.Windows.Forms.DataGridView();
            this.btnSelecion = new System.Windows.Forms.Button();
            this.btnAnularComprobante = new System.Windows.Forms.Button();
            this.btnCambiarDosificacion = new System.Windows.Forms.Button();
            this.btnPagar = new System.Windows.Forms.Button();
            this.btnLimpiarNombre = new System.Windows.Forms.Button();
            this.btnLimpiarCodigoLicencia = new System.Windows.Forms.Button();
            this.btnBuscarCodigoLicencia = new System.Windows.Forms.Button();
            this.btnBuscarNombreApellidos = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSitiosEncontrados)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPagosPendientes)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnLimpiarNombre);
            this.groupBox1.Controls.Add(this.btnLimpiarCodigoLicencia);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtApellidoMaterno);
            this.groupBox1.Controls.Add(this.btnBuscarCodigoLicencia);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtApellidoPaterno);
            this.groupBox1.Controls.Add(this.txtNombre);
            this.groupBox1.Controls.Add(this.btnBuscarNombreApellidos);
            this.groupBox1.Controls.Add(this.txtCodigoLicencia);
            this.groupBox1.Controls.Add(this.dgvSitiosEncontrados);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(856, 209);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Buscar Sitio";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(603, 23);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Ap. Paterno:";
            // 
            // txtApellidoMaterno
            // 
            this.txtApellidoMaterno.Location = new System.Drawing.Point(675, 19);
            this.txtApellidoMaterno.Name = "txtApellidoMaterno";
            this.txtApellidoMaterno.Size = new System.Drawing.Size(100, 20);
            this.txtApellidoMaterno.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(425, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Ap. Paterno:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(263, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Nombre:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Codigo Licencia";
            // 
            // txtApellidoPaterno
            // 
            this.txtApellidoPaterno.Location = new System.Drawing.Point(497, 20);
            this.txtApellidoPaterno.Name = "txtApellidoPaterno";
            this.txtApellidoPaterno.Size = new System.Drawing.Size(100, 20);
            this.txtApellidoPaterno.TabIndex = 4;
            this.txtApellidoPaterno.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtApellido_KeyDown);
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(316, 19);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(103, 20);
            this.txtNombre.TabIndex = 3;
            this.txtNombre.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNombre_KeyDown);
            // 
            // txtCodigoLicencia
            // 
            this.txtCodigoLicencia.Location = new System.Drawing.Point(95, 19);
            this.txtCodigoLicencia.Name = "txtCodigoLicencia";
            this.txtCodigoLicencia.Size = new System.Drawing.Size(100, 20);
            this.txtCodigoLicencia.TabIndex = 0;
            this.txtCodigoLicencia.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBuscar_KeyDown);
            // 
            // dgvSitiosEncontrados
            // 
            this.dgvSitiosEncontrados.AllowUserToAddRows = false;
            this.dgvSitiosEncontrados.AllowUserToDeleteRows = false;
            this.dgvSitiosEncontrados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSitiosEncontrados.Location = new System.Drawing.Point(6, 46);
            this.dgvSitiosEncontrados.MultiSelect = false;
            this.dgvSitiosEncontrados.Name = "dgvSitiosEncontrados";
            this.dgvSitiosEncontrados.ReadOnly = true;
            this.dgvSitiosEncontrados.RowTemplate.Height = 24;
            this.dgvSitiosEncontrados.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSitiosEncontrados.Size = new System.Drawing.Size(844, 157);
            this.dgvSitiosEncontrados.TabIndex = 0;
            this.dgvSitiosEncontrados.TabStop = false;
            this.dgvSitiosEncontrados.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSitiosEncontrados_CellDoubleClick);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dgvPagosPendientes);
            this.groupBox2.Location = new System.Drawing.Point(12, 227);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(856, 202);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Pagos Pendientes";
            // 
            // dgvPagosPendientes
            // 
            this.dgvPagosPendientes.AllowUserToAddRows = false;
            this.dgvPagosPendientes.AllowUserToDeleteRows = false;
            this.dgvPagosPendientes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPagosPendientes.Location = new System.Drawing.Point(6, 19);
            this.dgvPagosPendientes.Name = "dgvPagosPendientes";
            this.dgvPagosPendientes.ReadOnly = true;
            this.dgvPagosPendientes.RowTemplate.Height = 24;
            this.dgvPagosPendientes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPagosPendientes.Size = new System.Drawing.Size(844, 171);
            this.dgvPagosPendientes.TabIndex = 0;
            this.dgvPagosPendientes.TabStop = false;
            // 
            // btnSelecion
            // 
            this.btnSelecion.Image = global::ModuloEntidadFinanciera.Properties.Resources.Detalle;
            this.btnSelecion.Location = new System.Drawing.Point(556, 435);
            this.btnSelecion.Name = "btnSelecion";
            this.btnSelecion.Size = new System.Drawing.Size(88, 38);
            this.btnSelecion.TabIndex = 11;
            this.btnSelecion.Text = "Ver Detalle";
            this.btnSelecion.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSelecion.UseVisualStyleBackColor = true;
            this.btnSelecion.Click += new System.EventHandler(this.btnSelecion_Click);
            // 
            // btnAnularComprobante
            // 
            this.btnAnularComprobante.Image = global::ModuloEntidadFinanciera.Properties.Resources.cancel_doc_24;
            this.btnAnularComprobante.Location = new System.Drawing.Point(650, 435);
            this.btnAnularComprobante.Name = "btnAnularComprobante";
            this.btnAnularComprobante.Size = new System.Drawing.Size(103, 38);
            this.btnAnularComprobante.TabIndex = 9;
            this.btnAnularComprobante.Text = "Anular Comprobante";
            this.btnAnularComprobante.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAnularComprobante.UseVisualStyleBackColor = true;
            this.btnAnularComprobante.Click += new System.EventHandler(this.btnAnularComprobante_Click);
            // 
            // btnCambiarDosificacion
            // 
            this.btnCambiarDosificacion.Image = global::ModuloEntidadFinanciera.Properties.Resources.edit_form_24;
            this.btnCambiarDosificacion.Location = new System.Drawing.Point(759, 435);
            this.btnCambiarDosificacion.Name = "btnCambiarDosificacion";
            this.btnCambiarDosificacion.Size = new System.Drawing.Size(103, 38);
            this.btnCambiarDosificacion.TabIndex = 10;
            this.btnCambiarDosificacion.Text = "Cambiar Dosificación";
            this.btnCambiarDosificacion.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCambiarDosificacion.UseVisualStyleBackColor = true;
            this.btnCambiarDosificacion.Click += new System.EventHandler(this.btnCambiarDosificacion_Click);
            // 
            // btnPagar
            // 
            this.btnPagar.Image = global::ModuloEntidadFinanciera.Properties.Resources.payment_24;
            this.btnPagar.Location = new System.Drawing.Point(397, 435);
            this.btnPagar.Name = "btnPagar";
            this.btnPagar.Size = new System.Drawing.Size(88, 38);
            this.btnPagar.TabIndex = 8;
            this.btnPagar.Text = "Pagar";
            this.btnPagar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnPagar.UseVisualStyleBackColor = true;
            this.btnPagar.Click += new System.EventHandler(this.btnPagar_Click);
            // 
            // btnLimpiarNombre
            // 
            this.btnLimpiarNombre.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnLimpiarNombre.Image = global::ModuloEntidadFinanciera.Properties.Resources.Clear;
            this.btnLimpiarNombre.Location = new System.Drawing.Point(811, 17);
            this.btnLimpiarNombre.Name = "btnLimpiarNombre";
            this.btnLimpiarNombre.Size = new System.Drawing.Size(26, 24);
            this.btnLimpiarNombre.TabIndex = 7;
            this.btnLimpiarNombre.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnLimpiarNombre.UseVisualStyleBackColor = true;
            this.btnLimpiarNombre.Click += new System.EventHandler(this.btnLimpiar_Click);
            // 
            // btnLimpiarCodigoLicencia
            // 
            this.btnLimpiarCodigoLicencia.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnLimpiarCodigoLicencia.Image = global::ModuloEntidadFinanciera.Properties.Resources.Clear;
            this.btnLimpiarCodigoLicencia.Location = new System.Drawing.Point(231, 17);
            this.btnLimpiarCodigoLicencia.Name = "btnLimpiarCodigoLicencia";
            this.btnLimpiarCodigoLicencia.Size = new System.Drawing.Size(26, 24);
            this.btnLimpiarCodigoLicencia.TabIndex = 2;
            this.btnLimpiarCodigoLicencia.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnLimpiarCodigoLicencia.UseVisualStyleBackColor = true;
            this.btnLimpiarCodigoLicencia.Click += new System.EventHandler(this.btnLimpiar_Click);
            // 
            // btnBuscarCodigoLicencia
            // 
            this.btnBuscarCodigoLicencia.BackColor = System.Drawing.SystemColors.Control;
            this.btnBuscarCodigoLicencia.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBuscarCodigoLicencia.Image = global::ModuloEntidadFinanciera.Properties.Resources.Search_24;
            this.btnBuscarCodigoLicencia.Location = new System.Drawing.Point(201, 17);
            this.btnBuscarCodigoLicencia.Name = "btnBuscarCodigoLicencia";
            this.btnBuscarCodigoLicencia.Size = new System.Drawing.Size(24, 24);
            this.btnBuscarCodigoLicencia.TabIndex = 1;
            this.btnBuscarCodigoLicencia.UseVisualStyleBackColor = false;
            this.btnBuscarCodigoLicencia.Click += new System.EventHandler(this.btnBuscarCodigoLicencia_Click);
            // 
            // btnBuscarNombreApellidos
            // 
            this.btnBuscarNombreApellidos.BackColor = System.Drawing.SystemColors.Control;
            this.btnBuscarNombreApellidos.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBuscarNombreApellidos.Image = global::ModuloEntidadFinanciera.Properties.Resources.Search_24;
            this.btnBuscarNombreApellidos.Location = new System.Drawing.Point(781, 17);
            this.btnBuscarNombreApellidos.Name = "btnBuscarNombreApellidos";
            this.btnBuscarNombreApellidos.Size = new System.Drawing.Size(24, 24);
            this.btnBuscarNombreApellidos.TabIndex = 6;
            this.btnBuscarNombreApellidos.UseVisualStyleBackColor = false;
            this.btnBuscarNombreApellidos.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // FormCobranza
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(880, 485);
            this.Controls.Add(this.btnSelecion);
            this.Controls.Add(this.btnAnularComprobante);
            this.Controls.Add(this.btnCambiarDosificacion);
            this.Controls.Add(this.btnPagar);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormCobranza";
            this.Text = "FormCobranza";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSitiosEncontrados)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPagosPendientes)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnBuscarNombreApellidos;
        private System.Windows.Forms.TextBox txtCodigoLicencia;
        private System.Windows.Forms.DataGridView dgvSitiosEncontrados;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnPagar;
        private System.Windows.Forms.DataGridView dgvPagosPendientes;
        private System.Windows.Forms.TextBox txtApellidoPaterno;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnBuscarCodigoLicencia;
        private System.Windows.Forms.Button btnCambiarDosificacion;
        private System.Windows.Forms.Button btnAnularComprobante;
        private System.Windows.Forms.Button btnLimpiarCodigoLicencia;
        private System.Windows.Forms.Button btnLimpiarNombre;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtApellidoMaterno;
        private System.Windows.Forms.Button btnSelecion;
    }
}