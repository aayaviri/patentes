﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

using Control;
using Modelo;
using ModuloAdministracion;

namespace ModuloEntidadFinanciera
{
    public partial class FormCobranza : Form
    {
        private Gestion gestionActual;
        private Cajero cajero;

        public FormCobranza(Gestion gestion, Cajero cajero)
        {
            InitializeComponent();
            this.gestionActual = gestion;
            this.cajero = cajero;
        }

        private void buscarSitiosCodigoLicencia()
        {
            if (txtCodigoLicencia.Text.Length >= 5)
            {
                DataTable dataTable = SitioMunicipalControl.buscarSitiosMunicipalesPorCodigo(txtCodigoLicencia.Text);

                dgvSitiosEncontrados.DataSource = dataTable;
                dgvSitiosEncontrados.Columns["id"].Visible = false;
                dgvSitiosEncontrados.Columns["descripcion"].Visible = false;
                dgvSitiosEncontrados.Columns["idContribuyente"].Visible = false;

                dgvSitiosEncontrados.Columns["codigoLicencia"].HeaderText = "Codigo Licencia";
                dgvSitiosEncontrados.Columns["direccion"].HeaderText = "Dirección";
                dgvSitiosEncontrados.Columns["estado"].HeaderText = "Estado";
                dgvSitiosEncontrados.Columns["fechaInicio"].HeaderText = "Fecha Inicio";
                dgvSitiosEncontrados.Columns["razonSocial"].HeaderText = "Razon Social";
                dgvSitiosEncontrados.Columns["superficie"].HeaderText = "Superficie";
                dgvSitiosEncontrados.Columns["tipo"].HeaderText = "Tipo";
                dgvSitiosEncontrados.Columns["contribuyente"].HeaderText = "Contribuyente";

                dgvSitiosEncontrados.Refresh();
            }
            else
            {
                MessageBox.Show("Debe introducir al menos 5 caracteres.");
            }
        }

        private void buscarSitiosNombreApellidos()
        {
            if (txtNombre.Text.Length >= 2 && txtApellidoPaterno.Text.Length >= 2 && txtApellidoMaterno.Text.Length >= 2)
            {
                DataTable dataTable = SitioMunicipalControl.buscarSitiosMunicipalesPorNombreApellidos(txtNombre.Text, txtApellidoPaterno.Text, txtApellidoMaterno.Text);

                dgvSitiosEncontrados.DataSource = dataTable;
                dgvSitiosEncontrados.Columns["id"].Visible = false;
                dgvSitiosEncontrados.Columns["descripcion"].Visible = false;
                dgvSitiosEncontrados.Columns["idContribuyente"].Visible = false;

                dgvSitiosEncontrados.Columns["codigoLicencia"].HeaderText = "Codigo Licencia";
                dgvSitiosEncontrados.Columns["direccion"].HeaderText = "Dirección";
                dgvSitiosEncontrados.Columns["estado"].HeaderText = "Estado";
                dgvSitiosEncontrados.Columns["fechaInicio"].HeaderText = "Fecha Inicio";
                dgvSitiosEncontrados.Columns["razonSocial"].HeaderText = "Razon Social";
                dgvSitiosEncontrados.Columns["superficie"].HeaderText = "Superficie";
                dgvSitiosEncontrados.Columns["tipo"].HeaderText = "Tipo";
                dgvSitiosEncontrados.Columns["contribuyente"].HeaderText = "Contribuyente";

                dgvSitiosEncontrados.Refresh();
            }
            else
            {
                MessageBox.Show("Los campos del contribuyente deben llevar al menos 2 caracteres cada uno.");
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            buscarSitiosNombreApellidos();
        }

        private void loadDgvPagosPendientes(int idSitioMunicipal)
        {
            DataTable dataTable = PagoControl.getPagosPendientesPorIdSitioMunicipal(idSitioMunicipal);
            dataTable = PagoControl.calcularPagosPendientes(dataTable, gestionActual, cajero);
            dgvPagosPendientes.DataSource = dataTable;
            dgvPagosPendientes.Columns["id"].Visible = false;
            dgvPagosPendientes.Columns["idCajero"].Visible = false;
            dgvPagosPendientes.Columns["idSitioMunicipal"].Visible = false;
            dgvPagosPendientes.Columns["nroComprobante"].Visible = false;
            dgvPagosPendientes.Columns["fechaPago"].Visible = false;
            dgvPagosPendientes.Columns["estado"].Visible = false;
            dgvPagosPendientes.Columns["ufvPago"].Visible = false;

            dgvPagosPendientes.Columns["gestion"].HeaderText = "Gestion";
            dgvPagosPendientes.Columns["impuestoDeterminadoBs"].HeaderText = "Impuesto Determ. Bs";
            dgvPagosPendientes.Columns["ufvVenc"].HeaderText = "Ufv Venc.";
            dgvPagosPendientes.Columns["impuestoDeterminadoUfv"].HeaderText = "Impuesto Determ. Ufv";
            dgvPagosPendientes.Columns["descuentoUfv"].HeaderText = "Descto. Ufv";
            dgvPagosPendientes.Columns["interesUfv"].HeaderText = "Interés Ufv";
            dgvPagosPendientes.Columns["multaIDF"].HeaderText = "Multa IDF";
            dgvPagosPendientes.Columns["descuentoAplicado"].HeaderText = "Descto. Aplicado";
            dgvPagosPendientes.Columns["deudaTributariaUfv"].HeaderText = "Deuda Trib. Ufv";
            dgvPagosPendientes.Columns["total"].HeaderText = "Total";

            dgvPagosPendientes.Refresh();
        }

        private void dgvSitiosEncontrados_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            loadDgvPagosPendientes((int)dgvSitiosEncontrados.SelectedRows[0].Cells[0].Value);
        }



        private void btnPagar_Click(object sender, EventArgs e)
        {
            validarPago();
        }

        private void validarPago()
        {
            if (dgvPagosPendientes.SelectedRows.Count > 0)
            {
                if (validarMasAntiguo())
                {
                    FormComprobante formComprobante = new FormComprobante(cajero);
                    formComprobante.ShowDialog();
                    if (formComprobante.DialogResult.Equals(DialogResult.OK))
                    {
                        DataTable dataTable = (DataTable)dgvPagosPendientes.DataSource;
                        cobranza_patentesDataSet.pagoDataTable pagosDataTable = new cobranza_patentesDataSet.pagoDataTable();
                        SitioMunicipalDao sitioDao = new SitioMunicipalDao();

                        SitioMunicipal sitioMunicipal = sitioDao.getById((int)dgvSitiosEncontrados.SelectedRows[0].Cells[0].Value);

                        for (int i = 0; i < dgvPagosPendientes.SelectedRows.Count; i++)
                        {
                            int index = dgvPagosPendientes.SelectedRows[i].Index;

                            DataRow pagoRow = dataTable.Rows[index];
                            pagoRow["nroComprobante"] = cajero.getDosificacion();
                            pagosDataTable.ImportRow(pagoRow);
                        }
                        FormConfirmarPago formConfirmarPago = new FormConfirmarPago(pagosDataTable, cajero, sitioMunicipal);
                        formConfirmarPago.ShowDialog();
                        loadDgvPagosPendientes(sitioMunicipal.getId());
                    }
                    else if (formComprobante.DialogResult.Equals(DialogResult.No))
                    {
                        MessageBox.Show("El número de fomulario no concuerda con el seguimiento del sistema: " + cajero.getDosificacion() + ". \nPor favor revise el correlativo.");
                    }
                }
                else
                {
                    MessageBox.Show("Debe pagar las gestiones mas antiguas primero.");
                }
            }
            else
            {
                MessageBox.Show("Debe seleccionar al menos una gestion a pagar.");
            }
        }

        private Boolean validarMasAntiguo()
        {
            Boolean resp = true;

            DataTable dataTable = (DataTable)dgvPagosPendientes.DataSource;
            int gestionMasAntigua = (int)dataTable.Rows[0]["gestion"];
            int gestionRow;
            foreach (DataRow row in dataTable.Rows)
            {
                gestionRow = (int)row["gestion"];
                if (gestionRow < gestionMasAntigua)
                {
                    gestionMasAntigua = gestionRow;
                }
            }

            Debug.WriteLine("Gestion mas antigua: " + gestionMasAntigua);

            for (int i = 1; i <= dgvPagosPendientes.SelectedRows.Count; i++)
            {
                if (!buscarGestion(gestionMasAntigua, dgvPagosPendientes))
                {
                    resp = false;
                    break;
                }
                gestionMasAntigua++;
            }
            Debug.WriteLine(resp);
            return resp;
        }

        private Boolean buscarGestion(int gestion, DataGridView dgv)
        {
            Boolean resp = false;
            foreach (DataGridViewRow row in dgv.SelectedRows)
            {
                if (gestion == (int)row.Cells["gestion"].Value)
                {
                    resp = true;
                    break;
                }
            }
            return resp;
        }

        private void txtBuscar_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData.Equals(Keys.Return))
            {
                buscarSitiosCodigoLicencia();
            }
        }

        private void txtNombre_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData.Equals(Keys.Return))
            {
                buscarSitiosNombreApellidos();
            }
        }

        private void txtApellido_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData.Equals(Keys.Return))
            {
                buscarSitiosNombreApellidos();
            }
        }

        private void btnBuscarCodigoLicencia_Click(object sender, EventArgs e)
        {
            buscarSitiosCodigoLicencia();
        }

        private void btnCambiarDosificacion_Click(object sender, EventArgs e)
        {
            FormCambioDosificacion formCambioDosificacion = new FormCambioDosificacion(this.cajero);
            formCambioDosificacion.ShowDialog();
        }

        private void btnAnularComprobante_Click(object sender, EventArgs e)
        {
            FormAnularComprobante formAnularComp = new FormAnularComprobante(this.cajero);
            formAnularComp.ShowDialog();
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            txtNombre.Text = "";
            txtApellidoPaterno.Text = "";
            txtApellidoMaterno.Text = "";
            txtCodigoLicencia.Text = "";
            if (dgvSitiosEncontrados.DataSource != null)
                ((DataTable)dgvSitiosEncontrados.DataSource).Rows.Clear();
            if (dgvPagosPendientes.DataSource != null) 
                ((DataTable)dgvPagosPendientes.DataSource).Rows.Clear();
        }

        private void btnSelecion_Click(object sender, EventArgs e)
        {
            if (dgvSitiosEncontrados.CurrentCell != null)
            {

                int row = Convert.ToInt16(dgvSitiosEncontrados.CurrentCell.RowIndex);
                int id = Convert.ToInt32(dgvSitiosEncontrados.Rows[row].Cells[0].Value);
                SitioMunicipal sitioSeleccionado = SitioMunicipalControl.getSitioMunicipalById(id);
                FormSitioMunicipal vistaDetalle = new FormSitioMunicipal(sitioSeleccionado);
                vistaDetalle.Show();
            }
        }
    }
}
