﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Modelo;
using Control;

namespace ModuloEntidadFinanciera
{
    public partial class FormComprobante : Form
    {
        private Cajero cajero;

        public FormComprobante(Cajero cajero)
        {
            InitializeComponent();
            this.cajero = cajero;
        }

        public void registrarNumeroComprobante() {
            if (cajero.getDosificacion() == int.Parse(txtNumeroComprobante.Text))
            {
                this.DialogResult = DialogResult.OK;
            }
            else {
                this.DialogResult = DialogResult.No;
            }
            this.Close();
        }
        private RespuestaControl validarFormulario()
        {
            RespuestaControl resp = new RespuestaControl();

            if (String.IsNullOrEmpty(txtNumeroComprobante.Text) || String.IsNullOrWhiteSpace(txtNumeroComprobante.Text))
            {
                resp.setSuccess(false);
                resp.setMessage(resp.getMessage() + "El numero de comprobante es requerido.\n");
            }

            return resp;
        }
        private void btnAceptar_Click(object sender, EventArgs e)
        {
            RespuestaControl validacion = validarFormulario();

            if (validacion.getSuccess())
            {
                registrarNumeroComprobante();
            }
            else
            {
                lblErrorMessage.Text = validacion.getMessage();
                lblErrorMessage.Visible = true;
            }
            
        }

        private void txtNumeroComprobante_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData.Equals(Keys.Return))
            {
                registrarNumeroComprobante();
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
