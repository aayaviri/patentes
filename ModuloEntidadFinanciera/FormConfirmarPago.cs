﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using System.Diagnostics;

using Modelo;
using Control;
using System.Globalization;

namespace ModuloEntidadFinanciera
{
    public partial class FormConfirmarPago : Form
    {
        private Cajero cajero;
        private DataTable pagosDataTable;
        private SitioMunicipal sitioMunicipal;
        private Usuario usuario;
        private Boolean printing = false;
        
        public FormConfirmarPago(DataTable pagosDataTable, Cajero cajero, SitioMunicipal sitioMunicipal)
        {
            InitializeComponent();
            this.pagosDataTable = pagosDataTable;
            this.cajero = cajero;
            this.sitioMunicipal = sitioMunicipal;
        }

        private void FormConfirmarPago_Load(object sender, EventArgs e)
        {
            renderReporteCobranza(printing);
            LogControl.logGenerarComprobantePago(usuario, sitioMunicipal);
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnProcesarPago_Click(object sender, EventArgs e)
        {
            PagoDao pagoDao = new PagoDao();
            List<Pago> pagoList = new List<Pago>();
            foreach (DataRow pagoDataRow in pagosDataTable.Rows)
            {
                Pago pago = pagoDao.loadDataRowToModel(pagoDataRow);
                pagoList.Add(pago);
            }

            if (PagoControl.procesarPagos(pagoList))
            {
                cajero.setDosificacion(cajero.getDosificacion() + 1);
                RespuestaControl respControl = CajeroControl.guardarCajero(cajero);
                if (!respControl.getSuccess())
                {
                    MessageBox.Show(respControl.getMessage());
                }
                else
                {
                    LogControl.logPagarSitio(usuario, sitioMunicipal, pagoList);
                    MessageBox.Show("Pago realizado exitósamente");
                    
                    printing = true;
                    reportViewer1.LocalReport.SetParameters(new ReportParameter("printLayout", "true"));
                    this.reportViewer1.RefreshReport();
                }
            }
            else
            {
                MessageBox.Show("Ocurrio un error al procesar el pago. Intente de nuevo por favor");
                this.Close();
            }
        }

        private void FormConfirmarPago_FormClosing(object sender, FormClosingEventArgs e)
        {
            reportViewer1.Reset();
        }

        private void renderReporteCobranza(Boolean printing) {
            String printLayout = "false";
            if (printing) {
                printLayout = "true";
            }

            ContribuyenteDao contribuyenteDao = new ContribuyenteDao();
            SitioMunicipalDao sitioMunicipalDao = new SitioMunicipalDao();
            CajeroDao cajeroDao = new CajeroDao();
            SucursalDao sucursalDao = new SucursalDao();
            EntidadFinancieraDao entifinDao = new EntidadFinancieraDao();
            usuario = UsuarioControl.getUsuarioByid(cajero.getIdUsuario());

            //SitioMunicipal sitioMunicipal = sitioMunicipalDao.getById(pago.getIdSitioMunicipal());
            Contribuyente contribuyente = contribuyenteDao.getById((int)sitioMunicipal.getIdContribuyente());
            Sucursal sucursal = sucursalDao.getById(cajero.getIdSucursal());
            EntidadFinanciera entifin = entifinDao.getById(sucursal.getIdEntidadFinanciera());

            String dia, mes, anio;
            dia = DateTime.Today.Day.ToString();
            mes = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(DateTime.Today.Month).ToUpper();
            anio = DateTime.Today.Year.ToString();
            Double total = PagoControl.getPagosTotal(pagosDataTable);

            NumLetra nl = new NumLetra();

            this.reportViewer1.LocalReport.DataSources.Clear();
            ReportDataSource rprtDTSource = new Microsoft.Reporting.WinForms.ReportDataSource("pagosPagadosDataTable", pagosDataTable);
            this.reportViewer1.LocalReport.DataSources.Add(rprtDTSource);
            reportViewer1.LocalReport.SetParameters(new ReportParameter[] {
                new ReportParameter("nombreCompleto", contribuyente.getNombre() + " " + contribuyente.getApellidoPaterno() + " " + contribuyente.getApellidoMaterno()),
                new ReportParameter("ci", contribuyente.getCi()),
                new ReportParameter("direccion", sitioMunicipal.getDireccion()),
                new ReportParameter("actividad", sitioMunicipal.getDescripcion()),
                new ReportParameter("total", total.ToString()),
                new ReportParameter("dia", dia),
                new ReportParameter("mes", mes),
                new ReportParameter("anio", anio),
                new ReportParameter("superficie", sitioMunicipal.getSuperficie().ToString()),
                new ReportParameter("codigo", sitioMunicipal.getCodigoLicencia()),
                new ReportParameter("totalLiteral", nl.Convertir(total.ToString(),true)),
                new ReportParameter("entidadFinanciera", entifin.getNombre()),
                new ReportParameter("sucursal", sucursal.getCodigo()),
                new ReportParameter("direccionPago", sucursal.getDireccion()),
                new ReportParameter("cajero", cajero.getNombreCompleto()),
                new ReportParameter("nroComprobante", cajero.getDosificacion().ToString()),
                new ReportParameter("printLayout", printLayout)
            });
            
            this.reportViewer1.RefreshReport();
        }

        private void reportViewer1_RenderingComplete(object sender, RenderingCompleteEventArgs e)
        {
            Debug.WriteLine("RenderingComplete");
            Debug.WriteLine(printing);
            if (printing) {
                this.reportViewer1.PrintDialog();
                this.DialogResult = DialogResult.OK;
            }
        }

        private void reportViewer1_Print(object sender, ReportPrintEventArgs e)
        {
            //reportViewer1.LocalReport.SetParameters(new ReportParameter("printLayout", "false"));
            //this.reportViewer1.RefreshReport();
            
        }

        private void reportViewer1_StatusChanged(object sender, EventArgs e)
        {
            
        }

    }
}
