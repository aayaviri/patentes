﻿namespace ModuloEntidadFinanciera
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnMain5 = new System.Windows.Forms.Button();
            this.lbCajeroAdmin = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnMain4 = new System.Windows.Forms.Button();
            this.lblEntidadFinanciera = new System.Windows.Forms.Label();
            this.lblNombreCajero = new System.Windows.Forms.Label();
            this.lblTitleUfvActual = new System.Windows.Forms.Label();
            this.lblUfvActual = new System.Windows.Forms.Label();
            this.btnMain3 = new System.Windows.Forms.Button();
            this.btnMain2 = new System.Windows.Forms.Button();
            this.btnMain1 = new System.Windows.Forms.Button();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.btnMain6 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnMain6);
            this.panel1.Controls.Add(this.btnMain5);
            this.panel1.Controls.Add(this.lbCajeroAdmin);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.btnMain4);
            this.panel1.Controls.Add(this.lblEntidadFinanciera);
            this.panel1.Controls.Add(this.lblNombreCajero);
            this.panel1.Controls.Add(this.lblTitleUfvActual);
            this.panel1.Controls.Add(this.lblUfvActual);
            this.panel1.Controls.Add(this.btnMain3);
            this.panel1.Controls.Add(this.btnMain2);
            this.panel1.Controls.Add(this.btnMain1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(894, 70);
            this.panel1.TabIndex = 1;
            // 
            // btnMain5
            // 
            this.btnMain5.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnMain5.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnMain5.Image = global::ModuloEntidadFinanciera.Properties.Resources.pdf_48;
            this.btnMain5.Location = new System.Drawing.Point(300, 0);
            this.btnMain5.Margin = new System.Windows.Forms.Padding(2);
            this.btnMain5.Name = "btnMain5";
            this.btnMain5.Size = new System.Drawing.Size(75, 70);
            this.btnMain5.TabIndex = 5;
            this.btnMain5.UseVisualStyleBackColor = true;
            this.btnMain5.Click += new System.EventHandler(this.button1_Click);
            // 
            // lbCajeroAdmin
            // 
            this.lbCajeroAdmin.AutoSize = true;
            this.lbCajeroAdmin.Location = new System.Drawing.Point(659, 29);
            this.lbCajeroAdmin.Name = "lbCajeroAdmin";
            this.lbCajeroAdmin.Size = new System.Drawing.Size(43, 13);
            this.lbCajeroAdmin.TabIndex = 3;
            this.lbCajeroAdmin.Text = "AdmCaj";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(658, 9);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Entidad Financiera";
            // 
            // label1
            // 
            this.label1.AllowDrop = true;
            this.label1.AutoEllipsis = true;
            this.label1.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.SteelBlue;
            this.label1.Location = new System.Drawing.Point(380, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(273, 66);
            this.label1.TabIndex = 8;
            this.label1.Text = "TITULO";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btnMain4
            // 
            this.btnMain4.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnMain4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnMain4.Location = new System.Drawing.Point(225, 0);
            this.btnMain4.Name = "btnMain4";
            this.btnMain4.Size = new System.Drawing.Size(75, 70);
            this.btnMain4.TabIndex = 3;
            this.btnMain4.UseVisualStyleBackColor = true;
            this.btnMain4.Click += new System.EventHandler(this.btnMain4_Click);
            // 
            // lblEntidadFinanciera
            // 
            this.lblEntidadFinanciera.AutoSize = true;
            this.lblEntidadFinanciera.Location = new System.Drawing.Point(758, 9);
            this.lblEntidadFinanciera.Name = "lblEntidadFinanciera";
            this.lblEntidadFinanciera.Size = new System.Drawing.Size(95, 13);
            this.lblEntidadFinanciera.TabIndex = 6;
            this.lblEntidadFinanciera.Text = "Entidad Financiera";
            this.lblEntidadFinanciera.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblNombreCajero
            // 
            this.lblNombreCajero.AutoSize = true;
            this.lblNombreCajero.Location = new System.Drawing.Point(758, 29);
            this.lblNombreCajero.Name = "lblNombreCajero";
            this.lblNombreCajero.Size = new System.Drawing.Size(75, 13);
            this.lblNombreCajero.TabIndex = 5;
            this.lblNombreCajero.Text = "nombre Cajero";
            this.lblNombreCajero.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblTitleUfvActual
            // 
            this.lblTitleUfvActual.AutoSize = true;
            this.lblTitleUfvActual.Location = new System.Drawing.Point(658, 48);
            this.lblTitleUfvActual.Name = "lblTitleUfvActual";
            this.lblTitleUfvActual.Size = new System.Drawing.Size(60, 13);
            this.lblTitleUfvActual.TabIndex = 4;
            this.lblTitleUfvActual.Text = "Ufv Actual:";
            // 
            // lblUfvActual
            // 
            this.lblUfvActual.AutoSize = true;
            this.lblUfvActual.Location = new System.Drawing.Point(758, 48);
            this.lblUfvActual.Name = "lblUfvActual";
            this.lblUfvActual.Size = new System.Drawing.Size(52, 13);
            this.lblUfvActual.TabIndex = 3;
            this.lblUfvActual.Text = "ufvActual";
            // 
            // btnMain3
            // 
            this.btnMain3.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnMain3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnMain3.Image = global::ModuloEntidadFinanciera.Properties.Resources.pdf_48;
            this.btnMain3.Location = new System.Drawing.Point(150, 0);
            this.btnMain3.Name = "btnMain3";
            this.btnMain3.Size = new System.Drawing.Size(75, 70);
            this.btnMain3.TabIndex = 2;
            this.btnMain3.UseVisualStyleBackColor = true;
            this.btnMain3.Click += new System.EventHandler(this.btnMain3_Click);
            // 
            // btnMain2
            // 
            this.btnMain2.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnMain2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnMain2.Image = global::ModuloEntidadFinanciera.Properties.Resources.edit_forms_48;
            this.btnMain2.Location = new System.Drawing.Point(75, 0);
            this.btnMain2.Name = "btnMain2";
            this.btnMain2.Size = new System.Drawing.Size(75, 70);
            this.btnMain2.TabIndex = 1;
            this.btnMain2.UseVisualStyleBackColor = true;
            this.btnMain2.Click += new System.EventHandler(this.btnMain2_Click);
            // 
            // btnMain1
            // 
            this.btnMain1.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnMain1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnMain1.Image = global::ModuloEntidadFinanciera.Properties.Resources.coins_48;
            this.btnMain1.Location = new System.Drawing.Point(0, 0);
            this.btnMain1.Name = "btnMain1";
            this.btnMain1.Size = new System.Drawing.Size(75, 70);
            this.btnMain1.TabIndex = 0;
            this.btnMain1.Tag = "Inicio";
            this.btnMain1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnMain1.UseVisualStyleBackColor = true;
            this.btnMain1.Click += new System.EventHandler(this.btnMain1_Click);
            // 
            // btnMain6
            // 
            this.btnMain6.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnMain6.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnMain6.Image = global::ModuloEntidadFinanciera.Properties.Resources.exit_48;
            this.btnMain6.Location = new System.Drawing.Point(375, 0);
            this.btnMain6.Name = "btnMain6";
            this.btnMain6.Size = new System.Drawing.Size(75, 70);
            this.btnMain6.TabIndex = 4;
            this.btnMain6.UseVisualStyleBackColor = true;
            this.btnMain6.Click += new System.EventHandler(this.btnMain6_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(894, 594);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.IsMdiContainer = true;
            this.MaximizeBox = false;
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Sistema de Cobro de Patentes";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormMain_FormClosed);
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnMain3;
        private System.Windows.Forms.Button btnMain2;
        private System.Windows.Forms.Button btnMain1;
        private System.Windows.Forms.Label lblTitleUfvActual;
        private System.Windows.Forms.Label lblUfvActual;
        private System.Windows.Forms.Label lblEntidadFinanciera;
        private System.Windows.Forms.Label lblNombreCajero;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.Button btnMain4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnMain5;
        private System.Windows.Forms.Label lbCajeroAdmin;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnMain6;
    }
}