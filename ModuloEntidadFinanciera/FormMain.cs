﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

using Control;
using Modelo;
using System.Threading;

namespace ModuloEntidadFinanciera
{
    public partial class FormMain : Form
    {
        private Usuario usuario;
        private EntidadFinanciera entiFin;
        private Sucursal sucursal;
        private Gestion gestion;
        private Cajero cajero;
        private AdminEntidadFinanciera adminEntiFin;

        public FormMain(Usuario usuario)
        {
            InitializeComponent();
            this.usuario = usuario;
           
        }

        private void FormMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            LogControl.logSalidaDelSistema(usuario);
            Application.Exit();
        }

        private Boolean verificarUfvActual() {
            Boolean resp = true;
            gestion = GestionControl.obtenerUltimaGestion();
            if (!GestionControl.verificarUfvHoy(gestion)) {
                FormRegistrarUfv formUfv = new FormRegistrarUfv(gestion);
                formUfv.ShowDialog();
                if (!formUfv.DialogResult.Equals(DialogResult.OK)) {
                    resp = false;
                }
            }
            return resp;
        }

        private void loadVistaAdmin() {
            lblUfvActual.Hide();
            lblTitleUfvActual.Hide();

            //Diseño de los botones
            btnMain1.Image = global::ModuloEntidadFinanciera.Properties.Resources.users_48;
            btnMain2.Image = global::ModuloEntidadFinanciera.Properties.Resources.pdf_48;
            btnMain3.Image = global::ModuloEntidadFinanciera.Properties.Resources.keys_48;

            toolTip.SetToolTip(btnMain1, "Sucursales/Cajeros");
            toolTip.SetToolTip(btnMain2, "Reporte Entidad Financiera");
            toolTip.SetToolTip(btnMain3, "Cambiar Contraseña");
            toolTip.SetToolTip(btnMain5, "Reporte Resumen de Entidad Financiera");
            toolTip.SetToolTip(btnMain6, "Salir");
            btnMain4.Hide();

            this.adminEntiFin = AdminEntidadFinancieraControl.getAdminEntidadFinancieraByIdUsuario(usuario.getId());
            this.entiFin = EntidadFinancieraControl.getEntidadFinancieraById((int)this.adminEntiFin.getIdEntidadFinanciera());

            lblEntidadFinanciera.Text = entiFin.getNombre();
            lblNombreCajero.Text = adminEntiFin.getNombreCompleto();
            
            FormAdminEntidadFinanciera formAdmin = new FormAdminEntidadFinanciera(this.entiFin, this.usuario);
            formAdmin.MdiParent = this;
            formAdmin.Dock = DockStyle.Fill;
        }

        private void loadVistaCajero() {
            if (!verificarUfvActual()) {
                LogControl.logSalidaDelSistema(usuario);
                Thread.Sleep(3000);
                Application.Exit();
            }
            lblUfvActual.Text = gestion.getUfv().ToString();

            //Diseno de los botones
            btnMain1.Image = global::ModuloEntidadFinanciera.Properties.Resources.coins_48;
            btnMain2.Image = global::ModuloEntidadFinanciera.Properties.Resources.pdf_48;
            btnMain3.Image = global::ModuloEntidadFinanciera.Properties.Resources.keys_48;

            toolTip.SetToolTip(btnMain1, "Cobro Patentes");
            toolTip.SetToolTip(btnMain2, "Informe Diario");
            toolTip.SetToolTip(btnMain3, "Cambio de Contraseña");
            toolTip.SetToolTip(btnMain6, "Salir");

            btnMain4.Hide();
            btnMain5.Hide();

            this.cajero = CobranzaControl.getCajeroByIdUsuario(usuario.getId());
            // se debe mejorar esta parte
            SucursalDao sucursalDao = new SucursalDao();
            sucursal = sucursalDao.getById(cajero.getIdSucursal());

            EntidadFinancieraDao entiFinDao = new EntidadFinancieraDao();
            entiFin = entiFinDao.getById(sucursal.getIdEntidadFinanciera());

            lblEntidadFinanciera.Text = entiFin.getNombre();
            lblNombreCajero.Text = cajero.getNombreCompleto();
            

            FormCobranza formCobranza = new FormCobranza(this.gestion, this.cajero);
            formCobranza.MdiParent = this;
            formCobranza.Dock = DockStyle.Fill;

        }

        private void btnMain1_Click(object sender, EventArgs e)
        {
            MdiChildren[0].Show();
            MdiChildren[0].Activate();
        }

        private void btnMain2_Click(object sender, EventArgs e)
        {
            if (this.usuario.getRol().Equals("cajero"))
            {
                FormBalanceDiario formBalance = new FormBalanceDiario(this.cajero);
                formBalance.ShowDialog();
            }
            else
            {
                FormReporteEntidadFinanciera formReporteEntidad = new FormReporteEntidadFinanciera(this.entiFin, this.usuario);
                formReporteEntidad.ShowDialog();
            }
        }

        private void btnMain3_Click(object sender, EventArgs e)
        {
            FormCambioPassword formCabioPass = new FormCambioPassword(this.usuario);
            formCabioPass.ShowDialog();
        }

        private void btnMain4_Click(object sender, EventArgs e)
        {
            
        }


        private void FormMain_Load(object sender, EventArgs e)
        {
            if (this.usuario.getRol().Equals("cajero"))
            {
                loadVistaCajero();
                label1.Text = "COBRO DE PATENTES DE SITIOS MUNICIPALES";
                lbCajeroAdmin.Text = "Cajero";
            }
            else
            {
                loadVistaAdmin();
                label1.Text = "ADMINISTRACIÓN DE ENTIDAD FINANCIERA";
                lbCajeroAdmin.Text = "Administrador";
            }
            MdiChildren[0].Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FormReporteResumenEntiFin reportResumenEntiFin = new FormReporteResumenEntiFin(this.entiFin);
            reportResumenEntiFin.ShowDialog();
        }

        private void btnMain6_Click(object sender, EventArgs e)
        {
            DialogResult rs = MessageBox.Show("Esta seguro de salir?", "Salir del sistema", MessageBoxButtons.YesNo);
            if (rs == DialogResult.Yes)
            {
                this.Close();
            }
        }
    }
}
