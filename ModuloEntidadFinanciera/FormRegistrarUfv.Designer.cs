﻿namespace ModuloEntidadFinanciera
{
    partial class FormRegistrarUfv
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtUfvHoy = new System.Windows.Forms.TextBox();
            this.btnGuardarUfv = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtUfvHoy
            // 
            this.txtUfvHoy.Location = new System.Drawing.Point(12, 12);
            this.txtUfvHoy.MaxLength = 7;
            this.txtUfvHoy.Name = "txtUfvHoy";
            this.txtUfvHoy.Size = new System.Drawing.Size(184, 20);
            this.txtUfvHoy.TabIndex = 1;
            this.txtUfvHoy.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtUfvHoy_KeyDown);
            // 
            // btnGuardarUfv
            // 
            this.btnGuardarUfv.Image = global::ModuloEntidadFinanciera.Properties.Resources.Save_24;
            this.btnGuardarUfv.Location = new System.Drawing.Point(12, 47);
            this.btnGuardarUfv.Name = "btnGuardarUfv";
            this.btnGuardarUfv.Size = new System.Drawing.Size(89, 39);
            this.btnGuardarUfv.TabIndex = 2;
            this.btnGuardarUfv.Text = "Guardar";
            this.btnGuardarUfv.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnGuardarUfv.UseVisualStyleBackColor = true;
            this.btnGuardarUfv.Click += new System.EventHandler(this.btnGuardarUfv_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Image = global::ModuloEntidadFinanciera.Properties.Resources.cancel_24;
            this.btnCancelar.Location = new System.Drawing.Point(107, 47);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(89, 39);
            this.btnCancelar.TabIndex = 3;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // FormRegistrarUfv
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(207, 109);
            this.ControlBox = false;
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnGuardarUfv);
            this.Controls.Add(this.txtUfvHoy);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FormRegistrarUfv";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Registrar UFV Actual";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtUfvHoy;
        private System.Windows.Forms.Button btnGuardarUfv;
        private System.Windows.Forms.Button btnCancelar;
    }
}