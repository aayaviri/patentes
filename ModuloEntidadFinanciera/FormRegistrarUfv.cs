﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Modelo;
using Control;

namespace ModuloEntidadFinanciera
{
    public partial class FormRegistrarUfv : Form
    {
        private Gestion gestion;
        public FormRegistrarUfv(Gestion gestion)
        {
            InitializeComponent();
            this.gestion = gestion;
        }

        public FormRegistrarUfv()
        {
            // TODO: Complete member initialization
        }

        private void guardarUfv() {
            Double UFT;
            
            UFT = Double.Parse(txtUfvHoy.Text);
            
            gestion.setUfv(UFT);
            DialogResult respDialog = MessageBox.Show("Esta seguro de ingresar la UFV: "+ txtUfvHoy.Text, "Confirmación", MessageBoxButtons.YesNo);
            if (respDialog.Equals(DialogResult.Yes))
            {
                if (GestionControl.guardarNuevaUfv(gestion))
                {
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Ocurrio un error al guardar la ufv");
                }
            }
        }

        private void btnGuardarUfv_Click(object sender, EventArgs e)
        {
            guardarUfv();
        }

        private void txtUfvHoy_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData.Equals(Keys.Return))
            {
                RespuestaControl validacion = validarFormulario();
                if (validacion.getSuccess())
                {
                    guardarUfv();
                }
                else {
                    MessageBox.Show(validacion.getMessage());
                }
                    
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private RespuestaControl validarFormulario() { 
            RespuestaControl resp = new RespuestaControl();
            Double result;
            if (!Double.TryParse(txtUfvHoy.Text, out result))
            {
                resp.setSuccess(false);
                resp.setMessage("La ufv ingresada no es un valor permitido");
            }
            else {
                if (txtUfvHoy.Text.IndexOf('.') != 1) {
                    resp.setSuccess(false);
                    resp.setMessage("La ufv ingresada no es un valor permitido");
                }
            }
            return resp;
        }
    }
}
