﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Control;
using Modelo;
using Microsoft.Reporting.WinForms;

namespace ModuloEntidadFinanciera
{
    public partial class FormReporteEntidadFinanciera : Form
    {
        private EntidadFinanciera entidadFinanciera;
        private Usuario usuario;

        public FormReporteEntidadFinanciera(EntidadFinanciera entidadFinanciera, Usuario usuario)
        {
            InitializeComponent();
            this.entidadFinanciera = entidadFinanciera;
            cbxSucursal.DataSource = SucursalControl.listarSurcursalesPorEntidadFinanciera(this.entidadFinanciera.getId());
            cbxSucursal.ValueMember = "id";
            cbxSucursal.DisplayMember = "codigo";
            this.usuario = usuario;
        }

        private void btnVerReporte_Click(object sender, EventArgs e)
        {
            loadReporteEntidadFinanciera();
        }

        private void loadReporteEntidadFinanciera() {
            DateTime fechaInicio = dtpFechaInicio.Value;
            DateTime fechaFin = dtpFechaFin.Value.AddDays(1);
            int idSucursal = (int)cbxSucursal.SelectedValue;
            Sucursal sucursal = SucursalControl.getSucursalById(idSucursal);
            DataTable pagosPagados = ReporteControl.getPagosPagadosPorSucursalFechas(idSucursal, fechaInicio.Date, fechaFin.Date);
            DataTable comprobantesAnulados = ReporteControl.getComprobantesAnuladosPorSucursalFechas(idSucursal, fechaInicio.Date, fechaFin.Date);
            
            this.rpvEntidadFinanciera.LocalReport.DataSources.Clear();
            ReportDataSource rprtDTSource = new Microsoft.Reporting.WinForms.ReportDataSource("pagosPagadosDataTable", pagosPagados);
            ReportDataSource rprtDTSource2 = new Microsoft.Reporting.WinForms.ReportDataSource("comprobantesAnuladosDataTable", comprobantesAnulados);
            
            this.rpvEntidadFinanciera.LocalReport.DataSources.Add(rprtDTSource);
            this.rpvEntidadFinanciera.LocalReport.DataSources.Add(rprtDTSource2);

            rpvEntidadFinanciera.LocalReport.SetParameters(new ReportParameter[] {
                new ReportParameter("fechaInicio", fechaInicio.ToShortDateString()),
                new ReportParameter("fechaFin", fechaFin.ToShortDateString()),
                new ReportParameter("codigoSucursal", sucursal.getCodigo()),
                new ReportParameter("direccionSucursal", sucursal.getDireccion())
            });
            
            this.rpvEntidadFinanciera.RefreshReport();

            LogControl.logGenerarReporteEntidadFinanciera(usuario, entidadFinanciera);
        }

        private void FormReporteEntidadFinanciera_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.rpvEntidadFinanciera.Reset();
        }
    }
}
