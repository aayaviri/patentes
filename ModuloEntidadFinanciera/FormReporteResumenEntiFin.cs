﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Control;
using Modelo;
using Microsoft.Reporting.WinForms;

namespace ModuloEntidadFinanciera
{
    public partial class FormReporteResumenEntiFin : Form
    {
        private EntidadFinanciera entidadFinanciera;

        public FormReporteResumenEntiFin(EntidadFinanciera entidadFinanciera)
        {
            InitializeComponent();
            this.entidadFinanciera = entidadFinanciera;
        }

        private void FormReporteResumenEntiFin_Load(object sender, EventArgs e)
        {
        }

        private void loadReporteResumenEntidadFinanciera()
        {
            DateTime fechaInicio = dtpFechaInicio.Value;
            DateTime fechaFin = dtpFechaFin.Value.AddDays(1);

            DataTable pagosDataTable = ReporteControl.getResumenSucursalFechasIdEntidad(fechaInicio.Date, fechaFin.Date.AddDays(1), this.entidadFinanciera.getId());

            this.rpvResumenEntidad.LocalReport.DataSources.Clear();
            ReportDataSource rprtDTSource = new Microsoft.Reporting.WinForms.ReportDataSource("pagosDataTable", pagosDataTable);

            this.rpvResumenEntidad.LocalReport.DataSources.Add(rprtDTSource);

            rpvResumenEntidad.LocalReport.SetParameters(new ReportParameter[] {
                new ReportParameter("fechaInicio", fechaInicio.ToShortDateString()),
                new ReportParameter("fechaFin", fechaFin.ToShortDateString()),
                new ReportParameter("nombreEntidadFinanciera", entidadFinanciera.getNombre())
            });

            this.rpvResumenEntidad.RefreshReport();
        }

        private void btnVerReporte_Click(object sender, EventArgs e)
        {
            loadReporteResumenEntidadFinanciera();
        }

        private void FormReporteResumenEntiFin_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.rpvResumenEntidad.Reset();
        }
    }
}
