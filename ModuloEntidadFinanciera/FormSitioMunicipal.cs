﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Control;
using Modelo;

namespace ModuloAdministracion
{
    public partial class FormSitioMunicipal : Form
    {
        private SitioMunicipal sitioMunicipal;

        public FormSitioMunicipal(SitioMunicipal sitioMunicipal)
        {
            InitializeComponent();
            this.sitioMunicipal = sitioMunicipal;
            loadModelToForm();
        }

        private void loadModelToForm()
        {
            txtCodigoLicencia.Text = sitioMunicipal.getCodigoLicencia();
            txtDescripcion.Text = sitioMunicipal.getDescripcion();
            txtDireccion.Text = sitioMunicipal.getDireccion();
            txtRazonSocial.Text = sitioMunicipal.getRazonSocial();
            txtSuperficie.Text = sitioMunicipal.getSuperficie().ToString();
            txtTipo.Text = sitioMunicipal.getTipo();
            cbxEstado.SelectedItem = sitioMunicipal.getEstado();
            dtPickerFechaInicio.Value = sitioMunicipal.getFechaInicio();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
