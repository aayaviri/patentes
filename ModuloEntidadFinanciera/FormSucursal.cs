﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Modelo;
using Control;

namespace ModuloEntidadFinanciera
{
    public partial class FormSucursal : Form
    {
        private Sucursal sucursal;
        private EntidadFinanciera entiFin;
        private Usuario usuario;

        public FormSucursal(EntidadFinanciera entiFin, Usuario usuario)
        {
            InitializeComponent();
            this.entiFin = entiFin;
            this.usuario = usuario;
            this.sucursal = new Sucursal();
        }

        public FormSucursal(EntidadFinanciera entiFin, Sucursal sucursal, Usuario usuario)
        {
            InitializeComponent();
            this.entiFin = entiFin;
            this.sucursal = sucursal;
            this.usuario = usuario;
            loadModelToForm();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            RespuestaControl validacion = validarFormulario();

            if (validacion.getSuccess())
            {
                guardarFormulario();
            }
            else
            {
                lblErrorMessage.Text = validacion.getMessage();
                lblErrorMessage.Visible = true;
            }
        }

        private void loadModelToForm()
        {
            txtCodigo.Text = sucursal.getCodigo();
            txtDireccion.Text = sucursal.getDireccion();
        }

        private void loadFormToModel()
        {
            sucursal.setCodigo(txtCodigo.Text);
            sucursal.setDireccion(txtDireccion.Text);
            sucursal.setIdEntidadFinanciera(entiFin.getId());
        }
        private RespuestaControl validarFormulario()
        {
            RespuestaControl resp = new RespuestaControl();

            if (String.IsNullOrEmpty(txtCodigo.Text) || String.IsNullOrWhiteSpace(txtCodigo.Text))
            {
                resp.setSuccess(false);
                resp.setMessage(resp.getMessage() + "El codigo es requerido.\n");
            }
            if (String.IsNullOrEmpty(txtDireccion.Text) || String.IsNullOrWhiteSpace(txtDireccion.Text))
            {
                resp.setSuccess(false);
                resp.setMessage(resp.getMessage() + "La direccion es requerido.\n");
            }
            
            return resp;
        }
        private void guardarFormulario()
        {
            loadFormToModel();
            if (SucursalControl.guardarSucursal(this.sucursal))
            {
                this.DialogResult = DialogResult.OK;
                if (this.sucursal.getId() == 0)
                {
                    LogControl.logNuevaSucursal(usuario, sucursal);
                }
                else {
                    LogControl.logModificarSucursal(usuario, sucursal);
                }
                
                this.Close();
            }
            else
            {
                MessageBox.Show("Ocurrio un error al guardar la Sucursal");
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
