CREATE DATABASE  IF NOT EXISTS `cobranza_patentes` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `cobranza_patentes`;
-- MySQL dump 10.13  Distrib 5.5.16, for Win32 (x86)
--
-- Host: 127.0.0.1    Database: cobranza_patentes
-- ------------------------------------------------------
-- Server version	5.0.51b-community-nt-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Not dumping tablespaces as no INFORMATION_SCHEMA.FILES table on this server
--

--
-- Table structure for table `entidad_financiera`
--

DROP TABLE IF EXISTS `entidad_financiera`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entidad_financiera` (
  `id` int(11) NOT NULL auto_increment,
  `nit` varchar(20) default NULL,
  `nombre` varchar(100) default NULL,
  `estado` varchar(50) default NULL,
  `maxCajeros` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entidad_financiera`
--

LOCK TABLES `entidad_financiera` WRITE;
/*!40000 ALTER TABLE `entidad_financiera` DISABLE KEYS */;
INSERT INTO `entidad_financiera` VALUES (1,'1457859748','BANCO UNION','activo',25),(2,'2548759632','BANCO SOL','activo',20),(3,'3548547896','FONDO FINANCIERO PRIVADO FASSIL','activo',10),(4,'1548569712','BANCO DE CREDITO','activo',18);
/*!40000 ALTER TABLE `entidad_financiera` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `impuesto_patente`
--

DROP TABLE IF EXISTS `impuesto_patente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `impuesto_patente` (
  `id` int(11) NOT NULL auto_increment,
  `gestion` int(11) default NULL,
  `costo` decimal(8,6) default NULL,
  `superficie` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `impuesto_patente`
--

LOCK TABLES `impuesto_patente` WRITE;
/*!40000 ALTER TABLE `impuesto_patente` DISABLE KEYS */;
/*!40000 ALTER TABLE `impuesto_patente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `id` int(11) NOT NULL auto_increment,
  `login` varchar(45) default NULL,
  `password` varchar(100) default NULL,
  `rol` varchar(50) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (1,'F.ARANDIA','123','admin_entifin'),(2,'N.LOPEZ','456','cajero'),(3,'R.GUTIERREZ','789','admin_entifin'),(4,'A.GUZMAN','132','cajero'),(5,'L.AGUILAR','967','admin_entifin'),(6,'G.GOMEZ','414','cajero'),(7,'R.RUIZ','852','admin_entifin'),(8,'K ARTEAGA','794','cajero'),(9,'admin','admin','administrador');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cajero`
--

DROP TABLE IF EXISTS `cajero`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cajero` (
  `id` int(11) NOT NULL auto_increment,
  `idSucursal` int(11) default NULL,
  `codigo` varchar(50) default NULL,
  `nombreCompleto` varchar(50) default NULL,
  `estado` varchar(50) default NULL,
  `idUsuario` int(11) default NULL,
  `dosificacion` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `idSucursal` (`idSucursal`),
  KEY `idUsuario` (`idUsuario`),
  CONSTRAINT `FK_cajero_sucursal` FOREIGN KEY (`idSucursal`) REFERENCES `sucursal` (`id`),
  CONSTRAINT `FK_cajero_usuario` FOREIGN KEY (`idUsuario`) REFERENCES `usuario` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cajero`
--

LOCK TABLES `cajero` WRITE;
/*!40000 ALTER TABLE `cajero` DISABLE KEYS */;
INSERT INTO `cajero` VALUES (1,1,'741','FRANCISCO ARANDIA PEREZ','activo',1,NULL),(2,1,'742','NOELIA LOPEZ ARANDIA','activo',2,NULL),(3,2,'641','RODRIGO GUTIERREZ CHAVEZ','activo',3,NULL),(4,2,'642','ARIEL GUZMAN SANGUINO','activo',4,NULL),(5,3,'541','LUISA AGUILAR BOZA','activo',5,NULL),(6,3,'542','GUILLERMO GOMEZ RODRIGUEZ','activo',6,NULL),(7,4,'441','RUBEN RUIZ NAVARRO','activo',7,NULL),(8,4,'442','KATHERINE ARTEAGA MONTAÑO','activo',8,NULL);
/*!40000 ALTER TABLE `cajero` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sucursal`
--

DROP TABLE IF EXISTS `sucursal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sucursal` (
  `id` int(11) NOT NULL auto_increment,
  `codigo` varchar(50) default NULL,
  `direccion` varchar(200) default NULL,
  `idEntidadFinanciera` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `idEntidadFinanciera` (`idEntidadFinanciera`),
  CONSTRAINT `FK_sucursal_entidad_financiera` FOREIGN KEY (`idEntidadFinanciera`) REFERENCES `entidad_financiera` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sucursal`
--

LOCK TABLES `sucursal` WRITE;
/*!40000 ALTER TABLE `sucursal` DISABLE KEYS */;
INSERT INTO `sucursal` VALUES (1,'7854','CALLE JORDAN NRO 272 ENTRE NATANIEL AGUIRRE Y ESTEBAN ARCE ',1),(2,'7855','CALLE SUCRE  NRO 336 ENTRE 25 DE MAYO Y ESTEBAN ARCE',1),(3,'7856','AV. AYACUCHO NRO 1665 ENTRE ARANI Y CLIZA',1),(4,'6987','CALLE ESTEBAN ARCE NRO 631 ENTRE LADISLAO CABRERA Y URUGUAY',2),(5,'6988','AV. SUECIA NRO 3013 FRENTE MERCADO 15 DE ABRIL',2),(6,'5897','CALLE NATANIEL AGUIRRE NRO 508 ENTRE CALAMA Y LADISLAO CABRERA',3),(7,'5898','CALLE SAN MARTIN NRO 458 ENTRE CALAMA Y LADISLAO CABRERA',3),(8,'5899','AV HEROINAS NRO 578 ENTRE AYACUCHO Y JUNIN',3),(9,'4860','AV. BALLIVIAN ESQUINA MEXICO',4),(10,'4861','AV. HEROINAS NRO 664 ESQUINA CALLE FALSURI',4),(11,'4862','AV. HEROINAS NRO 435 ENTRE CALAMA SAN MARTIN Y 25 DE MAYO',4);
/*!40000 ALTER TABLE `sucursal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contribuyente`
--

DROP TABLE IF EXISTS `contribuyente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contribuyente` (
  `id` int(11) NOT NULL auto_increment,
  `nombre` varchar(50) default NULL,
  `apellidoPaterno` varchar(50) default NULL,
  `apellidoMaterno` varchar(50) default NULL,
  `ci` varchar(50) default NULL,
  `direccion` varchar(200) default NULL,
  `password` varchar(50) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contribuyente`
--

LOCK TABLES `contribuyente` WRITE;
/*!40000 ALTER TABLE `contribuyente` DISABLE KEYS */;
INSERT INTO `contribuyente` VALUES (1,'CARLOS ','AGUIRRE','PEREZ','123456','AV. PANAMERICANA','123'),(2,'ALICIA','MARZA','APAZA','2541535','LADISLAO CABRERA Y HUAYNA KAPAC','456'),(3,'GRACIELA ','ROMERO','PAZ','4256957','AV. HEROINAS ENTRE AVAROA Y GRAL ACHA','789'),(4,'PEPE','ARISPE','VIZCARRA','2475864','AV, 6 DE AGOSTO ENTRE 22 DE JULIO Y TUCUMAN','321'),(5,'MERCEDEZ ','MAMANI','TORREZ','1254856','SUIPACHA Y MENDEZ ARCOS','759'),(6,'JUAN ','ILLANES','BOZA','3215425','AV. G URQUIDI Y JOSE ARMANDO MENDEZ','654'),(7,'KEVIN','UGARTE','ROSAS','5474585','TARAPACA COLOMBIA Y ECUADOR','789'),(8,'JULIO','TORRICO','FLORES','2451248','PULACAYO Y AV. REPUBLICA','159'),(9,'TERESA ','OTONDO','ZURITA','1425478','PACATA ALTA CALLE 8 NRO 47','487'),(10,'ELENA ','CHAMBI','LOZADA','3652145','AV. BLANCO GALINDO KM 5','965');
/*!40000 ALTER TABLE `contribuyente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gestion`
--

DROP TABLE IF EXISTS `gestion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gestion` (
  `id` int(11) NOT NULL auto_increment,
  `fechaUfv` date default NULL,
  `ufv` double default NULL,
  `gestion` int(11) default NULL,
  `interesUfv` double default NULL,
  `multaIDF` double default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gestion`
--

LOCK TABLES `gestion` WRITE;
/*!40000 ALTER TABLE `gestion` DISABLE KEYS */;
INSERT INTO `gestion` VALUES (1,'1996-12-31',1.75485,1996,60,50),(2,'1997-12-31',1.75845,1997,58,50),(3,'1998-12-31',1.84521,1998,55,50),(4,'1999-12-31',1.61542,1999,54,50),(5,'2000-12-31',1.45785,2000,41,50),(6,'2001-12-31',1.45264,2001,33,50),(7,'2002-12-31',1.74896,2002,32,50),(8,'2003-12-31',1.86954,2003,29,50),(9,'2004-12-31',1.96546,2004,28,50),(10,'2005-12-31',1.75486,2005,20,50),(11,'2006-12-31',1.42687,2006,19,50),(12,'2007-12-31',1.86541,2007,15,50),(13,'2008-12-31',1.42547,2008,10,50),(14,'2009-12-31',1.56457,2009,5,50),(15,'2010-12-31',1.42631,2010,0,50),(16,'2011-12-31',1.87694,2011,0,50),(17,'2012-12-31',1.75486,2012,0,50),(18,'2013-08-07',1.445578,2013,0,0);
/*!40000 ALTER TABLE `gestion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pago`
--

DROP TABLE IF EXISTS `pago`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pago` (
  `id` int(11) NOT NULL auto_increment,
  `idSitioMunicipal` int(11) default NULL,
  `idCajero` int(11) default NULL,
  `estado` varchar(45) default NULL,
  `fechaPago` date default NULL,
  `ufvPago` double default NULL,
  `nroComprobante` int(11) default NULL,
  `gestion` int(11) default NULL,
  `impuestoDeterminadoBs` double default NULL,
  `ufvVenc` double default NULL,
  `impuestoDeterminadoUfv` double default NULL,
  `descuentoUfv` double default NULL,
  `interesUfv` double default NULL,
  `multaIDF` double default NULL,
  `descuentoAplicado` double default NULL,
  `deudaTributariaUfv` double default NULL,
  `total` double default NULL,
  PRIMARY KEY  (`id`),
  KEY `idCajero` (`idCajero`),
  KEY `idSitioMunicipal` (`idSitioMunicipal`),
  CONSTRAINT `FK_pago_cajero` FOREIGN KEY (`idCajero`) REFERENCES `cajero` (`id`),
  CONSTRAINT `FK_pago_sitio_municipal` FOREIGN KEY (`idSitioMunicipal`) REFERENCES `sitio_municipal` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pago`
--

LOCK TABLES `pago` WRITE;
/*!40000 ALTER TABLE `pago` DISABLE KEYS */;
INSERT INTO `pago` VALUES (1,1,2,'pagado','2013-07-30',1.675881,0,2005,45,0,45,0,20,50,0,115,192.73),(2,1,NULL,'pendiente',NULL,NULL,NULL,2006,45,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(3,1,NULL,'pendiente',NULL,NULL,NULL,2007,45,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(4,1,NULL,'pendiente',NULL,NULL,NULL,2010,55,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(5,2,NULL,'pendiente',NULL,NULL,NULL,2009,45,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(6,2,NULL,'pendiente',NULL,NULL,NULL,2010,55,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(7,2,NULL,'pendiente',NULL,NULL,NULL,2012,54,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(8,3,NULL,'pendiente',NULL,NULL,NULL,1998,40,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(9,3,NULL,'pendiente',NULL,NULL,NULL,2000,50,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(10,3,NULL,'pendiente',NULL,NULL,NULL,2002,50,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(11,3,NULL,'pendiente',NULL,NULL,NULL,2004,50,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(12,3,NULL,'pendiente',NULL,NULL,NULL,2005,50,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(13,4,NULL,'pendiente',NULL,NULL,NULL,2000,50,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(14,4,NULL,'pendiente',NULL,NULL,NULL,2001,50,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(15,4,NULL,'pendiente',NULL,NULL,NULL,2002,50,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(16,4,NULL,'pendiente',NULL,NULL,NULL,2004,50,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(17,4,NULL,'pendiente',NULL,NULL,NULL,2005,50,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(18,4,NULL,'pendiente',NULL,NULL,NULL,2008,53,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(19,4,NULL,'pendiente',NULL,NULL,NULL,2012,56,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(20,5,NULL,'pendiente',NULL,NULL,NULL,2003,54,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(21,5,NULL,'pendiente',NULL,NULL,NULL,2004,54,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(22,5,NULL,'pendiente',NULL,NULL,NULL,2005,53,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(23,6,NULL,'pendiente',NULL,NULL,NULL,1999,48,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(24,6,NULL,'pendiente',NULL,NULL,NULL,2001,51,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(25,6,NULL,'pendiente',NULL,NULL,NULL,2005,54,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(26,6,NULL,'pendiente',NULL,NULL,NULL,2006,53,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(27,6,NULL,'pendiente',NULL,NULL,NULL,2012,55,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(28,7,NULL,'pendiente',NULL,NULL,NULL,2001,50,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(29,7,NULL,'pendiente',NULL,NULL,NULL,2003,53,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(30,7,NULL,'pendiente',NULL,NULL,NULL,2004,54,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(31,8,NULL,'pendiente',NULL,NULL,NULL,1999,49,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(32,8,NULL,'pendiente',NULL,NULL,NULL,2000,50,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(33,8,NULL,'pendiente',NULL,NULL,NULL,2001,50,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(34,8,NULL,'pendiente',NULL,NULL,NULL,2002,51,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(35,8,NULL,'pendiente',NULL,NULL,NULL,2003,53,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(36,9,NULL,'pendiente',NULL,NULL,NULL,2010,55,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(37,9,NULL,'pendiente',NULL,NULL,NULL,2011,55,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(38,10,NULL,'pendiente',NULL,NULL,NULL,2008,54,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(39,10,NULL,'pendiente',NULL,NULL,NULL,2009,44,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(40,10,NULL,'pendiente',NULL,NULL,NULL,2010,59,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(41,10,NULL,'pendiente',NULL,NULL,NULL,2011,59,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(42,11,NULL,'pendiente',NULL,NULL,NULL,2005,50,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(43,11,NULL,'pendiente',NULL,NULL,NULL,2006,54,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(44,12,NULL,'pendiente',NULL,NULL,NULL,2008,55,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(45,12,NULL,'pendiente',NULL,NULL,NULL,2010,60,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(46,12,NULL,'pendiente',NULL,NULL,NULL,2011,61,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(47,12,NULL,'pendiente',NULL,NULL,NULL,2013,62,NULL,NULL,0,NULL,NULL,0,NULL,NULL);
/*!40000 ALTER TABLE `pago` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sitio_municipal`
--

DROP TABLE IF EXISTS `sitio_municipal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sitio_municipal` (
  `id` int(11) NOT NULL auto_increment,
  `codigoLicencia` varchar(45) default NULL,
  `descripcion` varchar(255) default NULL,
  `direccion` varchar(100) default NULL,
  `estado` varchar(45) default NULL,
  `fechaInicio` date default NULL,
  `razonSocial` varchar(100) default NULL,
  `superficie` double default NULL,
  `tipo` varchar(45) default NULL,
  `idContribuyente` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `idContribuyente` (`idContribuyente`),
  CONSTRAINT `FK_sitio_municipal_contribuyente` FOREIGN KEY (`idContribuyente`) REFERENCES `contribuyente` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sitio_municipal`
--

LOCK TABLES `sitio_municipal` WRITE;
/*!40000 ALTER TABLE `sitio_municipal` DISABLE KEYS */;
INSERT INTO `sitio_municipal` VALUES (1,'14879652','VENTA DE ROPA','PUNATA ENTRE 25 DE MAYO Y ESTEBAN ARCE AC. NORTE','activo','2005-02-14','CARLOS AGUIRRE',2,'CASETA',1),(2,'15548962','VENTA DE CALZADOS','CALAMA ENTRE LANZA Y LADISLAO CABRERA AC. SUD','activo','2008-06-24','IMAGEN Y ESTILOS',3,'PUESTO',2),(3,'13425478','VENTA DE PLANTAS','AV. AROMA ENTRE SAN MARTIN Y URUGUAY AC. ESTE','activo','1998-03-11','GRACIELA ROMERO',1,'CASETA',3),(4,'14452365','VENTA DE POLLOS','AV. BARRIENTOS ENTRE PUNATA Y TARATA AC, OESTE','activo','2000-04-07','PEPE ARISPE',1,'PUESTO',4),(5,'14485692','VENTA DE POLLOS ','AV. BARRIENTOS ENTRE PUNATA Y TARATA AC.OESTE','activo','2002-07-15','PEPE VIZCARRA',1,'PUESTO',4),(6,'11524698','VENTA DE FIAMBRES','AV. BARRIENTOS ENTRE LANZA Y PUNATA AC. NORTE','activo','1999-08-09','MERCEDEZ MAMANI',2,'PUESTO',5),(7,'15474585','VENTA DE FIAMBRES','AV. BARRIENTOS ENTRE LANZA Y PUNATA AC. NORTE','activo','2001-04-22','MERCEDEZ TORREZ',1,'PUESTO',5),(8,'14257487','VENTA DE INSUMOS Y BEBIDAS','25 DE MAYO Y PUNATA AC. SUD','activo','2000-02-21','RON ABUELO ALVAREZ',1,'CASETA',6),(9,'15242168','VENTA DE INSUMOS Y BEBIDAS','25 DE MAYO Y PUNATA AC. SUD','activo','2006-09-17','KEVIN UGARTE',2,'CASETA ',7),(10,'14253647','VENTA DE COMIDA','PUNATA ENTRE ESTEBAN ARCE AC. OESTE','activo','1996-07-14','JULIO TORRICO',1,'PUESTO',8),(11,'14856985','VENTA DE COMIDA','ESTEBAN ARCE Y PUNATA AC. ESTE','activo','1998-05-18','TERESA OTONDO',1,'PUESTO',9),(12,'14758459','VENTA DE FRUTAS','PUNATA ENTRE NATANIEL AGUIRRE Y ESTEBAN ARCE AC. SUD','activo','2007-07-28','ELENA GALINDO',1,'PUESTO',10);
/*!40000 ALTER TABLE `sitio_municipal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'cobranza_patentes'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-08-07 22:15:34
