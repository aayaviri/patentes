CREATE DATABASE  IF NOT EXISTS `cobranza_patentes` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `cobranza_patentes`;
-- MySQL dump 10.13  Distrib 5.5.16, for Win32 (x86)
--
-- Host: 127.0.0.1    Database: cobranza_patentes
-- ------------------------------------------------------
-- Server version	5.0.51b-community-nt-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Not dumping tablespaces as no INFORMATION_SCHEMA.FILES table on this server
--

--
-- Table structure for table `entidad_financiera`
--

DROP TABLE IF EXISTS `entidad_financiera`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entidad_financiera` (
  `id` int(11) NOT NULL auto_increment,
  `nit` varchar(20) default NULL,
  `nombre` varchar(100) default NULL,
  `estado` varchar(50) default NULL,
  `maxCajeros` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entidad_financiera`
--

LOCK TABLES `entidad_financiera` WRITE;
/*!40000 ALTER TABLE `entidad_financiera` DISABLE KEYS */;
INSERT INTO `entidad_financiera` VALUES (1,'1457859748','BANCO UNION','activo',25),(2,'2548759632','BANCO SOL','activo',20),(3,'3548547896','FONDO FINANCIERO PRIVADO FASSIL','activo',10),(4,'1548569712','BANCO DE CREDITO','activo',18);
/*!40000 ALTER TABLE `entidad_financiera` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_entidad_financiera`
--

DROP TABLE IF EXISTS `admin_entidad_financiera`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_entidad_financiera` (
  `id` int(11) NOT NULL auto_increment,
  `nombreCompleto` varchar(50) default NULL,
  `idUsuario` int(11) default NULL,
  `idEntidadFinanciera` int(11) default NULL,
  `estado` varchar(50) default NULL,
  PRIMARY KEY  (`id`),
  KEY `idEntidadFinanciera` (`idEntidadFinanciera`),
  KEY `idUsuario` (`idUsuario`),
  CONSTRAINT `FK_admin_entidad_financiera_entidad_financiera` FOREIGN KEY (`idEntidadFinanciera`) REFERENCES `entidad_financiera` (`id`),
  CONSTRAINT `FK_admin_entidad_financiera_usuario` FOREIGN KEY (`idUsuario`) REFERENCES `usuario` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_entidad_financiera`
--

LOCK TABLES `admin_entidad_financiera` WRITE;
/*!40000 ALTER TABLE `admin_entidad_financiera` DISABLE KEYS */;
INSERT INTO `admin_entidad_financiera` VALUES (1,'Fernando Arandia',1,1,'activo'),(2,'Admin Banco Sol',11,2,'activo');
/*!40000 ALTER TABLE `admin_entidad_financiera` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gestion`
--

DROP TABLE IF EXISTS `gestion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gestion` (
  `id` int(11) NOT NULL auto_increment,
  `fechaUfv` date default NULL,
  `ufv` double default NULL,
  `gestion` int(11) default NULL,
  `interesUfv` double default NULL,
  `multaIDF` double default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gestion`
--

LOCK TABLES `gestion` WRITE;
/*!40000 ALTER TABLE `gestion` DISABLE KEYS */;
INSERT INTO `gestion` VALUES (1,'1996-12-31',1.75485,1996,60,50),(2,'1997-12-31',1.75845,1997,58,50),(3,'1998-12-31',1.84521,1998,55,50),(4,'1999-12-31',1.61542,1999,54,50),(5,'2000-12-31',1.45785,2000,41,50),(6,'2001-12-31',1.45264,2001,33,50),(7,'2002-12-31',1.74896,2002,32,50),(8,'2003-12-31',1.86954,2003,29,50),(9,'2004-12-31',1.96546,2004,28,50),(10,'2005-12-31',1.75486,2005,20,50),(11,'2006-12-31',1.42687,2006,19,50),(12,'2007-12-31',1.86541,2007,15,50),(13,'2008-12-31',1.42547,2008,10,50),(14,'2009-12-31',1.56457,2009,5,50),(15,'2010-12-31',1.42631,2010,0,50),(16,'2011-12-31',1.87694,2011,0,50),(17,'2012-12-31',1.75486,2012,0,50),(18,'2013-08-30',1.65845,2013,0,0);
/*!40000 ALTER TABLE `gestion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `id` int(11) NOT NULL auto_increment,
  `login` varchar(45) default NULL,
  `password` varchar(100) default NULL,
  `rol` varchar(50) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (1,'F.ARANDIA','cb676c05a8242c126ee8f32ae17edc17','admin_entifin'),(2,'N.LOPEZ','202cb962ac59075b964b07152d234b70','cajero'),(3,'R.GUTIERREZ','202cb962ac59075b964b07152d234b70','admin_entifin'),(5,'L.AGUILAR','202cb962ac59075b964b07152d234b70','admin_entifin'),(6,'G.GOMEZ','202cb962ac59075b964b07152d234b70','cajero'),(7,'R.RUIZ','202cb962ac59075b964b07152d234b70','admin_entifin'),(8,'K ARTEAGA','202cb962ac59075b964b07152d234b70','cajero'),(9,'admin','21232f297a57a5a743894a0e4a801fc3','administrador'),(10,'J.LOPEZ','202cb962ac59075b964b07152d234b70','cajero'),(11,'A.BANCO.SOL','202cb962ac59075b964b07152d234b70','admin_entifin'),(12,'A.GUZMAN','202cb962ac59075b964b07152d234b70','cajero');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cajero`
--

DROP TABLE IF EXISTS `cajero`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cajero` (
  `id` int(11) NOT NULL auto_increment,
  `idSucursal` int(11) default NULL,
  `codigo` varchar(50) default NULL,
  `nombreCompleto` varchar(50) default NULL,
  `estado` varchar(50) default NULL,
  `idUsuario` int(11) default NULL,
  `dosificacion` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `idSucursal` (`idSucursal`),
  KEY `idUsuario` (`idUsuario`),
  CONSTRAINT `FK_cajero_sucursal` FOREIGN KEY (`idSucursal`) REFERENCES `sucursal` (`id`),
  CONSTRAINT `FK_cajero_usuario` FOREIGN KEY (`idUsuario`) REFERENCES `usuario` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cajero`
--

LOCK TABLES `cajero` WRITE;
/*!40000 ALTER TABLE `cajero` DISABLE KEYS */;
INSERT INTO `cajero` VALUES (2,1,'742','NOELIA LOPEZ ARANDIA','activo',2,702),(3,2,'641','RODRIGO GUTIERREZ CHAVEZ','activo',3,NULL),(5,3,'541','LUISA AGUILAR BOZA','activo',5,NULL),(6,3,'542','GUILLERMO GOMEZ RODRIGUEZ','activo',6,NULL),(7,4,'441','RUBEN RUIZ NAVARRO','activo',7,NULL),(8,4,'442','KATHERINE ARTEAGA MONTAÑO','activo',8,NULL),(9,1,'672','ARIEL GUZMAN','activo',12,0);
/*!40000 ALTER TABLE `cajero` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sucursal`
--

DROP TABLE IF EXISTS `sucursal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sucursal` (
  `id` int(11) NOT NULL auto_increment,
  `codigo` varchar(50) default NULL,
  `direccion` varchar(200) default NULL,
  `idEntidadFinanciera` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `idEntidadFinanciera` (`idEntidadFinanciera`),
  CONSTRAINT `FK_sucursal_entidad_financiera` FOREIGN KEY (`idEntidadFinanciera`) REFERENCES `entidad_financiera` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sucursal`
--

LOCK TABLES `sucursal` WRITE;
/*!40000 ALTER TABLE `sucursal` DISABLE KEYS */;
INSERT INTO `sucursal` VALUES (1,'7854','CALLE JORDAN NRO 272 ENTRE NATANIEL AGUIRRE Y ESTEBAN ARCE ',1),(2,'7855','CALLE SUCRE  NRO 336 ENTRE 25 DE MAYO Y ESTEBAN ARCE',1),(3,'7856','AV. AYACUCHO NRO 1665 ENTRE ARANI Y CLIZA',1),(4,'6987','CALLE ESTEBAN ARCE NRO 631 ENTRE LADISLAO CABRERA Y URUGUAY',2),(5,'6988','AV. SUECIA NRO 3013 FRENTE MERCADO 15 DE ABRIL',2),(6,'5897','CALLE NATANIEL AGUIRRE NRO 508 ENTRE CALAMA Y LADISLAO CABRERA',3),(7,'5898','CALLE SAN MARTIN NRO 458 ENTRE CALAMA Y LADISLAO CABRERA',3),(8,'5899','AV HEROINAS NRO 578 ENTRE AYACUCHO Y JUNIN',3),(9,'4860','AV. BALLIVIAN ESQUINA MEXICO',4),(10,'4861','AV. HEROINAS NRO 664 ESQUINA CALLE FALSURI',4),(11,'4862','AV. HEROINAS NRO 435 ENTRE CALAMA SAN MARTIN Y 25 DE MAYO',4);
/*!40000 ALTER TABLE `sucursal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contribuyente`
--

DROP TABLE IF EXISTS `contribuyente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contribuyente` (
  `id` int(11) NOT NULL auto_increment,
  `nombre` varchar(50) default NULL,
  `apellidoPaterno` varchar(50) default NULL,
  `apellidoMaterno` varchar(50) default NULL,
  `ci` varchar(50) default NULL,
  `direccion` varchar(200) default NULL,
  `password` varchar(50) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `ci_UNIQUE` (`ci`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contribuyente`
--

LOCK TABLES `contribuyente` WRITE;
/*!40000 ALTER TABLE `contribuyente` DISABLE KEYS */;
INSERT INTO `contribuyente` VALUES (1,'CARLOS ','AGUIRRE','PEREZ','123456','AV. PANAMERICANA','202cb962ac59075b964b07152d234b70'),(2,'ALICIA','MARZA','APAZA','2541535','LADISLAO CABRERA Y HUAYNA KAPAC','4c97d0b72f38d0712a34a7666563c57d'),(3,'GRACIELA ','ROMERO','PAZ','4256957','AV. HEROINAS ENTRE AVAROA Y GRAL ACHA','789'),(4,'PEPE','ARISPE','VIZCARRA','2475864','AV, 6 DE AGOSTO ENTRE 22 DE JULIO Y TUCUMAN','321'),(5,'MERCEDEZ ','MAMANI','TORREZ','1254856','SUIPACHA Y MENDEZ ARCOS','759'),(6,'JUAN ','ILLANES','BOZA','3215425','AV. G URQUIDI Y JOSE ARMANDO MENDEZ','654'),(7,'KEVIN','UGARTE','ROSAS','5474585','TARAPACA COLOMBIA Y ECUADOR','789'),(8,'JULIO','TORRICO','FLORES','2451248','PULACAYO Y AV. REPUBLICA','159'),(9,'TERESA ','OTONDO','ZURITA','1425478','PACATA ALTA CALLE 8 NRO 47','487'),(10,'ELENA ','CHAMBI','LOZADA','3652145','AV. BLANCO GALINDO KM 5','965');
/*!40000 ALTER TABLE `contribuyente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comprobantes_anulados`
--

DROP TABLE IF EXISTS `comprobantes_anulados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comprobantes_anulados` (
  `id` int(11) NOT NULL auto_increment,
  `nroComprobante` int(11) default NULL,
  `fechaAnulacion` datetime default NULL,
  `motivo` varchar(500) default NULL,
  `idCajero` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_comprobantes_anulados_cajero` (`idCajero`),
  CONSTRAINT `FK_comprobantes_anulados_cajero` FOREIGN KEY (`idCajero`) REFERENCES `cajero` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comprobantes_anulados`
--

LOCK TABLES `comprobantes_anulados` WRITE;
/*!40000 ALTER TABLE `comprobantes_anulados` DISABLE KEYS */;
INSERT INTO `comprobantes_anulados` VALUES (3,123,'2013-08-17 10:22:35','Error en el pago',2),(4,566,'2013-08-18 19:52:32','PRUEBA',2);
/*!40000 ALTER TABLE `comprobantes_anulados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `impuesto_patente`
--

DROP TABLE IF EXISTS `impuesto_patente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `impuesto_patente` (
  `id` int(11) NOT NULL auto_increment,
  `gestion` int(11) default NULL,
  `costo` decimal(8,6) default NULL,
  `superficie` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `impuesto_patente`
--

LOCK TABLES `impuesto_patente` WRITE;
/*!40000 ALTER TABLE `impuesto_patente` DISABLE KEYS */;
/*!40000 ALTER TABLE `impuesto_patente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pago`
--

DROP TABLE IF EXISTS `pago`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pago` (
  `id` int(11) NOT NULL auto_increment,
  `idSitioMunicipal` int(11) default NULL,
  `idCajero` int(11) default NULL,
  `estado` varchar(45) default NULL,
  `fechaPago` datetime default NULL,
  `ufvPago` double default NULL,
  `nroComprobante` int(11) default NULL,
  `gestion` int(11) default NULL,
  `impuestoDeterminadoBs` double default NULL,
  `ufvVenc` double default NULL,
  `impuestoDeterminadoUfv` double default NULL,
  `descuentoUfv` double default NULL,
  `interesUfv` double default NULL,
  `multaIDF` double default NULL,
  `descuentoAplicado` double default NULL,
  `deudaTributariaUfv` double default NULL,
  `total` double default NULL,
  PRIMARY KEY  (`id`),
  KEY `idCajero` (`idCajero`),
  KEY `idSitioMunicipal` (`idSitioMunicipal`),
  CONSTRAINT `FK_pago_cajero` FOREIGN KEY (`idCajero`) REFERENCES `cajero` (`id`),
  CONSTRAINT `FK_pago_sitio_municipal` FOREIGN KEY (`idSitioMunicipal`) REFERENCES `sitio_municipal` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pago`
--

LOCK TABLES `pago` WRITE;
/*!40000 ALTER TABLE `pago` DISABLE KEYS */;
INSERT INTO `pago` VALUES (1,1,2,'pendiente','2013-08-17 10:24:26',1.88,566,2005,45,0,45,0,20,50,0,115,216.2),(2,1,2,'pagado','2013-08-10 00:00:00',1.4556,560,2006,45,0,45,0,19,50,0,114,165.94),(3,1,2,'pagado','2013-08-10 00:00:00',1.4556,560,2007,45,0,45,0,15,50,0,110,160.12),(4,1,2,'pagado','2013-08-10 00:00:00',1.4556,561,2010,55,0,55,0,0,50,0,105,152.84),(5,2,2,'pagado','2013-08-11 14:00:00',1.14566,562,2009,45,0,45,0,5,50,0,100,114.57),(6,2,2,'pagado','2013-08-13 00:47:23',1.42256,563,2010,55,0,55,0,0,50,0,105,149.37),(7,2,2,'pagado','2013-08-13 00:47:23',1.42256,563,2012,54,0,54,0,0,50,0,104,147.95),(8,3,2,'pagado','2013-08-13 00:54:19',1.42256,564,1998,40,0,40,0,55,50,0,145,206.27),(9,3,2,'pagado','2013-08-13 00:54:19',1.42256,564,2000,50,0,50,0,41,50,0,141,200.58),(10,3,2,'pagado','2013-08-19 01:04:29',1.58994,700,2002,50,1.74896,28.59,0,32,50,0,110.59,175.83),(11,3,2,'pagado','2013-08-19 01:04:44',1.58994,701,2004,50,1.96546,25.44,0,28,50,0,103.44,164.46),(12,3,2,'pagado','2013-08-19 01:04:44',1.58994,701,2005,50,1.75486,28.49,0,20,50,0,98.49,156.59),(13,4,NULL,'pendiente',NULL,NULL,NULL,2000,50,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(14,4,NULL,'pendiente',NULL,NULL,NULL,2001,50,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(15,4,NULL,'pendiente',NULL,NULL,NULL,2002,50,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(16,4,NULL,'pendiente',NULL,NULL,NULL,2004,50,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(17,4,NULL,'pendiente',NULL,NULL,NULL,2005,50,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(18,4,NULL,'pendiente',NULL,NULL,NULL,2008,53,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(19,4,NULL,'pendiente',NULL,NULL,NULL,2012,56,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(20,5,NULL,'pendiente',NULL,NULL,NULL,2003,54,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(21,5,NULL,'pendiente',NULL,NULL,NULL,2004,54,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(22,5,NULL,'pendiente',NULL,NULL,NULL,2005,53,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(23,6,NULL,'pendiente',NULL,NULL,NULL,1999,48,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(24,6,NULL,'pendiente',NULL,NULL,NULL,2001,51,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(25,6,NULL,'pendiente',NULL,NULL,NULL,2005,54,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(26,6,NULL,'pendiente',NULL,NULL,NULL,2006,53,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(27,6,NULL,'pendiente',NULL,NULL,NULL,2012,55,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(28,7,NULL,'pendiente',NULL,NULL,NULL,2001,50,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(29,7,NULL,'pendiente',NULL,NULL,NULL,2003,53,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(30,7,NULL,'pendiente',NULL,NULL,NULL,2004,54,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(31,8,2,'pagado','2013-08-18 19:57:00',1.88889,567,1999,49,1.61542,30.33,0,54,50,0,134.33,253.73),(32,8,2,'pagado','2013-08-18 20:03:57',1.88889,568,2000,50,1.45785,34.3,0,41,50,0,125.3,236.68),(33,8,NULL,'pendiente',NULL,NULL,NULL,2001,50,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(34,8,NULL,'pendiente',NULL,NULL,NULL,2002,51,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(35,8,NULL,'pendiente',NULL,NULL,NULL,2003,53,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(36,9,NULL,'pendiente',NULL,NULL,NULL,2010,55,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(37,9,NULL,'pendiente',NULL,NULL,NULL,2011,55,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(38,10,NULL,'pendiente',NULL,NULL,NULL,2008,54,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(39,10,NULL,'pendiente',NULL,NULL,NULL,2009,44,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(40,10,NULL,'pendiente',NULL,NULL,NULL,2010,59,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(41,10,NULL,'pendiente',NULL,NULL,NULL,2011,59,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(42,11,NULL,'pendiente',NULL,NULL,NULL,2005,50,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(43,11,NULL,'pendiente',NULL,NULL,NULL,2006,54,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(44,12,NULL,'pendiente',NULL,NULL,NULL,2008,55,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(45,12,NULL,'pendiente',NULL,NULL,NULL,2010,60,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(46,12,NULL,'pendiente',NULL,NULL,NULL,2011,61,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(47,12,NULL,'pendiente',NULL,NULL,NULL,2013,62,NULL,NULL,0,NULL,NULL,0,NULL,NULL);
/*!40000 ALTER TABLE `pago` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sitio_municipal`
--

DROP TABLE IF EXISTS `sitio_municipal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sitio_municipal` (
  `id` int(11) NOT NULL auto_increment,
  `codigoLicencia` varchar(45) default NULL,
  `descripcion` varchar(255) default NULL,
  `direccion` varchar(100) default NULL,
  `estado` varchar(45) default NULL,
  `fechaInicio` date default NULL,
  `razonSocial` varchar(100) default NULL,
  `superficie` double default NULL,
  `tipo` varchar(45) default NULL,
  `idContribuyente` int(11) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `codigoLicencia_UNIQUE` (`codigoLicencia`),
  KEY `idContribuyente` (`idContribuyente`),
  CONSTRAINT `FK_sitio_municipal_contribuyente` FOREIGN KEY (`idContribuyente`) REFERENCES `contribuyente` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sitio_municipal`
--

LOCK TABLES `sitio_municipal` WRITE;
/*!40000 ALTER TABLE `sitio_municipal` DISABLE KEYS */;
INSERT INTO `sitio_municipal` VALUES (1,'14879652','VENTA DE ROPA','PUNATA ENTRE 25 DE MAYO Y ESTEBAN ARCE AC. NORTE','activo','2005-02-14','CARLOS AGUIRRE',2,'CASETA',2),(2,'15548962','VENTA DE CALZADOS','CALAMA ENTRE LANZA Y LADISLAO CABRERA AC. SUD','activo','2008-06-24','IMAGEN Y ESTILOS',3,'PUESTO',2),(3,'13425478','VENTA DE PLANTAS','AV. AROMA ENTRE SAN MARTIN Y URUGUAY AC. ESTE','activo','1998-03-11','GRACIELA ROMERO',1,'CASETA',3),(4,'14452365','VENTA DE POLLOS','AV. BARRIENTOS ENTRE PUNATA Y TARATA AC, OESTE','activo','2000-04-07','PEPE ARISPE',1,'PUESTO',4),(5,'14485692','VENTA DE POLLOS ','AV. BARRIENTOS ENTRE PUNATA Y TARATA AC.OESTE','activo','2002-07-15','PEPE VIZCARRA',1,'PUESTO',4),(6,'11524698','VENTA DE FIAMBRES','AV. BARRIENTOS ENTRE LANZA Y PUNATA AC. NORTE','activo','1999-08-09','MERCEDEZ MAMANI',2,'PUESTO',5),(7,'15474585','VENTA DE FIAMBRES','AV. BARRIENTOS ENTRE LANZA Y PUNATA AC. NORTE','activo','2001-04-22','MERCEDEZ TORREZ',1,'PUESTO',5),(8,'14257487','VENTA DE INSUMOS Y BEBIDAS','25 DE MAYO Y PUNATA AC. SUD','activo','2000-02-21','RON ABUELO ALVAREZ',1,'CASETA',6),(9,'15242168','VENTA DE INSUMOS Y BEBIDAS','25 DE MAYO Y PUNATA AC. SUD','activo','2006-09-17','KEVIN UGARTE',2,'CASETA ',7),(10,'14253647','VENTA DE COMIDA','PUNATA ENTRE ESTEBAN ARCE AC. OESTE','activo','1996-07-14','JULIO TORRICO',1,'PUESTO',8),(11,'14856985','VENTA DE COMIDA','ESTEBAN ARCE Y PUNATA AC. ESTE','activo','1998-05-18','TERESA OTONDO',1,'PUESTO',9),(12,'14758459','VENTA DE FRUTAS','PUNATA ENTRE NATANIEL AGUIRRE Y ESTEBAN ARCE AC. SUD','activo','2007-07-28','ELENA GALINDO',1,'PUESTO',10);
/*!40000 ALTER TABLE `sitio_municipal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log`
--

DROP TABLE IF EXISTS `log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log` (
  `id` int(11) NOT NULL auto_increment,
  `fechaHora` datetime default NULL,
  `descripcion` varchar(500) default NULL,
  `idUsuario` int(11) default NULL,
  `rol` varchar(45) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log`
--

LOCK TABLES `log` WRITE;
/*!40000 ALTER TABLE `log` DISABLE KEYS */;
INSERT INTO `log` VALUES (1,'2013-08-27 20:56:35','El usuario N.LOPEZno pudo acceder correctamente al sistema con las siguientes credenciales: \nid: 0\nlogin: N.LOPEZ\npassword: 456\nrol: ',NULL,NULL),(2,'2013-08-27 20:56:38','El usuario N.LOPEZ accedio correctamente al sistema. \nid: 2\nlogin: N.LOPEZ\npassword: 202cb962ac59075b964b07152d234b70\nrol: cajero',2,'cajero'),(3,'2013-08-27 20:57:53','El usuario N.LOPEZ salió correctamente del sistema. \nid: 2\nlogin: N.LOPEZ\npassword: 202cb962ac59075b964b07152d234b70\nrol: cajero',2,'cajero'),(4,'2013-08-27 22:09:30','El usuario F.ARANDIA no pudo acceder correctamente al sistema con las siguientes credenciales: \nid: 0\nlogin: F.ARANDIA\npassword: 123\nrol: ',NULL,NULL),(5,'2013-08-27 22:09:33','El usuario F.ARANDIA no pudo acceder correctamente al sistema con las siguientes credenciales: \nid: 0\nlogin: F.ARANDIA\npassword: 456\nrol: ',NULL,NULL),(6,'2013-08-27 22:09:40','El usuario F.ARANDIA no pudo acceder correctamente al sistema con las siguientes credenciales: \nid: 0\nlogin: F.ARANDIA\npassword: 123456\nrol: ',NULL,NULL),(7,'2013-08-27 22:13:39','El usuario F.ARANDIA accedio correctamente al sistema. \nid: 1\nlogin: F.ARANDIA\npassword: cb676c05a8242c126ee8f32ae17edc17\nrol: admin_entifin',1,'admin_entifin'),(8,'2013-08-27 22:13:52','El usuario F.ARANDIA salió correctamente del sistema. \nid: 1\nlogin: F.ARANDIA\npassword: cb676c05a8242c126ee8f32ae17edc17\nrol: admin_entifin',1,'admin_entifin'),(9,'2013-08-30 03:14:15','El usuario N.LOPEZ accedio correctamente al sistema. \nid: 2\nlogin: N.LOPEZ\npassword: 202cb962ac59075b964b07152d234b70\nrol: cajero',2,'cajero'),(10,'2013-08-30 03:14:21','El usuario N.LOPEZ salió correctamente del sistema. \nid: 2\nlogin: N.LOPEZ\npassword: 202cb962ac59075b964b07152d234b70\nrol: cajero',2,'cajero'),(11,'2013-08-30 03:14:55','El usuario N.LOPEZ accedio correctamente al sistema. \nid: 2\nlogin: N.LOPEZ\npassword: 202cb962ac59075b964b07152d234b70\nrol: cajero',2,'cajero'),(12,'2013-08-30 03:15:02','El usuario N.LOPEZ salió correctamente del sistema. \nid: 2\nlogin: N.LOPEZ\npassword: 202cb962ac59075b964b07152d234b70\nrol: cajero',2,'cajero'),(13,'2013-08-30 03:16:21','El usuario N.LOPEZ accedio correctamente al sistema. \nid: 2\nlogin: N.LOPEZ\npassword: 202cb962ac59075b964b07152d234b70\nrol: cajero',2,'cajero'),(14,'2013-08-30 03:16:23','El usuario N.LOPEZ salió correctamente del sistema. \nid: 2\nlogin: N.LOPEZ\npassword: 202cb962ac59075b964b07152d234b70\nrol: cajero',2,'cajero'),(15,'2013-08-30 03:21:29','El usuario N.LOPEZ accedio correctamente al sistema. \nid: 2\nlogin: N.LOPEZ\npassword: 202cb962ac59075b964b07152d234b70\nrol: cajero',2,'cajero'),(16,'2013-08-30 03:21:38','El usuario N.LOPEZ salió correctamente del sistema. \nid: 2\nlogin: N.LOPEZ\npassword: 202cb962ac59075b964b07152d234b70\nrol: cajero',2,'cajero'),(17,'2013-08-30 03:24:22','El usuario N.LOPEZ accedio correctamente al sistema. \nid: 2\nlogin: N.LOPEZ\npassword: 202cb962ac59075b964b07152d234b70\nrol: cajero',2,'cajero'),(18,'2013-08-30 03:24:27','El usuario N.LOPEZ salió correctamente del sistema. \nid: 2\nlogin: N.LOPEZ\npassword: 202cb962ac59075b964b07152d234b70\nrol: cajero',2,'cajero'),(19,'2013-08-30 03:28:30','El usuario N.LOPEZ accedio correctamente al sistema. \nid: 2\nlogin: N.LOPEZ\npassword: 202cb962ac59075b964b07152d234b70\nrol: cajero',2,'cajero'),(20,'2013-08-30 03:28:31','El usuario N.LOPEZ salió correctamente del sistema. \nid: 2\nlogin: N.LOPEZ\npassword: 202cb962ac59075b964b07152d234b70\nrol: cajero',2,'cajero'),(21,'2013-08-30 03:30:09','El usuario N.LOPEZ accedio correctamente al sistema. \nid: 2\nlogin: N.LOPEZ\npassword: 202cb962ac59075b964b07152d234b70\nrol: cajero',2,'cajero'),(22,'2013-08-30 03:30:11','El usuario N.LOPEZ salió correctamente del sistema. \nid: 2\nlogin: N.LOPEZ\npassword: 202cb962ac59075b964b07152d234b70\nrol: cajero',2,'cajero'),(23,'2013-08-30 03:30:11','El usuario N.LOPEZ salió correctamente del sistema. \nid: 2\nlogin: N.LOPEZ\npassword: 202cb962ac59075b964b07152d234b70\nrol: cajero',2,'cajero'),(24,'2013-08-30 03:33:46','El usuario N.LOPEZ accedio correctamente al sistema. \nid: 2\nlogin: N.LOPEZ\npassword: 202cb962ac59075b964b07152d234b70\nrol: cajero',2,'cajero'),(25,'2013-08-30 03:33:49','El usuario N.LOPEZ salió correctamente del sistema. \nid: 2\nlogin: N.LOPEZ\npassword: 202cb962ac59075b964b07152d234b70\nrol: cajero',2,'cajero'),(26,'2013-08-30 03:36:03','El usuario N.LOPEZ accedio correctamente al sistema. \nid: 2\nlogin: N.LOPEZ\npassword: 202cb962ac59075b964b07152d234b70\nrol: cajero',2,'cajero'),(27,'2013-08-30 03:36:05','El usuario N.LOPEZ salió correctamente del sistema. \nid: 2\nlogin: N.LOPEZ\npassword: 202cb962ac59075b964b07152d234b70\nrol: cajero',2,'cajero'),(28,'2013-08-30 03:57:41','El usuario N.LOPEZ no pudo acceder correctamente al sistema con las siguientes credenciales: \nid: 0\nlogin: N.LOPEZ\npassword: 456\nrol: ',NULL,NULL),(29,'2013-08-30 03:58:04','El usuario N.LOPEZ accedio correctamente al sistema. \nid: 2\nlogin: N.LOPEZ\npassword: 202cb962ac59075b964b07152d234b70\nrol: cajero',2,'cajero'),(30,'2013-08-30 03:58:41','El usuario N.LOPEZ salió correctamente del sistema. \nid: 2\nlogin: N.LOPEZ\npassword: 202cb962ac59075b964b07152d234b70\nrol: cajero',2,'cajero'),(31,'2013-08-30 04:08:41','El usuario N.LOPEZ accedio correctamente al sistema. \nid: 2\nlogin: N.LOPEZ\npassword: 202cb962ac59075b964b07152d234b70\nrol: cajero',2,'cajero'),(32,'2013-08-30 04:09:38','El usuario N.LOPEZ salió correctamente del sistema. \nid: 2\nlogin: N.LOPEZ\npassword: 202cb962ac59075b964b07152d234b70\nrol: cajero',2,'cajero');
/*!40000 ALTER TABLE `log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'cobranza_patentes'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-08-31  9:01:49
