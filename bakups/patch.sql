
--Issue #6 - 15-09-2013
ALTER TABLE `cobranza_patentes`.`gestion` ADD COLUMN `descuentoUfv` DOUBLE NULL  AFTER `multaIDF` , ADD COLUMN `fechaLimiteDescuento` DATE NULL  AFTER `descuentoUfv` ;

--

ALTER TABLE `cobranza_patentes`.`gestion` CHANGE COLUMN `descuentoUfv` `descuentoAplicado` DOUBLE NULL DEFAULT NULL  , CHANGE COLUMN `fechaLimiteDescuento` `fechaLimiteDescuentoAplicado` DATE NULL DEFAULT NULL  ;
